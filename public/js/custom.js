/* SELECTOR DE ARCHIVOS CON PREVISUALIZACION */
$(document).on('click', '#close-preview', function () {
    $('.image-preview').popover('hide');
    // Hover befor close the preview
    $('.image-preview').hover(
        function () {
            $('.image-preview').popover('show');
        },
        function () {
            $('.image-preview').popover('hide');
        }
    );
});

$(function () {
    // Create the close button
    var closebtn = $('<button/>', {
        type: "button",
        text: 'x',
        id: 'close-preview',
        style: 'font-size: initial;',
    });
    closebtn.attr("class", "close pull-right");
    // Set the popover default content
    $('.image-preview').popover({
        trigger: 'manual',
        html: true,
        title: "<strong>Vista Previa</strong>" + $(closebtn)[0].outerHTML,
        content: "No hay imagen",
        placement: 'bottom'
    });
    // Clear event
    $('.image-preview-clear').click(function () {
        $('.image-preview').attr("data-content", "").popover('hide');
        $('.image-preview-filename').val("");
        $('.image-preview-clear').hide();
        $('.image-preview-input input:file').val("");
        $(".image-preview-input-title").text("Buscar");
    });
    // Create the preview image
    $(".image-preview-input input:file").change(function () {
        var img = $('<img/>', {
            id: 'dynamic',
            width: 250,
            height: 200
        });
        var file = this.files[0];
        var reader = new FileReader();
        // Set preview image into the popover data-content
        reader.onload = function (e) {
            $(".image-preview-input-title").text("Cambiar");
            $(".image-preview-clear").show();
            $(".image-preview-filename").val(file.name);
            img.attr('src', e.target.result);
            $(".image-preview").attr("data-content", $(img)[0].outerHTML).popover("show");
        }
        reader.readAsDataURL(file);
    });

    $("#create-pregunta-form").submit(function (e) {
        e.preventDefault();
        var _form = $(this);

        var formData = new FormData($(this)[0]);

        container = $('#contenidoTablaPreguntas');

        $.ajax({
            method: _form.prop('method'),
            url: _form.prop('action'),
            data: formData,
            contentType: false,
            cache: false,
            processData: false,
            success: function (response) {
                var pregunta = response['pregunta'];
                console.log(pregunta);
                $('#modal-dialog-agregarPregunta').modal('hide')
                if (response.status == 'success') {
                    container.append($('<tr></tr>')
                        .append($('<td>' + pregunta['pregunta'] + '</td>'))
                        .append($('<td></td>')
                            .append('<a data-toggle="modal" data-target="#modal-dialog-edit-' + pregunta['id'] + '" href="#" class="btn btn-primary btn-sm"> <i class="fa fa-edit" aria-hidden="true"></i> Editar</a>')
                            .append('<form method="POST" action="'+ baseurl + '/admin/preguntas/' + pregunta['id'] + '" accept-charset="UTF-8" style="display:inline"> ' +
                                '<input type="hidden" name="_method" value="DELETE"> <input type="hidden" name="_token" value="' + response.token + '">' +
                                '<button type="submit" class="btn btn-danger btn-sm" title="Eliminar Pregunta" ' +
                                'onclick="return confirm(\'¿Desea eliminar este registro?\')">' +
                                '<i class="fa fa-trash" aria-hidden="true"></i> Eliminar </button></form>'))
                    );
                    limpiarCamposPregunta();
                } else {

                }
            },
            error: function (jqXHR, textStatus, errorThrown) { // What to do if we fail
                console.log(JSON.stringify(jqXHR));
                console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
            }
        });

    });

    function limpiarCamposPregunta() {
        $('#pregunta').val('');
        $('#linkexterno').val('');
        $('#opcion1').val('');
        $('#opcion2').val('');
        $('#opcion3').val('');
        $('#opcion4').val('');
        $('#opcion5').val('');
        $('#opcion6').val('');
    }
});

