/**
 * Created by Juan José Cavallero on 16/08/2018
 */
function validacionesAjax(data, tipo, form) {
    tipo = tipo || 'todas';
    respuesta = true;

    $.each(data, function (i, e) {

        if (e != '' && e[0].length > 0 && e != 'error') {

            if (e.length > 1) {

                $.each(e, function (index, element) {

                    console.log(1);
                    if (element.substring(0,10)=='[Optional]') {
                        if (tipo == 'todas' || tipo == 'opcional') {
                            swal({
                                    title: "Atención!",
                                    text: element.replace('[Optional]','') + ' ¿Desea confirmar?',
                                    type: "warning",
                                    showCancelButton: true,
                                    showConfirmButton: true,
                                    confirmButtonClass: "btn-danger",
                                    confirmButtonText: "Si",
                                    cancelButtonText: "No",
                                    closeOnConfirm: false,
                                    closeOnCancel: true
                                },
                                function(isConfirm) {
                                    if ( isConfirm) {
                                        respuesta = true;
                                        $('#confirmed').val(1);
                                        form.submit();
                                        console.log('submit');
                                        return respuesta;
                                    } else
                                    {
                                        respuesta = false;
                                        return respuesta;
                                    }
                                });
                        }

                    } else  {
                        respuesta = false;
                        $('#'+i+'-error').text(element);
                        $('#'+i+'-error').addClass('help-block-error');


                        if ($('#'+i ).hasClass('select2')) {
                            $('#'+i+'-error').attr('style','opacity:1');
                            $('#'+i ).closest('div.form-md-line-input').find('span.select2-container').removeClass('selectmd');
                            $('#'+i ).closest('div.form-md-line-input').find('span.select2-container').addClass("selectmd-error");
                        }

                        $('#'+i ).closest('div.form-group').addClass('has-error');
                    }

                });

            } else

            {

                if (e[0].substring(0,10)=='[Optional]') {
                    if (tipo == 'todas' || tipo == 'opcional') {
                        swal({
                                title: "Atención!",
                                text: e[0].replace('[Optional]','') + ' ¿Desea confirmar?',
                                type: "warning",
                                showCancelButton: true,
                                showConfirmButton: true,
                                confirmButtonClass: "btn-danger",
                                confirmButtonText: "Si",
                                cancelButtonText: "No",
                                closeOnConfirm: false,
                                closeOnCancel: true
                            },
                            function(isConfirm) {
                                if ( isConfirm) {
                                    respuesta = true;
                                    $('#confirmed').val(1);
                                    console.log('submit');
                                    form.submit();

                                    return respuesta;
                                } else
                                {
                                    respuesta = false;
                                    return respuesta;
                                }
                            });
                    }
                } else  {
                    respuesta = false;
                    $('#'+i+'-error').text(e[0]);
                    $('#'+i+'-error').addClass('help-block-error');


                    if ($('#'+i ).hasClass('select2')) {
                        $('#'+i+'-error').attr('style','opacity:1');
                        $('#'+i ).closest('div.form-md-line-input').find('span.select2-container').removeClass('selectmd');
                        $('#'+i ).closest('div.form-md-line-input').find('span.select2-container').addClass("selectmd-error");
                    }

                    $('#'+i ).closest('div.form-group').addClass('has-error');
                }
            }




        }
    });

    /*    if (tipo == 'todas' || tipo == 'opcional') {

     }*/

    var button = $('.btn-oneclick');

    button.val('Guardar');
    button.prop('disabled',false);
    $('.grey-cascade').show();

    return respuesta;
}

$(document).ready(function(){
    $('.btn-oneclick').click(function (e) {
        console.log("algo");
        var button = $(e.target);

        // if (button.parents('form').valid()) {
            button.val('Guardando...');
            button.prop('disabled',true);
            $('.grey-cascade').hide();
            $('#loading-img').show();
            button.parents('form').submit();
        // }

    });
});

/* SELECTOR DE ARCHIVOS CON PREVISUALIZACION */
$(document).on('click', '#close-preview', function(){
    $('.image-preview').popover('hide');
    // Hover befor close the preview
    $('.image-preview').hover(
        function () {
            $('.image-preview').popover('show');
        },
        function () {
            $('.image-preview').popover('hide');
        }
    );
});

$(function() {
    // Create the close button
    var closebtn = $('<button/>', {
        type:"button",
        text: 'x',
        id: 'close-preview',
        style: 'font-size: initial;',
    });
    closebtn.attr("class","close pull-right");
    // Set the popover default content
    $('.image-preview').popover({
        trigger:'manual',
        html:true,
        title: "<strong>Vista Previa</strong>"+$(closebtn)[0].outerHTML,
        content: "No hay imagen",
        placement:'bottom'
    });
    // Clear event
    $('.image-preview-clear').click(function(){
        $('.image-preview').attr("data-content","").popover('hide');
        $('.image-preview-filename').val("");
        $('.image-preview-clear').hide();
        $('.image-preview-input input:file').val("");
        $(".image-preview-input-title").text("Buscar");
    });
    // Create the preview image
    $(".image-preview-input input:file").change(function (){
        var img = $('<img/>', {
            id: 'dynamic',
            width:250,
            height:200
        });
        var file = this.files[0];
        var reader = new FileReader();
        // Set preview image into the popover data-content
        reader.onload = function (e) {
            $(".image-preview-input-title").text("Cambiar");
            $(".image-preview-clear").show();
            $(".image-preview-filename").val(file.name);
            img.attr('src', e.target.result);
            $(".image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
        }
        reader.readAsDataURL(file);
    });
});
