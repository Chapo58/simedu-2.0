var DatatablesAdvancedColumnRendering = {
    init: function () {
        $("#m_table_1").DataTable({
            responsive: !0,
            paging: !0,
            language: {
                "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
            },
        })
    }
};
jQuery(document).ready(function () {
    DatatablesAdvancedColumnRendering.init()
});