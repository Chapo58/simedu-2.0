<?php

/**
 * Frontend Controllers
 * All route names are prefixed with 'frontend.'.
 */
Route::get('/', 'HomeController@index')->name('index');
Route::get('contact', 'ContactController@index')->name('contact');
Route::post('contact/send', 'ContactController@send')->name('contact.send');

/*
 * These frontend controllers require the user to be logged in
 * All route names are prefixed with 'frontend.'
 * These routes can not be hit if the password is expired
 */
Route::group(['middleware' => ['auth', 'password_expires']], function () {
    Route::group(['namespace' => 'User', 'as' => 'user.'], function () {
        /*
         * User Dashboard Specific
         */
        Route::get('dashboard', 'DashboardController@index')->name('dashboard');

        /*
         * User Account Specific
         */
        Route::get('account', 'AccountController@index')->name('account');

        /*
         * User Profile Specific
         */
        Route::patch('profile/update', 'ProfileController@update')->name('profile.update');

        Route::get('perfil', 'AccountController@index')->name('perfil');
    });
});


Route::resource('empresas', 'EmpresaController');
Route::get('empresas/seleccionar/{id_empresa}', 'EmpresaController@seleccionarEmpresa');

Route::resource('cuentos', 'CuentoController');

Route::group(['namespace' => 'Sueldos', 'as' => 'sueldos.'], function () {

    Route::resource('sueldos/categoriasSueldos', 'CategoriasSueldosController');
    Route::resource('sueldos/empleados', 'EmpleadosController');
    Route::resource('sueldos/codigos', 'CodigoController');

    Route::get('sueldos/liquidaciones/empleado/{id}', 'LiquidacionController@index');
    Route::get('sueldos/liquidaciones/empleado/{id}/create', 'LiquidacionController@create');
    Route::resource('sueldos/liquidaciones', 'LiquidacionController');

    Route::get('sueldos/codigos_empleado/{id}', 'CodigoEmpleadoController@index');
    Route::get('sueldos/codigos_empleado/{id}/create', 'CodigoEmpleadoController@create');
    Route::post('sueldos/codigos_empleado/store', 'CodigoEmpleadoController@store');
    Route::get('sueldos/codigos_empleado/{id}/editar/{id_codigo}', 'CodigoEmpleadoController@edit');
    Route::post('sueldos/codigos_empleado/update', 'CodigoEmpleadoController@update');
    Route::get('sueldos/codigos_empleado/{id}/eliminar/{id_codigo}', 'CodigoEmpleadoController@destroy');
    Route::post('sueldos/codigos_empleado/obtenerCodigo', 'CodigoEmpleadoController@obtenerCodigo');

    Route::get('sueldos/recibo/imprimir/{id}', 'ReciboController@imprimir');

});

Route::group(['namespace' => 'Contabilidad', 'as' => 'contabilidad.'], function () {

    Route::get('contabilidad/plan_cuentas/obtenerCuentas', 'PlanCuentaController@obtenerCuentas');
    Route::resource('contabilidad/plan_cuentas', 'PlanCuentaController');

    Route::resource('contabilidad/asientos', 'AsientoController');

    Route::resource('contabilidad/periodos', 'PeriodoController');
    Route::get('contabilidad/periodos/seleccionar/{id_periodo}', 'PeriodoController@seleccionarPeriodo');

});
