<?php

/**
 * All route names are prefixed with 'admin.'.
 */
Route::redirect('/', '/admin/dashboard', 301);
Route::get('dashboard', 'DashboardController@index')->name('dashboard');

Route::group(['namespace' => 'Superadmin', 'as' => 'superadmin.'], function () {
    Route::resource('superadmin/paises', 'PaisesController');
    Route::resource('superadmin/provincias', 'ProvinciasController');
    Route::resource('superadmin/localidades', 'LocalidadesController');
    Route::resource('superadmin/instituciones', 'InstitucionesController');
    Route::resource('superadmin/obra-social', 'ObraSocialController');
});
Route::resource('profesores', 'ProfesoresController');
Route::resource('alumnos', 'AlumnosController');
Route::resource('cursos', 'CursosController');
Route::resource('planteos', 'PlanteosController');
Route::resource('preguntas', 'PreguntasController');