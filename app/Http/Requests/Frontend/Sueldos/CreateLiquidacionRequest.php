<?php

namespace App\Http\Requests\Frontend\Sueldos;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Frontend\Sueldos\Liquidacion;

class CreateLiquidacionRequest extends FormRequest{

    public function authorize(){
        return true;
    }

    public function rules(){
        return Liquidacion::$rules;
    }
}
