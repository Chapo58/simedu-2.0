<?php

namespace App\Http\Requests\Frontend\Contabilidad;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Frontend\Contabilidad\Periodo;

class UpdatePeriodoRequest extends FormRequest{

    public function authorize(){
        return true;
    }

    public function rules(){
        return Periodo::$rules;
    }
}
