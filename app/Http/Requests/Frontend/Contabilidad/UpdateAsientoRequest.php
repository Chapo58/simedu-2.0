<?php

namespace App\Http\Requests\Frontend\Contabilidad;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Frontend\Contabilidad\Asiento;

class UpdateAsientoRequest extends FormRequest{

    public function authorize(){
        return true;
    }

    public function rules(){
        return Asiento::$rules;
    }
}
