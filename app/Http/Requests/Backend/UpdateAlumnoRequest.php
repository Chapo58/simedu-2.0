<?php

namespace App\Http\Requests\Backend;

use App\Models\Backend\Alumno;
use Illuminate\Foundation\Http\FormRequest;

class UpdateAlumnoRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $alumno=Alumno::where('id',\Request::segment(3))->first();

        $rules=Alumno::$rules;
        $rules['email'] .= ',' . $alumno->user_id ;

        return $rules;

    }

}
