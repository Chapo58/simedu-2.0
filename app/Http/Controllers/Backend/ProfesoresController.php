<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\CreateProfesorRequest;
use App\Http\Requests\Backend\UpdateProfesorRequest;
use App\Models\Auth\User;
use App\Models\Backend\Profesor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Auth;
use Laracasts\Flash\Flash;
use Session;

class ProfesoresController extends Controller{

    public function index(Request $request){
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $profesores = Profesor::where('nombre', 'LIKE', "%$keyword%")
                ->orWhere('apellido', 'LIKE', "%$keyword%")
                ->orWhere('email', 'LIKE', "%$keyword%")
                ->orWhere('genero', 'LIKE', "%$keyword%")
                ->orWhere('fecha_nacimiento', 'LIKE', "%$keyword%")
                ->orWhere('telefono', 'LIKE', "%$keyword%")
                ->orWhere('direccion', 'LIKE', "%$keyword%")
                ->orWhere('user_id', 'LIKE', "%$keyword%")
                ->orWhere('institucion_id', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $profesores = Profesor::latest()->paginate($perPage);
        }

        return view('backend.profesores.index', compact('profesores'));
    }

    public function create(){
        return view('backend.profesores.create');
    }

    public function store(CreateProfesorRequest $request){

        $requestData = $request->all();

        //Creo el Usuario
        $usuario= new User();
        $usuario->first_name= $request->nombre;
        $usuario->last_name= $request->apellido;
        $usuario->email= $request->email;
        $usuario->password= 'SIMEDU2018';
        $usuario->confirmed= 0;
        $usuario->confirmation_code = md5(uniqid(mt_rand(), true));
        $usuario->save();
        $usuario->syncRoles(['4']);
        $usuario->syncPermissions(['1']);

        $requestData['user_id'] = $usuario->id;
        $requestData['institucion_id'] = Auth::user()->institucion()->id;

        Profesor::create($requestData);

        Mail::send('backend.mails.nuevousuario', ['usuario' => $usuario,], function ($m) use ($usuario) {

            $m->from('info@simedu.com.ar', 'SIMEDU');

            $m->to($usuario->email, $usuario->nombre)->subject('¡Bienvenido a SIMEDU!');
        });
        if (count(Mail::failures()) > 0) {
            Flash::success('El profesor se creó correctamente pero el email no pudo ser enviado.');
            return redirect('admin/profesores');
        }

        return redirect('admin/profesores')->with('flash_message', 'Profesor Creado Correctamente');
    }

    public function show($id){
        $profesor = Profesor::findOrFail($id);

        return view('backend.profesores.show', compact('profesor'));
    }

    public function edit($id){
        $profesor = Profesor::findOrFail($id);

        return view('backend.profesores.edit', compact('profesor'));
    }

    public function update(UpdateProfesorRequest $request, $id){
        $profesor = Profesor::findOrFail($id);

        $profesor->user->email=$request->email;
        $profesor->user->first_name=$request->nombre;
        $profesor->user->last_name=$request->apellido;
        $profesor->user->save();

        $requestData = $request->all();

        $profesor->update($requestData);

        return redirect('admin/profesores')->with('flash_message', 'Profesore Actualizado!');
    }

    public function destroy($id){
        Profesor::destroy($id);

        return redirect('backend.profesores.index')->with('flash_message', 'Profesore Eliminado!');
    }
}
