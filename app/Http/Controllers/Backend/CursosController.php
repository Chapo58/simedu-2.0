<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\CreateCursoRequest;
use App\Http\Requests\Backend\UpdateCursoRequest;
use App\Models\Backend\Alumno;
use App\Models\Backend\Curso;
use App\Models\Backend\Profesor;
use Illuminate\Http\Request;
use Auth;
use Session;

class CursosController extends Controller{

    public function index(Request $request){
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $cursos = Curso::where('nombre', 'LIKE', "%$keyword%")
                ->orWhere('institucion_id', 'LIKE', "%$keyword%")
                ->orWhere('habilitado', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $cursos = Curso::latest()->paginate($perPage);
        }

        return view('backend.cursos.index', compact('cursos'));
    }

    public function create(){
        $profesores = Profesor::all();
        $alumnos = Alumno::all();
        return view('backend.cursos.create',compact('cursos','profesores','alumnos'));
    }

    public function store(CreateCursoRequest $request){

        $requestData = $request->all();

        $requestData['institucion_id'] = Auth::user()->institucion()->id;
        $curso=Curso::create($requestData);
        $curso->profesores()->sync($request->profesores);
        $curso->alumnos()->sync($request->alumnos);


        return redirect('admin/cursos')->with('flash_message', 'Curso Creado!');
    }

    public function show($id){
        $curso = Curso::findOrFail($id);

        return view('backend.cursos.show', compact('curso'));
    }

    public function edit($id){
        $profesores = Profesor::all();
        $alumnos = Alumno::all();
        $curso = Curso::findOrFail($id);
        $profesorescurso=$curso->profesores()->get();
        $alumnosscurso=$curso->alumnos()->get();

        return view('backend.cursos.edit', compact('curso','profesores','profesorescurso','alumnosscurso','alumnos'));
    }

    public function update(UpdateCursoRequest $request, $id){
        $curso = Curso::findOrFail($id);

        $requestData = $request->all();

        $curso->update($requestData);

        $curso->profesores()->sync($request->profesores);
        $curso->alumnos()->sync($request->alumnos);

        return redirect('admin/cursos')->with('flash_message', 'Curso Actualizado!');

    }

    public function destroy($id){
        Curso::destroy($id);

        return redirect('backend.cursos.index')->with('flash_message', 'Curso Eliminado!');
    }
}
