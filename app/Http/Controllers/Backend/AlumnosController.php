<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\CreateAlumnoRequest;
use App\Http\Requests\Backend\UpdateAlumnoRequest;
use App\Models\Auth\User;
use App\Models\Backend\Alumno;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Auth;
use Laracasts\Flash\Flash;
use Session;

class AlumnosController extends Controller{

    public function index(Request $request){
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $alumnos = Alumno::where('nombre', 'LIKE', "%$keyword%")
                ->orWhere('apellido', 'LIKE', "%$keyword%")
                ->orWhere('email', 'LIKE', "%$keyword%")
                ->orWhere('genero', 'LIKE', "%$keyword%")
                ->orWhere('fecha_nacimiento', 'LIKE', "%$keyword%")
                ->orWhere('telefono', 'LIKE', "%$keyword%")
                ->orWhere('direccion', 'LIKE', "%$keyword%")
                ->orWhere('user_id', 'LIKE', "%$keyword%")
                ->orWhere('institucion_id', 'LIKE', "%$keyword%")
                ->orWhere('facebook', 'LIKE', "%$keyword%")
                ->orWhere('twitter', 'LIKE', "%$keyword%")
                ->orWhere('instagram', 'LIKE', "%$keyword%")
                ->orWhere('googlemas', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $alumnos = Alumno::latest()->paginate($perPage);
        }

        return view('backend.alumnos.index', compact('alumnos'));
    }

    public function create(){
        return view('backend.alumnos.create');
    }

    public function store(CreateAlumnoRequest $request){

        $requestData = $request->all();

        //Creo el Usuario
        $usuario= new User();
        $usuario->first_name= $request->nombre;
        $usuario->last_name= $request->apellido;
        $usuario->email= $request->email;
        $usuario->password= 'SIMEDU2018';
        $usuario->confirmed= 0;
        $usuario->confirmation_code = md5(uniqid(mt_rand(), true));
        $usuario->save();
        $usuario->syncRoles(['3']);

        $requestData['user_id'] = $usuario->id;
        $requestData['institucion_id'] = Auth::user()->institucion()->id;

        Alumno::create($requestData);

        Mail::send('backend.mails.nuevousuario', ['usuario' => $usuario,], function ($m) use ($usuario) {

            $m->from('info@simedu.com.ar', 'SIMEDU');

            $m->to($usuario->email, $usuario->nombre)->subject('¡Bienvenido a SIMEDU!');
        });
        if (count(Mail::failures()) > 0) {
            Flash::success('El alumno se creó correctamente pero el email no pudo ser enviado.');
            return redirect('admin/alumnos');
        }

        return redirect('admin/alumnos')->with('flash_message', 'Alumno Creado Correctamente!');
    }

    public function show($id){
        $alumno = Alumno::findOrFail($id);

        return view('backend.alumnos.show', compact('alumno'));
    }

    public function edit($id){
        $alumno = Alumno::findOrFail($id);

        return view('backend.alumnos.edit', compact('alumno'));
    }

    public function update(UpdateAlumnoRequest $request, $id){
        $alumno = Alumno::findOrFail($id);

        $alumno->user->email=$request->email;
        $alumno->user->save();

        $requestData = $request->all();

        $alumno->update($requestData);

        return redirect('admin/alumnos')->with('flash_message', 'Alumno Actualizado!');

    }

    public function destroy($id){
        Alumno::destroy($id);

        return redirect('backend.alumnos.index')->with('flash_message', 'Alumno Eliminado!');
    }
}
