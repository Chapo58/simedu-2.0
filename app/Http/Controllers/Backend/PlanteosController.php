<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\CreatePlanteoRequest;
use App\Models\Backend\Planteo;
use App\Models\Backend\Pregunta;
use Illuminate\Http\Request;
use Auth;
use Session;

class PlanteosController extends Controller{

    public function index(Request $request){
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $planteos = Planteo::where('nombre', 'LIKE', "%$keyword%")
                ->orWhere('profesor_id', 'LIKE', "%$keyword%")
                ->orWhere('habilitado', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $planteos = Planteo::latest()->paginate($perPage);
        }

        return view('backend.planteos.index', compact('planteos'));
    }

    public function create(){
        return view('backend.planteos.create');
    }

    public function store(CreatePlanteoRequest $request){

        $requestData = $request->all();
        $requestData['profesor_id'] = Auth::user()->profesor()->id;
        Planteo::create($requestData);

        return redirect('admin/planteos')->with('flash_message', 'Planteo Creado Correctamente');
    }

    public function show($id){
        $planteo = Planteo::findOrFail($id);

        return view('backend.planteos.show', compact('planteo'));
    }

    public function edit($id){
        $planteo = Planteo::findOrFail($id);
        $preguntas=Pregunta::where('planteo_id',$planteo->id)->get();

        return view('backend.planteos.edit', compact('planteo','preguntas'));
    }

    public function update(Request $request, $id){
        $this->validate($request, [
			'nombre' => 'required'
		]);
        $requestData = $request->all();
        
        $planteo = Planteo::findOrFail($id);
        $planteo->update($requestData);

        return redirect('admin/planteos')->with('flash_message', 'Planteo Actualizado!');

    }

    public function destroy($id){
        Planteo::destroy($id);

        return redirect('backend.planteos.index')->with('flash_message', 'Planteo Eliminado!');
    }
}
