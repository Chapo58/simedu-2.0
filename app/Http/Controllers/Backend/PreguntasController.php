<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Backend\Pregunta;
use Illuminate\Http\Request;
use Auth;
use Session;
use Validator;

class PreguntasController extends Controller{

    public function index(Request $request){
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $preguntas = Pregunta::where('pregunta', 'LIKE', "%$keyword%")
                ->orWhere('archivo', 'LIKE', "%$keyword%")
                ->orWhere('linkexterno', 'LIKE', "%$keyword%")
                ->orWhere('opcion1', 'LIKE', "%$keyword%")
                ->orWhere('opcion2', 'LIKE', "%$keyword%")
                ->orWhere('opcion3', 'LIKE', "%$keyword%")
                ->orWhere('opcion4', 'LIKE', "%$keyword%")
                ->orWhere('opcion5', 'LIKE', "%$keyword%")
                ->orWhere('opcion6', 'LIKE', "%$keyword%")
                ->orWhere('opcionCorrecta', 'LIKE', "%$keyword%")
                ->orWhere('planteo_id', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $preguntas = Pregunta::latest()->paginate($perPage);
        }

        return view('backend.preguntas.index', compact('preguntas'));
    }

    public function create(){
        return view('backend.preguntas.create');
    }

    public function store(Request $request){

        $this->validate($request, [
			'pregunta' => 'required'
		]);

        //Valido el archivo
        $archivo = $request->file('archivo');

        $archivoRules = array(
            'archivo' => 'mimes:jpeg,jpg,png,gif,mp4,mov,ogg,pdf,xlsx,doc,docx|required'
        );

        if ($archivo) {
            $validator = Validator::make(['archivo' => $archivo], $archivoRules);
            if ($validator->fails()) {

                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            }
        }

        $requestData = $request->all();
        $requestData['planteo_id'] = $request->planteo;
        $requestData['opcionCorrecta'] = $request->opcionCorrectaCreate;

        $pregunta= Pregunta::create($requestData);

        //Guardo el archivo
        if ($archivo) {
            $destinationPath = 'uploads/preguntas/';
            $filename = md5(microtime()) . '.' . $archivo->getClientOriginalExtension();
            $archivo->move($destinationPath, $filename);

            $pregunta->archivo = $filename;
            $pregunta->save();

        }

        return response()->json(['status' => 'success', 'msg' => 'Pregunta Creada y Agregada Correctamente.','pregunta'=>$pregunta,'token'=>$request->_token]);

    }

    public function show($id){
        $pregunta = Pregunta::findOrFail($id);

        return view('backend.preguntas.show', compact('pregunta'));
    }

    public function edit($id){
        $pregunta = Pregunta::findOrFail($id);

        return view('backend.preguntas.edit', compact('pregunta'));
    }

    public function update(Request $request, $id){
        $this->validate($request, [
			'pregunta' => 'required'
		]);
        $requestData = $request->all();

        $pregunta = Pregunta::findOrFail($id);
        $pregunta->update($requestData);

        return back()->with('flash_message', 'Pregunta Actualizada Correctamente');

    }

    public function destroy($id){
        Pregunta::destroy($id);

        return back()->with('flash_message', 'Pregunta Eliminada Correctamente');
    }
}
