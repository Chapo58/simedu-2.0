<?php

namespace App\Http\Controllers\Backend\Superadmin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\ImageController;
use App\Models\Backend\Superadmin\Pais;
use Illuminate\Http\Request;
use Auth;
use Session;

class PaisesController extends Controller{

    public function index(Request $request){
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $paises = Pais::where('nombre', 'LIKE', "%$keyword%")
                ->orWhere('imagen_url', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $paises = Pais::latest()->paginate($perPage);
        }

        return view('backend.superadmin.paises.index', compact('paises'));
    }

    public function create(){
        return view('backend.superadmin.paises.create');
    }

    public function store(Request $request){
        $this->validate($request, [
			'nombre' => 'required'
		]);
        $requestData = $request->all();

        $controllerImagen = new ImageController();
        $imagen_url = $controllerImagen->guardarImagenPais($request);
        if($imagen_url != false){
            $requestData['imagen_url'] = $imagen_url;
        }
        
        Pais::create($requestData);

        return redirect('admin/superadmin/paises')->with('flash_message', 'Pais Agregado!');
    }

    public function show($id){
        $pais = Pais::findOrFail($id);

        return view('backend.superadmin.paises.show', compact('pais'));
    }

    public function edit($id){
        $pais = Pais::findOrFail($id);

        return view('backend.superadmin.paises.edit', compact('pais'));
    }

    public function update(Request $request, $id){
        $this->validate($request, [
			'nombre' => 'required'
		]);
        $requestData = $request->all();
        
        $pais = Pais::findOrFail($id);

        $controllerImagen = new ImageController();
        $imagen_url = $controllerImagen->guardarImagenPais($request);
        if($imagen_url != false){
            $requestData['imagen_url'] = $imagen_url;
        }

        $pais->update($requestData);

        return redirect('admin/superadmin/paises')->with('flash_message', 'Pais actualizado!');
    }

    public function destroy($id){
        Pais::destroy($id);

        return redirect('admin/superadmin/paises')->with('flash_message', 'Pais Eliminado!');
    }
}
