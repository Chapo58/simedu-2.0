<?php

namespace App\Http\Controllers\Backend\Superadmin;

use App\Http\Controllers\Controller;
use App\Models\Backend\Superadmin\Pais;
use App\Models\Backend\Superadmin\Provincia;
use Illuminate\Http\Request;
use Auth;
use Session;

class ProvinciasController extends Controller{

    public function index(Request $request){
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $provincias = Provincia::where('nombre', 'LIKE', "%$keyword%")
                ->orWhere('pais_id', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $provincias = Provincia::latest()->paginate($perPage);
        }

        return view('backend.superadmin.provincias.index', compact('provincias'));
    }

    public function create(){
        $paises = Pais::getPaisesArray();

        return view('backend.superadmin.provincias.create', compact('paises'));
    }

    public function store(Request $request){
        $this->validate($request, [
			'nombre' => 'required',
			'pais_id' => 'required'
		]);
        $requestData = $request->all();
        
        Provincia::create($requestData);

        return redirect('admin/superadmin/provincias')->with('flash_message', 'Provincia creada!');
    }

    public function show($id){
        $provincia = Provincia::findOrFail($id);

        return view('backend.superadmin.provincias.show', compact('provincia'));
    }

    public function edit($id){
        $provincia = Provincia::findOrFail($id);
        $paises = Pais::getPaisesArray();

        return view('backend.superadmin.provincias.edit', compact('provincia','paises'));
    }

    public function update(Request $request, $id){
        $this->validate($request, [
			'nombre' => 'required',
			'pais_id' => 'required'
		]);
        $requestData = $request->all();
        
        $provincia = Provincia::findOrFail($id);
        $provincia->update($requestData);

        return redirect('admin/superadmin/provincias')->with('flash_message', 'Provincia actualizada!');
    }

    public function destroy($id){
        Provincia::destroy($id);

        return redirect('admin/superadmin/provincias')->with('flash_message', 'Provincia eliminada!');
    }
}
