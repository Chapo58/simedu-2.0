<?php

namespace App\Http\Controllers\Backend\Superadmin;

use App\Http\Controllers\Controller;
use App\Models\Backend\Superadmin\ObraSocial;
use Illuminate\Http\Request;
use Auth;
use Session;

class ObraSocialController extends Controller{

    public function index(Request $request){
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $obrasocial = ObraSocial::where('codigo', 'LIKE', "%$keyword%")
                ->orWhere('descripcion', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $obrasocial = ObraSocial::latest()->paginate($perPage);
        }

        return view('backend.superadmin.obra-social.index', compact('obrasocial'));
    }

    public function create(){
        return view('backend.superadmin.obra-social.create');
    }

    public function store(Request $request){
        $this->validate($request, [
			'codigo' => 'required',
			'descripcion' => 'required'
		]);
        $requestData = $request->all();
        
        ObraSocial::create($requestData);

        return redirect('admin/superadmin/obra-social')->with('flash_message', 'Obra Social Creada!');
    }

    public function show($id){
        $obrasocial = ObraSocial::findOrFail($id);

        return view('backend.superadmin.obra-social.show', compact('obrasocial'));
    }

    public function edit($id){
        $obrasocial = ObraSocial::findOrFail($id);

        return view('backend.superadmin.obra-social.edit', compact('obrasocial'));
    }

    public function update(Request $request, $id){
        $this->validate($request, [
			'codigo' => 'required',
			'descripcion' => 'required'
		]);
        $requestData = $request->all();
        
        $obrasocial = ObraSocial::findOrFail($id);
        $obrasocial->update($requestData);

        return redirect('admin/superadmin/obra-social')->with('flash_message', 'Obra Social Actualizada!');

    }

    public function destroy($id){
        ObraSocial::destroy($id);

        return redirect('admin/superadmin/obra-social')->with('flash_message', 'Obra Social Eliminada!');
    }
}
