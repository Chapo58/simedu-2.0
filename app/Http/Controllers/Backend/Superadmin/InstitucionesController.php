<?php

namespace App\Http\Controllers\Backend\Superadmin;

use App\Http\Controllers\Controller;

use App\Http\Requests\Backend\CreateInstitucionRequest;
use App\Http\Requests\Backend\UpdateInstitucionRequest;
use App\Models\Auth\User;
use App\Models\Backend\Superadmin\Institucion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Validator;
use Auth;
use Session;

class InstitucionesController extends Controller{

    public function index(Request $request){
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $instituciones = Institucion::where('numero', 'LIKE', "%$keyword%")
                ->orWhere('nombre', 'LIKE', "%$keyword%")
                ->orWhere('direccion', 'LIKE', "%$keyword%")
                ->orWhere('telefono1', 'LIKE', "%$keyword%")
                ->orWhere('telefono2', 'LIKE', "%$keyword%")
                ->orWhere('email', 'LIKE', "%$keyword%")
                ->orWhere('user_id', 'LIKE', "%$keyword%")
                ->orWhere('logo', 'LIKE', "%$keyword%")
                ->orWhere('habilitado', 'LIKE', "%$keyword%")
                ->orWhere('nombre_director', 'LIKE', "%$keyword%")
                ->orWhere('telefono_director', 'LIKE', "%$keyword%")
                ->orWhere('email_director', 'LIKE', "%$keyword%")
                ->orWhere('direccion_director', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $instituciones = Institucion::latest()->paginate($perPage);
        }

        return view('backend.superadmin.instituciones.index', compact('instituciones'));
    }

    public function create(){
        return view('backend.superadmin.instituciones.create');
    }

    public function store(CreateInstitucionRequest $request){

        //Guardo el archivo
        $archivo = $request->file('logo');

        $archivoRules = array(
            'archivo' => 'mimes:jpeg,jpg,png,gif,mp4,mov,ogg|required' // max 10000kb
        );

        if ($archivo) {
            $validator = Validator::make(['archivo' => $archivo], $archivoRules);
            if ($validator->fails()) {

                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            }
        }


        $requestData = $request->all();

        //Creo el Usuario
        $usuario= new User;
        $usuario->first_name= $request->nombre;
        $usuario->last_name= $request->nombre;
        $usuario->email= $request->email;
        $usuario->password= 'SIMEDU2018';
        $usuario->confirmed= 0;
        $usuario->confirmation_code = md5(uniqid(mt_rand(), true));
        $usuario->save();
        $usuario->syncRoles(['2']);
        $usuario->syncPermissions(['1']);

        $requestData['user_id'] = $usuario->id;

        $institucion=Institucion::create($requestData);

        if ($archivo) {
            $destinationPath = 'uploads/instituciones/';
            $filename = md5(microtime()) . '.' . $archivo->getClientOriginalExtension();
            $archivo->move($destinationPath, $filename);

            $institucion->logo = $filename;
            $institucion->save();

        }

        Mail::send('backend.mails.nuevousuario', ['usuario' => $usuario,], function ($m) use ($usuario) {

            $m->from('info@simedu.com.ar', 'SIMEDU');

            $m->to($usuario->email, $usuario->nombre)->subject('¡Bienvenido a SIMEDU!');
        });
        if (count(Mail::failures()) > 0) {
            Flash::success('La Institución se creó correctamente pero el email no pudo ser enviado.');
            return redirect('admin/superadmin/instituciones');
        }

        return redirect('admin/superadmin/instituciones')->with('flash_message', 'Institución Creada Correctamente');

    }

    public function show($id){
        $institucion = Institucion::findOrFail($id);

        return view('backend.superadmin.instituciones.show', compact('institucion'));
    }

    public function edit($id){
        $institucion = Institucion::findOrFail($id);

        return view('backend.superadmin.instituciones.edit', compact('institucion'));
    }

    public function update(UpdateInstitucionRequest $request, $id){

        //Guardo el archivo
        $archivo = $request->file('logo');

        $archivoRules = array(
            'archivo' => 'mimes:jpeg,jpg,png,gif,mp4,mov,ogg|required' // max 10000kb
        );

        if ($archivo) {
            $validator = Validator::make(['archivo' => $archivo], $archivoRules);
            if ($validator->fails()) {

                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            }
        }

        $institucion = Institucion::findOrFail($id);

        $requestData = $request->all();

        $fotoVieja = $institucion->imagen;

        $institucion->update($requestData);

        if ($archivo) {
            $destinationPath = 'uploads/instituciones/';
            $filename = md5(microtime()) . '.' . $archivo->getClientOriginalExtension();
            $archivo->move($destinationPath, $filename);

            if ($fotoVieja) {
                if (file_exists('uploads/instituciones/' . $fotoVieja)) {
                    @unlink('uploads/instituciones/' . $fotoVieja);
                }
            }
            $institucion->logo = $filename;
            $institucion->save();

        }

        return redirect('admin/superadmin/instituciones')->with('flash_message', 'Institución Actualizada Correctamente');
    }

    public function destroy($id){
        Institucion::destroy($id);

        return redirect('admin/superadmin/instituciones')->with('flash_message', 'Institución Eliminada!');
    }
}
