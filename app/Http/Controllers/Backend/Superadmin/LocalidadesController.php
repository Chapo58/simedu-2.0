<?php

namespace App\Http\Controllers\Backend\Superadmin;

use App\Http\Controllers\Controller;
use App\Models\Backend\Superadmin\Provincia;
use App\Models\Backend\Superadmin\Localidad;
use Illuminate\Http\Request;
use Auth;
use Session;

class LocalidadesController extends Controller{

    public function index(Request $request){
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $localidades = Localidad::where('nombre', 'LIKE', "%$keyword%")
                ->orWhere('provincia_id', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $localidades = Localidad::latest()->paginate($perPage);
        }

        return view('backend.superadmin.localidades.index', compact('localidades'));
    }

    public function create(){
        $provincias = Provincia::getProvinciasArray();

        return view('backend.superadmin.localidades.create', compact('provincias'));
    }

    public function store(Request $request){
        $this->validate($request, [
			'nombre' => 'required',
			'provincia_id' => 'required'
		]);
        $requestData = $request->all();
        
        Localidad::create($requestData);

        return redirect('admin/superadmin/localidades')->with('flash_message', 'Localidad Creada!');
    }

    public function show($id){
        $localidad = Localidad::findOrFail($id);

        return view('backend.superadmin.localidades.show', compact('localidad'));
    }

    public function edit($id){
        $localidad = Localidad::findOrFail($id);
        $provincias = Provincia::getProvinciasArray();

        return view('backend.superadmin.localidades.edit', compact('localidad','provincias'));
    }

    public function update(Request $request, $id){
        $this->validate($request, [
			'nombre' => 'required',
			'provincia_id' => 'required'
		]);
        $requestData = $request->all();
        
        $localidad = Localidad::findOrFail($id);
        $localidad->update($requestData);

        return redirect('admin/superadmin/localidades')->with('flash_message', 'Localidad Actualizada!');

    }

    public function destroy($id){
        Localidad::destroy($id);

        return redirect('admin/superadmin/localidades')->with('flash_message', 'Localidad Eliminada!');
    }
}
