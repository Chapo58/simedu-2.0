<?php

namespace App\Http\Controllers\Frontend\Sueldos;

use App\Http\Requests\Frontend\Sueldos\CreateLiquidacionRequest;
use App\Http\Requests\Frontend\Sueldos\UpdateLiquidacionRequest;
use App\Models\Frontend\Sueldos\Empleado;
use App\Models\Frontend\Sueldos\Liquidacion;
use App\Repositories\Frontend\Sueldos\LiquidacionRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Auth;

class LiquidacionController extends AppBaseController{
    private $liquidacionRepository;

    public function __construct(LiquidacionRepository $liquidacionRepo){
        $this->liquidacionRepository = $liquidacionRepo;
    }

    public function index(Request $request,$id){
        $this->liquidacionRepository->pushCriteria(new RequestCriteria($request));
        $liquidacions = $this->liquidacionRepository->all();
        $liquidacions = $liquidacions->where('empleado_id',$id);

        $empleado = Empleado::findOrFail($id);

        return view('frontend.sueldos.liquidaciones.index', compact('liquidacions','empleado'));
    }

    public function create($id){
        $empleado = Empleado::findOrFail($id);
        return view('frontend.sueldos.liquidaciones.create', compact('empleado'));
    }

    public function store(CreateLiquidacionRequest $request){
        $input = $request->all();

        $empleado = Empleado::findOrFail($request->empleado_id);

        $numero = Liquidacion::where('empleado_id',$empleado->id)->max('numero') + 1;

        $input['empleado_id'] = $empleado->id;
        $input['numero'] = $numero;

        $liquidacion = $this->liquidacionRepository->create($input);

        $liquidacion->archivo_url = 'frontend/recibos_liquidaciones/'.$liquidacion->id.'.pdf';
        $liquidacion->save();

        $pdf = app('dompdf.wrapper');
        $pdf->getDomPDF()->set_option("enable_php", true);
        $pdf->loadView('frontend.sueldos.recibo', compact('empleado','liquidacion'))->save( 'frontend/recibos_liquidaciones/'.$liquidacion->id.'.pdf' );

        return $pdf->stream('Recibo.pdf');


//        Flash::success('Liquidacion creado correctamente.');
//        return redirect(route('frontend.sueldos.liquidaciones.index'));
    }

    public function show($id){
        $liquidacion = $this->liquidacionRepository->findWithoutFail($id);

        if (empty($liquidacion)) {
            Flash::error('Liquidacion no encontrado.');

            return redirect(route('sueldos.liquidaciones.index'));
        }

        return view('frontend.sueldos.liquidaciones.show', compact('liquidacion'));
    }

    public function edit($id){
        $liquidacion = $this->liquidacionRepository->findWithoutFail($id);

        if (empty($liquidacion)) {
            Flash::error('Liquidacion no encontrado.');

            return redirect(route('frontend.sueldos.liquidaciones.index'));
        }

        return view('frontend.sueldos.liquidaciones.edit', compact('liquidacion'));
    }

    public function update($id, UpdateLiquidacionRequest $request){
        $liquidacion = $this->liquidacionRepository->findWithoutFail($id);

        if (empty($liquidacion)) {
            Flash::error('Liquidacion no encontrado.');

            return redirect(route('frontend.sueldos.liquidaciones.index'));
        }

        $liquidacion = $this->liquidacionRepository->update($request->all(), $id);

        Flash::success('Liquidacion actualizado correctamente.');

        return redirect(route('frontend.sueldos.liquidaciones.index'));
    }

    public function destroy($id){
        $liquidacion = $this->liquidacionRepository->findWithoutFail($id);

        if (empty($liquidacion)) {
            Flash::error('Liquidacion no encontrado.');

            return redirect(route('frontend.sueldos.liquidaciones.index'));
        }

        $this->liquidacionRepository->delete($id);

        Flash::success('Liquidacion eliminado correctamente.');

        return redirect(route('frontend.sueldos.liquidaciones.index'));
    }
}
