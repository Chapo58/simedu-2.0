<?php

namespace App\Http\Controllers\Frontend\Sueldos;

use App\Http\Controllers\AppBaseController;
use App\Models\Frontend\Sueldos\Codigo;
use App\Models\Frontend\Sueldos\Empleado;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class CodigoEmpleadoController extends AppBaseController{

    public function index($idEmpleado){
        $empleado = Empleado::findOrFail($idEmpleado);

        return view('frontend.sueldos.codigos_empleado.index')
            ->with('empleado', $empleado);
    }

    public function create($idEmpleado){
        $empleado = Empleado::findOrFail($idEmpleado);
        $codigos = Codigo::getCodigosArray();
        return view('frontend.sueldos.codigos_empleado.create', compact('empleado','codigos'));
    }

    public function store(Request $request){
        $empleado = Empleado::findOrFail($request->empleado_id);
        $codigo = Codigo::findOrFail($request->codigo_id);

        $input = $request->all();

        $empleado->codigos()->attach($input['codigo_id'], ['cantidad' => $input['cantidad'], 'multiplica' => $input['multiplica'], 'divide' => $input['divide']]);

        Flash::success('Codigo creado correctamente.');

        return redirect('sueldos/codigos_empleado/' . $empleado->id);
    }

    public function edit($idEmpleado,$idCodigo){
        $empleado = Empleado::findOrFail($idEmpleado);
        $codigo = Codigo::findOrFail($idCodigo);
        $codigoEmpleado = $empleado->codigos()->where('codigo_id', $codigo->id)->first();
        $codigos = Codigo::getCodigosArray();
     //   dd($codigoEmpleado->id);

        return view('frontend.sueldos.codigos_empleado.edit', compact('empleado','codigo','codigos','codigoEmpleado'));
    }

    public function update(Request $request){
        $empleado = Empleado::findOrFail($request->empleado_id);

        $input = $request->all();
        $input = array_except($input,['_token','codigo_actual_id','empleado_id']);

        $empleado->codigos()->updateExistingPivot($request->codigo_actual_id, $input);

        Flash::success('Codigo actualizado correctamente.');

        return redirect('sueldos/codigos_empleado/' . $empleado->id);
    }

    public function destroy($idEmpleado,$idCodigo){
        $empleado = Empleado::findOrFail($idEmpleado);
        $codigo = Codigo::findOrFail($idCodigo);

        $empleado->codigos()->detach($codigo->id);

        Flash::success('Codigo eliminado correctamente.');

        return redirect('sueldos/codigos_empleado/' . $empleado->id);
    }

    public function obtenerCodigo(Request $request){
        $codigo = Codigo::findOrFail($request->codigo_id);

        $results = [];
        $results['cantidad'] = $codigo->cantidad;
        $results['multiplica'] = $codigo->multiplica;
        $results['divide'] = $codigo->divide;

        return response()->json($results);
    }

}
