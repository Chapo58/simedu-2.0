<?php

namespace App\Http\Controllers\Frontend\Sueldos;

use App\Http\Requests\Frontend\Sueldos\CreateCodigoRequest;
use App\Http\Requests\Frontend\Sueldos\UpdateCodigoRequest;
use App\Repositories\Frontend\Sueldos\CodigoRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Auth;

class CodigoController extends AppBaseController{

    private $codigoRepository;

    public function __construct(CodigoRepository $codigoRepo){
        $this->codigoRepository = $codigoRepo;
    }

    public function index(Request $request){
        $this->codigoRepository->pushCriteria(new RequestCriteria($request));
        $codigos = $this->codigoRepository->all();

        return view('frontend.sueldos.codigos.index')
            ->with('codigos', $codigos);
    }

    public function create(){
        return view('frontend.sueldos.codigos.create');
    }

    public function store(CreateCodigoRequest $request){
        $input = $request->all();

        $empresa = Auth::user()->alumno()->empresa;

        $input['empresa_id'] = $empresa->id;

        $codigo = $this->codigoRepository->create($input);

        Flash::success('Codigo creado correctamente.');

        return redirect(route('frontend.sueldos.codigos.index'));
    }

    public function show($id){
        $codigo = $this->codigoRepository->findWithoutFail($id);

        if (empty($codigo)) {
            Flash::error('Codigo no encontrado.');

            return redirect(route('sueldos.codigos.index'));
        }

        return view('frontend.sueldos.codigos.show')->with('codigo', $codigo);
    }

    public function edit($id){
        $codigo = $this->codigoRepository->findWithoutFail($id);

        if (empty($codigo)) {
            Flash::error('Codigo no encontrado.');

            return redirect(route('frontend.sueldos.codigos.index'));
        }

        return view('frontend.sueldos.codigos.edit')->with('codigo', $codigo);
    }

    public function update($id, UpdateCodigoRequest $request){
        $codigo = $this->codigoRepository->findWithoutFail($id);

        if (empty($codigo)) {
            Flash::error('Codigo no encontrado.');

            return redirect(route('frontend.sueldos.codigos.index'));
        }

        $codigo = $this->codigoRepository->update($request->all(), $id);

        Flash::success('Codigo actualizado correctamente.');

        return redirect(route('frontend.sueldos.codigos.index'));
    }

    public function destroy($id){
        $codigo = $this->codigoRepository->findWithoutFail($id);

        if (empty($codigo)) {
            Flash::error('Codigo no encontrado.');

            return redirect(route('frontend.sueldos.codigos.index'));
        }

        $this->codigoRepository->delete($id);

        Flash::success('Codigo eliminado correctamente.');

        return redirect(route('frontend.sueldos.codigos.index'));
    }
}
