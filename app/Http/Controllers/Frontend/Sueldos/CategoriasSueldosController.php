<?php

namespace App\Http\Controllers\Frontend\Sueldos;

use App\Http\Requests\Frontend\Sueldos\CreateCategoriasSueldosRequest;
use App\Http\Requests\Frontend\Sueldos\UpdateCategoriasSueldosRequest;
use App\Repositories\Frontend\Sueldos\CategoriasSueldosRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Auth;

class CategoriasSueldosController extends AppBaseController
{
    /** @var  CategoriasSueldosRepository */
    private $categoriasSueldosRepository;

    public function __construct(CategoriasSueldosRepository $categoriasSueldosRepo)
    {
        $this->categoriasSueldosRepository = $categoriasSueldosRepo;
    }

    /**
     * Display a listing of the CategoriasSueldos.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->categoriasSueldosRepository->pushCriteria(new RequestCriteria($request));
        $categoriasSueldos = $this->categoriasSueldosRepository->all();

        return view('frontend.sueldos.categorias_sueldos.index')
            ->with('categoriasSueldos', $categoriasSueldos);
    }

    /**
     * Show the form for creating a new CategoriasSueldos.
     *
     * @return Response
     */
    public function create()
    {
        return view('frontend.sueldos.categorias_sueldos.create');
    }

    /**
     * Store a newly created CategoriasSueldos in storage.
     *
     * @param CreateCategoriasSueldosRequest $request
     *
     * @return Response
     */
    public function store(CreateCategoriasSueldosRequest $request)
    {
        $input = $request->all();
        $input['empresa_id'] = Auth::user()->alumno()->empresa_actual_id;

        $categoriasSueldos = $this->categoriasSueldosRepository->create($input);

        Flash::success('Categoria creada correctamente.');

        return redirect(route('frontend.sueldos.categoriasSueldos.index'));
    }

    /**
     * Display the specified CategoriasSueldos.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $categoriasSueldos = $this->categoriasSueldosRepository->findWithoutFail($id);

        if (empty($categoriasSueldos)) {
            Flash::error('Categoria no encontrada.');

            return redirect(route('sueldos.categoriasSueldos.index'));
        }

        return view('frontend.sueldos.categorias_sueldos.show')->with('categoriasSueldos', $categoriasSueldos);
    }

    /**
     * Show the form for editing the specified CategoriasSueldos.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $categoriasSueldos = $this->categoriasSueldosRepository->findWithoutFail($id);

        if (empty($categoriasSueldos)) {
            Flash::error('Categoria no encontrada.');

            return redirect(route('frontend.sueldos.categoriasSueldos.index'));
        }

        return view('frontend.sueldos.categorias_sueldos.edit')->with('categoriasSueldos', $categoriasSueldos);
    }

    /**
     * Update the specified CategoriasSueldos in storage.
     *
     * @param  int              $id
     * @param UpdateCategoriasSueldosRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCategoriasSueldosRequest $request)
    {
        $categoriasSueldos = $this->categoriasSueldosRepository->findWithoutFail($id);

        if (empty($categoriasSueldos)) {
            Flash::error('Categoria no encontrada.');

            return redirect(route('frontend.sueldos.categoriasSueldos.index'));
        }

        $categoriasSueldos = $this->categoriasSueldosRepository->update($request->all(), $id);

        Flash::success('Categoria actualizada correctamente.');

        return redirect(route('frontend.sueldos.categoriasSueldos.index'));
    }

    /**
     * Remove the specified CategoriasSueldos from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $categoriasSueldos = $this->categoriasSueldosRepository->findWithoutFail($id);

        if (empty($categoriasSueldos)) {
            Flash::error('Categoria no encontrada.');

            return redirect(route('frontend.sueldos.categoriasSueldos.index'));
        }

        $this->categoriasSueldosRepository->delete($id);

        Flash::success('Categoria eliminada correctamente.');

        return redirect(route('frontend.sueldos.categoriasSueldos.index'));
    }
}
