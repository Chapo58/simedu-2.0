<?php

namespace App\Http\Controllers\Frontend\Sueldos;

use App\Http\Requests\Frontend\Sueldos\CreateEmpleadosRequest;
use App\Http\Requests\Frontend\Sueldos\UpdateEmpleadosRequest;
use App\Models\Backend\Superadmin\Localidad;
use App\Models\Backend\Superadmin\ObraSocial;
use App\Models\Backend\Superadmin\Pais;
use App\Models\Backend\Superadmin\Provincia;
use App\Models\Frontend\Sueldos\CategoriasSueldos;
use App\Repositories\Frontend\Sueldos\EmpleadosRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Auth;

class EmpleadosController extends AppBaseController{
    private $empleadosRepository;

    public function __construct(EmpleadosRepository $empleadosRepo){
        $this->empleadosRepository = $empleadosRepo;
    }


    public function index(Request $request){
        $this->empleadosRepository->pushCriteria(new RequestCriteria($request));
        $empleados = $this->empleadosRepository->all();

        return view('frontend.sueldos.empleados.index')
            ->with('empleados', $empleados);
    }


    public function create(){
        $paises = Pais::getPaisesArray();
        $provincias = Provincia::getProvinciasArray();
        $localidades = Localidad::getLocalidadesArray();
        $categorias = CategoriasSueldos::getCategoriasArray();
        $obras_sociales = ObraSocial::getObrasSocialesArray();
        $sexos = [1 => 'Masculino', 2 => 'Femenino'];
        $estados_civiles = [1 => 'Soltero', 2 => 'Casado', 3 => 'Divorciado', 4 => 'Viudo'];
        return view('frontend.sueldos.empleados.create', compact('sexos','paises','provincias','localidades','categorias','obras_sociales','estados_civiles'));
    }

    public function store(CreateEmpleadosRequest $request){
        $input = $request->all();
        $empresa = Auth::user()->alumno()->empresa;

        $input['empresa_id'] = $empresa->id;

        $empleado = $this->empleadosRepository->create($input);

        Flash::success('Empleado creado correctamente.');

        return redirect(route('frontend.sueldos.empleados.index'));
    }

    public function show($id){
        $empleado = $this->empleadosRepository->findWithoutFail($id);

        if (empty($empleado)) {
            Flash::error('Empleado no encontrado.');

            return redirect(route('sueldos.empleados.index'));
        }

        return view('frontend.sueldos.empleados.show')->with('empleado', $empleado);
    }

    public function edit($id){
        $empleado = $this->empleadosRepository->findWithoutFail($id);

        $paises = Pais::getPaisesArray();
        $provincias = Provincia::getProvinciasArray();
        $localidades = Localidad::getLocalidadesArray();
        $categorias = CategoriasSueldos::getCategoriasArray();
        $obras_sociales = ObraSocial::getObrasSocialesArray();
        $sexos = [1 => 'Masculino', 2 => 'Femenino'];
        $estados_civiles = [1 => 'Soltero', 2 => 'Casado', 3 => 'Divorciado', 4 => 'Viudo'];

        if (empty($empleado)) {
            Flash::error('Empleado no encontrado.');

            return redirect(route('frontend.sueldos.empleados.index'));
        }

        return view('frontend.sueldos.empleados.edit', compact('empleado','sexos','paises','provincias','localidades','categorias','obras_sociales','estados_civiles'));
    }

    public function update($id, UpdateEmpleadosRequest $request){
        $empleado = $this->empleadosRepository->findWithoutFail($id);

        if (empty($empleado)) {
            Flash::error('Empleado no encontrado.');

            return redirect(route('frontend.sueldos.empleados.index'));
        }

        $empleado = $this->empleadosRepository->update($request->all(), $id);

        Flash::success('Empleado actualizado correctamente.');

        return redirect(route('frontend.sueldos.empleados.index'));
    }

    public function destroy($id){
        $empleado = $this->empleadosRepository->findWithoutFail($id);

        if (empty($empleado)) {
            Flash::error('Empleado no encontrado.');

            return redirect(route('frontend.sueldos.empleados.index'));
        }

        $this->empleadosRepository->delete($id);

        Flash::success('Empleado eliminado correctamente.');

        return redirect(route('frontend.sueldos.empleados.index'));
    }
}
