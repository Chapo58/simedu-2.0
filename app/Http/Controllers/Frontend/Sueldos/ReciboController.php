<?php

namespace App\Http\Controllers\Frontend\Sueldos;

use App\Models\Frontend\Sueldos\Empleado;
use App\Http\Controllers\AppBaseController;
use Response;
use Auth;

class ReciboController extends AppBaseController{

    public function imprimir($id){
        $empleado = Empleado::findOrFail($id);

        $pdf = app('dompdf.wrapper');
        $pdf->getDomPDF()->set_option("enable_php", true);
        $pdf->loadView('frontend.sueldos.recibo', compact('empleado'));

        return $pdf->stream('Recibo.pdf');
    }

}
