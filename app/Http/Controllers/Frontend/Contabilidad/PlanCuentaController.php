<?php

namespace App\Http\Controllers\Frontend\Contabilidad;

use App\Http\Requests\Frontend\Contabilidad\CreatePlanCuentaRequest;
use App\Http\Requests\Frontend\Contabilidad\UpdatePlanCuentaRequest;
use App\Models\Frontend\Contabilidad\RubrosContables;
use App\Repositories\Frontend\Contabilidad\PlanCuentaRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Illuminate\Support\Facades\DB;
use Response;
use Auth;

class PlanCuentaController extends AppBaseController{

    private $planCuentaRepository;

    public function __construct(PlanCuentaRepository $planCuentaRepo){
        $this->planCuentaRepository = $planCuentaRepo;
    }

    public function index(Request $request){
        $this->planCuentaRepository->pushCriteria(new RequestCriteria($request));
        $planCuentas = $this->planCuentaRepository->all();
        $rubros = RubrosContables::$getArray;

        return view('frontend.contabilidad.plan_cuentas.index', compact('planCuentas','rubros'));
    }

    public function create(){
        $rubros = RubrosContables::$getArray;

        return view('frontend.contabilidad.plan_cuentas.create', compact('rubros'));
    }

    public function store(CreatePlanCuentaRequest $request){
        $input = $request->all();

        $empresa = Auth::user()->alumno()->empresa;

        $input['empresa_id'] = $empresa->id;

        $planCuenta = $this->planCuentaRepository->create($input);

        Flash::success('Cuenta creada correctamente.');

        return redirect(route('frontend.contabilidad.plan_cuentas.index'));
    }

    public function show($id){
        $planCuenta = $this->planCuentaRepository->findWithoutFail($id);

        if (empty($planCuenta)) {
            Flash::error('Cuenta no encontrada.');

            return redirect(route('contabilidad.plan_cuentas.index'));
        }

        return view('frontend.contabilidad.plan_cuentas.show')->with('planCuenta', $planCuenta);
    }

    public function edit($id){
        $planCuenta = $this->planCuentaRepository->findWithoutFail($id);
        $rubros = RubrosContables::$getArray;

        if (empty($planCuenta)) {
            Flash::error('Cuenta no encontrada.');

            return redirect(route('frontend.contabilidad.plan_cuentas.index'));
        }

        return view('frontend.contabilidad.plan_cuentas.edit', compact('planCuenta','rubros'));
    }

    public function update($id, UpdatePlanCuentaRequest $request){
        $planCuenta = $this->planCuentaRepository->findWithoutFail($id);

        if (empty($planCuenta)) {
            Flash::error('Cuenta no encontrada.');

            return redirect(route('frontend.contabilidad.plan_cuentas.index'));
        }

        $planCuenta = $this->planCuentaRepository->update($request->all(), $id);

        Flash::success('Cuenta actualizada correctamente.');

        return redirect(route('frontend.contabilidad.plan_cuentas.index'));
    }

    public function destroy($id){
        $planCuenta = $this->planCuentaRepository->findWithoutFail($id);

        if (empty($planCuenta)) {
            Flash::error('Cuenta no encontrada.');

            return redirect(route('frontend.contabilidad.plan_cuentas.index'));
        }

        $this->planCuentaRepository->delete($id);

        Flash::success('Cuenta eliminada correctamente.');

        return redirect(route('frontend.contabilidad.plan_cuentas.index'));
    }

    public function obtenerCuentas(Request $request){
        $term = $request->term;
        $codigo = $request->codigo;
        $empresa = Auth::user()->alumno()->empresa;

        $cuentas = DB::table('planes_cuentas')
            ->where('empresa_id', $empresa->id)
            ->where('imputable', 0)
            ->where('codigo', '<', $codigo)
            ->whereNull('deleted_at')
            ->where(function($q) use ($term) {
                $q->where('codigo', 'LIKE', "%$term%")
                    ->orWhere('denominacion', 'LIKE', "%$term%");
            })
            ->paginate(5);

        foreach ($cuentas as $cuenta) {
            $results[] = [
                'id'                    => $cuenta->id,
                'value'                 => $cuenta->codigo.' - '.$cuenta->denominacion
            ];
        }
        return response()->json($results);
    }
}
