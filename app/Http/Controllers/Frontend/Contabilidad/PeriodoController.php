<?php

namespace App\Http\Controllers\Frontend\Contabilidad;

use App\Http\Requests\Frontend\Contabilidad\CreatePeriodoRequest;
use App\Http\Requests\Frontend\Contabilidad\UpdatePeriodoRequest;
use App\Models\Frontend\Contabilidad\Periodo;
use App\Repositories\Frontend\Contabilidad\PeriodoRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Auth;

class PeriodoController extends AppBaseController{
    private $periodoRepository;

    public function __construct(PeriodoRepository $periodoRepo){
        $this->periodoRepository = $periodoRepo;
    }

    public function index(Request $request){
        $this->periodoRepository->pushCriteria(new RequestCriteria($request));
        $periodos = $this->periodoRepository->all();

        return view('frontend.contabilidad.periodos.index', compact('periodos'));
    }

    public function create(){
        return view('frontend.contabilidad.periodos.create');
    }

    public function store(CreatePeriodoRequest $request){
        $input = $request->all();

        $user = Auth::user();

        $input['user_id'] = $user->id;

        $periodo = $this->periodoRepository->create($input);

        Flash::success('Periodo creado correctamente.');

        return redirect(route('frontend.contabilidad.periodos.index'));
    }

    public function show($id){
        $periodo = $this->periodoRepository->findWithoutFail($id);

        if (empty($periodo)) {
            Flash::error('Periodo no encontrado.');

            return redirect(route('contabilidad.periodos.index'));
        }

        return view('frontend.contabilidad.periodos.show')->with('periodo', $periodo);
    }

    public function edit($id){
        $periodo = $this->periodoRepository->findWithoutFail($id);

        if (empty($periodo)) {
            Flash::error('Periodo no encontrado.');

            return redirect(route('frontend.contabilidad.periodos.index'));
        }

        return view('frontend.contabilidad.periodos.edit')->with('periodo', $periodo);
    }

    public function update($id, UpdatePeriodoRequest $request){
        $periodo = $this->periodoRepository->findWithoutFail($id);

        if (empty($periodo)) {
            Flash::error('Periodo no encontrado.');

            return redirect(route('frontend.contabilidad.periodos.index'));
        }

        $periodo = $this->periodoRepository->update($request->all(), $id);

        Flash::success('Periodo actualizado correctamente.');

        return redirect(route('frontend.contabilidad.periodos.index'));
    }

    public function destroy($id){
        $periodo = $this->periodoRepository->findWithoutFail($id);

        if (empty($periodo)) {
            Flash::error('Periodo no encontrado.');

            return redirect(route('frontend.contabilidad.periodos.index'));
        }

        $this->periodoRepository->delete($id);

        Flash::success('Periodo eliminado correctamente.');

        return redirect(route('frontend.contabilidad.periodos.index'));
    }

    public function seleccionarPeriodo($id){
        $periodo = Periodo::findOrFail($id);

        $user = Auth::user();
        $user->periodo_actual_id = $periodo->id;
        $user->save();

        Flash::success('Periodo seleccionado correctamente.');

        return redirect(route('frontend.contabilidad.periodos.index'));
    }
}
