<?php

namespace App\Http\Controllers\Frontend\Contabilidad;

use App\Http\Requests\Frontend\Contabilidad\CreateAsientoRequest;
use App\Http\Requests\Frontend\Contabilidad\UpdateAsientoRequest;
use App\Models\Frontend\Contabilidad\Asiento;
use App\Repositories\Frontend\Contabilidad\AsientoRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Auth;

class AsientoController extends AppBaseController{
    private $asientoRepository;

    public function __construct(AsientoRepository $asientoRepo){
        $this->asientoRepository = $asientoRepo;
    }

    public function index(Request $request){
        $this->asientoRepository->pushCriteria(new RequestCriteria($request));
        $asientos = $this->asientoRepository->all();

        return view('frontend.contabilidad.asientos.index', compact('asientos'));
    }

    public function create(){
        $empresa = Auth::user()->alumno()->empresa;
        $periodo = Auth::user()->periodo;

        $nro = Asiento::where('empresa_id',$empresa->id)
                ->where('periodo_id',$periodo->id)
                ->max('numero_lista') + 1;
        return view('frontend.contabilidad.asientos.create', compact('nro'));
    }

    public function store(CreateAsientoRequest $request){
        $input = $request->all();

        $empresa = Auth::user()->alumno()->empresa;
        $periodo = Auth::user()->periodo;

        $input['empresa_id'] = $empresa->id;
        $input['periodo_id'] = $periodo->id;

        $asiento = $this->asientoRepository->create($input);

        Flash::success('Asiento creado correctamente.');

        return redirect(route('frontend.contabilidad.asientos.index'));
    }

    public function show($id){
        $asiento = $this->asientoRepository->findWithoutFail($id);

        if (empty($asiento)) {
            Flash::error('Asiento no encontrado.');

            return redirect(route('contabilidad.asientos.index'));
        }

        return view('frontend.contabilidad.asientos.show', compact('asiento'));
    }

    public function edit($id){
        $asiento = $this->asientoRepository->findWithoutFail($id);

        if (empty($asiento)) {
            Flash::error('Asiento no encontrado.');

            return redirect(route('frontend.contabilidad.asientos.index'));
        }

        return view('frontend.contabilidad.asientos.edit', compact('asiento'));
    }

    public function update($id, UpdateAsientoRequest $request){
        $asiento = $this->asientoRepository->findWithoutFail($id);

        if (empty($asiento)) {
            Flash::error('Asiento no encontrado.');

            return redirect(route('frontend.contabilidad.asientos.index'));
        }

        $asiento = $this->asientoRepository->update($request->all(), $id);

        Flash::success('Asiento actualizado correctamente.');

        return redirect(route('frontend.contabilidad.asientos.index'));
    }

    public function destroy($id){
        $asiento = $this->asientoRepository->findWithoutFail($id);

        if (empty($asiento)) {
            Flash::error('Asiento no encontrado.');

            return redirect(route('frontend.contabilidad.asientos.index'));
        }

        $this->asientoRepository->delete($id);

        Flash::success('Asiento eliminado correctamente.');

        return redirect(route('frontend.contabilidad.asientos.index'));
    }
}
