<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Requests\Frontend\CreateCuentoRequest;
use App\Http\Requests\Frontend\UpdateCuentoRequest;
use App\Repositories\Frontend\CuentoRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class CuentoController extends AppBaseController{
    private $cuentoRepository;

    public function __construct(CuentoRepository $cuentoRepo){
        $this->cuentoRepository = $cuentoRepo;
    }

    public function index(Request $request){
        $this->cuentoRepository->pushCriteria(new RequestCriteria($request));
        $cuentos = $this->cuentoRepository->all();

        return view('frontend.cuentos.index')
            ->with('cuentos', $cuentos);
    }

    public function create(){
        return view('frontend.cuentos.create');
    }

    public function store(CreateCuentoRequest $request){
        $input = $request->all();

    //    $input['empresa_id'] = Auth::user()->empresa()->id;
        $input['empresa_id'] = 1;

        $cuento = $this->cuentoRepository->create($input);

        Flash::success('Cuento creado correctamente.');

        return redirect(route('frontend.cuentos.index'));
    }

    public function show($id){
        $cuento = $this->cuentoRepository->findWithoutFail($id);

        if (empty($cuento)) {
            Flash::error('Cuento no encontrado.');

            return redirect(route('cuentos.index'));
        }

        return view('frontend.cuentos.show')->with('cuento', $cuento);
    }

    public function edit($id){
        $cuento = $this->cuentoRepository->findWithoutFail($id);

        if (empty($cuento)) {
            Flash::error('Cuento no encontrado.');

            return redirect(route('frontend.cuentos.index'));
        }

        return view('frontend.cuentos.edit')->with('cuento', $cuento);
    }

    public function update($id, UpdateCuentoRequest $request){
        $cuento = $this->cuentoRepository->findWithoutFail($id);

        if (empty($cuento)) {
            Flash::error('Cuento no encontrado.');

            return redirect(route('frontend.cuentos.index'));
        }

        $cuento = $this->cuentoRepository->update($request->all(), $id);

        Flash::success('Cuento actualizado correctamente.');

        return redirect(route('frontend.cuentos.index'));
    }

    public function destroy($id){
        $cuento = $this->cuentoRepository->findWithoutFail($id);

        if (empty($cuento)) {
            Flash::error('Cuento no encontrado.');

            return redirect(route('frontend.cuentos.index'));
        }

        $this->cuentoRepository->delete($id);

        Flash::success('Cuento eliminado correctamente.');

        return redirect(route('frontend.cuentos.index'));
    }
}
