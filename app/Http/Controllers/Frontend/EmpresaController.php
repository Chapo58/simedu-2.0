<?php

namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\ImageController;
use App\Models\Backend\Direccion;
use App\Models\Backend\Superadmin\Localidad;
use App\Models\Backend\Superadmin\Pais;
use App\Models\Backend\Superadmin\Provincia;
use App\Models\Frontend\CondicionIva;
use App\Models\Frontend\Empresa;
use App\Repositories\Frontend\EmpresaRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Illuminate\Support\Facades\DB;
use Response;
use Auth;

class EmpresaController extends AppBaseController
{
    private $empresaRepository;

    public function __construct(EmpresaRepository $empresaRepo){
        $this->empresaRepository = $empresaRepo;
    }

    public function index(Request $request){
        $this->empresaRepository->pushCriteria(new RequestCriteria($request));
        $empresas = $this->empresaRepository->all();

        return view('frontend.empresas.index')
            ->with('empresas', $empresas);
    }

    public function create(){
        $condicionesIva = CondicionIva::$getArray;
        $paises = Pais::getPaisesArray();
        $provincias = Provincia::getProvinciasArray();
        $localidades = Localidad::getLocalidadesArray();

        return view('frontend.empresas.create', compact('condicionesIva','paises','provincias','localidades'));
    }

    public function store(Request $request){

        DB::transaction(function() use ($request) {
            $requestData = $request->all();
            $empresa = New Empresa($requestData);
            $alumno = Auth::user()->alumno();

            $empresa->alumno_id = $alumno->id;

            if($requestData['direccion']['direccion']){
                $direccion = New Direccion($requestData['direccion']);
                $direccion->save();
                $empresa->direccion_id = $direccion->id;
            }

            $controllerImagen = new ImageController();
            $imagen_url = $controllerImagen->guardarImagenEmpresa($request);
            if($imagen_url != false){
                $empresa->logo_url = $imagen_url;
            }

            $empresa->save();

        });

        Flash::success('Empresa creada correctamente.');

        return redirect(route('frontend.empresas.index'));
    }

    public function show($id){
        $empresa = $this->empresaRepository->findWithoutFail($id);

        if (empty($empresa)) {
            Flash::error('Empresa no encontrada.');

            return redirect(route('empresas.index'));
        }

        return view('frontend.empresas.show')->with('empresa', $empresa);
    }

    public function edit($id){
        $empresa = $this->empresaRepository->findWithoutFail($id);
        $condicionesIva = CondicionIva::$getArray;
        $paises = Pais::getPaisesArray();
        $provincias = Provincia::getProvinciasArray();
        $localidades = Localidad::getLocalidadesArray();

        if (empty($empresa)) {
            Flash::error('Empresa no encontrada.');

            return redirect(route('frontend.empresas.index'));
        }

        return view('frontend.empresas.edit', compact('empresa','condicionesIva','paises','provincias','localidades'));
    }

    public function update($id, Request $request){
        $empresa = $this->empresaRepository->findWithoutFail($id);

        if (empty($empresa)) {
            Flash::error('Empresa no encontrada.');

            return redirect(route('frontend.empresas.index'));
        }

        DB::transaction(function() use ($request,$empresa) {
            $requestData = $request->all();
            $empresa->update($requestData);

            if($requestData['direccion']['direccion']){
                if(isset($empresa->direccion)){
                    $empresa->direccion->update($requestData['direccion']);
                }else{
                    $direccion = New Direccion($requestData['direccion']);
                    $direccion->save();
                    $empresa->direccion_id = $direccion->id;
                }
            }

            $controllerImagen = new ImageController();
            $imagen_url = $controllerImagen->guardarImagenEmpresa($request);
            if($imagen_url != false){
                $empresa->logo_url = $imagen_url;
            }

            $empresa->save();
        });

        Flash::success('Empresa actualizada correctamente.');

        return redirect(route('frontend.empresas.index'));
    }

    public function destroy($id){
        $empresa = $this->empresaRepository->findWithoutFail($id);

        if (empty($empresa)) {
            Flash::error('Empresa no encontrada.');

            return redirect(route('frontend.empresas.index'));
        }

        $this->empresaRepository->delete($id);

        Flash::success('Empresa eliminada correctamente.');

        return redirect(route('frontend.empresas.index'));
    }

    public function seleccionarEmpresa($id_empresa){
        $empresa = Empresa::findOrFail($id_empresa);

        $alumno = Auth::user()->alumno();
        $alumno->empresa_actual_id = $empresa->id;
        $alumno->save();

        Flash::success('Empresa seleccionada correctamente.');

        return redirect(route('frontend.empresas.index'));
    }
}
