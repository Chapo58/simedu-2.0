<?php

namespace App\Models\Frontend\Sueldos;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Codigo extends Model{
    use SoftDeletes;

    public $table = 'codigos';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'empresa_id',
        'numero',
        'descripcion',
        'es_sueldo',
        'tipo_haberes',
        'obtencion',
        'cantidad',
        'multiplica',
        'divide',
        'sujeto_a_aguinaldo',
        'codigo_superior_id'
    ];

    protected $casts = [
        'empresa_id' => 'integer',
        'numero' => 'integer',
        'descripcion' => 'string',
        'tipo_haberes' => 'integer',
        'obtencion' => 'integer',
        'cantidad' => 'integer',
        'multiplica' => 'float',
        'divide' => 'float',
        'codigo_superior_id' => 'integer'
    ];

    public static $rules = [
        'numero' => 'required',
        'descripcion' => 'required|min:3'
    ];

    public static function getCodigos(){
        $codigos = Codigo::whereNull('deleted_at')->orderBy('numero','asc')->get();
        return $codigos;
    }

    public static function getCodigosArray(){
        $codigos = ['' => 'Seleccionar']+self::getCodigos()->pluck('descripcion', 'id')->all();
        return $codigos;
    }

    public function empleados(){
        return $this->belongsToMany(Empleado::class)
            ->withPivot('multiplica', 'divide','cantidad')
            ->withTimestamps();
    }

    public function __toString(){
        return $this->numero.' - '.$this->descripcion;
    }

}
