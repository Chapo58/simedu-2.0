<?php

namespace App\Models\Frontend\Sueldos;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class CategoriasSueldos
 * @package App\Models\Sueldos
 * @version April 24, 2019, 10:41 pm UTC
 */
class CategoriasSueldos extends Model
{
    use SoftDeletes;

    public $table = 'categorias_sueldos';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'empresa_id',
        'nombre',
        'descripcion'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'empresa_id' => 'integer',
        'nombre' => 'string',
        'descripcion' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nombre' => 'required|min:3'
    ];

    public static function getCategorias(){
        $categorias = CategoriasSueldos::whereNull('deleted_at')->orderBy('nombre','asc')->get();
        return $categorias;
    }

    public static function getCategoriasArray(){
        $categorias = self::getCategorias()->pluck('nombre', 'id')->all();
        return $categorias;
    }

    public function __toString(){
        return $this->nombre;
    }

    
}
