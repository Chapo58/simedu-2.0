<?php

namespace App\Models\Frontend\Sueldos;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Liquidacion extends Model{
    use SoftDeletes;

    public $table = 'liquidaciones';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'empleado_id',
        'numero',
        'periodo',
        'archivo_url'
    ];

    protected $casts = [
        'id' => 'integer',
        'empleado_id' => 'integer',
        'numero' => 'integer',
        'periodo' => 'date',
        'archivo_url' => 'string'
    ];

    public static $rules = [
        'periodo' => 'required'
    ];

    

    public function __toString(){
        return $this->id;
    }
}
