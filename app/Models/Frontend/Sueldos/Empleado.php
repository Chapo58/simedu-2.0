<?php

namespace App\Models\Frontend\Sueldos;

use App\Models\Backend\Superadmin\ObraSocial;
use App\Models\Frontend\Empresa;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Empleado extends Model{
    use SoftDeletes;

    public $table = 'empleados';

    protected $dates = ['deleted_at','fecha_ingreso','fecha_nacimiento'];

    public $fillable = [
        'empresa_id',
        'nombre',
        'apellido',
        'sexo',
        'fecha_ingreso',
        'estado_civil',
        'cuit',
        'fecha_nacimiento',
        'direccion_id',
        'categoria_id',
        'obra_social_id',
        'telefono',
        'email',
        'telefono',
        'legajo',
        'cargo',
        'observaciones'
    ];

    protected $casts = [
        'empresa_id' => 'integer',
        'nombre' => 'string',
        'apellido' => 'string',
        'sexo' => 'integer',
        'fecha_ingreso' => 'date',
        'estado_civil' => 'integer',
        'cuit' => 'string',
        'fecha_nacimiento' => 'date',
        'direccion_id' => 'integer',
        'categoria_id' => 'integer',
        'obras_sociales_id' => 'integer',
        'telefono' => 'string',
        'email' => 'string',
        'observaciones' => 'string'
    ];

    public static $rules = [
        'nombre' => 'required|min:3',
        'apellido' => 'required|min:3',
        'cuit' => 'required|min:8'
    ];

    public function empresa(){
        return $this->belongsTo(Empresa::class);
    }

    public function categoria(){
        return $this->belongsTo(CategoriasSueldos::class);
    }

    public function obra_social(){
        return $this->belongsTo(ObraSocial::class);
    }

    public function codigos(){
        return $this->belongsToMany(Codigo::class)
            ->withPivot('multiplica', 'divide','cantidad')
            ->withTimestamps();
    }

    public function __toString(){
        return $this->nombre . ' ' . $this->apellido;
    }

}
