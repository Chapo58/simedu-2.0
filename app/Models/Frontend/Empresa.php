<?php

namespace App\Models\Frontend;

use App\Models\Backend\Alumno;
use App\Models\Backend\Direccion;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Empresa extends Model{
    use SoftDeletes;

    public $table = 'empresas';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'alumno_id',
        'razon_social',
        'cuit',
        'inicio_actividad',
        'direccion_id',
        'condicion_iva',
        'ingresos_brutos',
        'telefono',
        'web',
        'email',
        'contacto',
        'logo_url',
        'comienzo_asientos'
    ];

    protected $casts = [
        'alumno_id' => 'integer',
        'razon_social' => 'string',
        'cuit' => 'string',
        'inicio_actividad' => 'date',
        'direccion_id' => 'integer',
        'condicion_iva' => 'integer',
        'ingresos_brutos' => 'string',
        'telefono' => 'string',
        'web' => 'string',
        'email' => 'string',
        'contacto' => 'string',
        'logo_url' => 'string',
        'comienzo_asientos' => 'integer'
    ];

    public static $rules = [
        'razon_social' => 'required|min:3',
        'cuit' => 'required|min:8',
        'telefono' => 'min:5',
        'web' => 'min:5',
        'comienzo_asientos' => 'required'
    ];

    public function alumno(){
        return $this->belongsTo(Alumno::class);
    }

    public function direccion(){
        return $this->belongsTo(Direccion::class);
    }

    public function condicionIva(){
        if(isset($this->condicion_iva)){
            return CondicionIva::$getArray[$this->condicion_iva];
        }else{
            return '';
        }
    }

    public function __toString(){
        return $this->razon_social;
    }
    
}
