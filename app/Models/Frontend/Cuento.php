<?php

namespace App\Models\Frontend;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Cuento
 * @package App\Models
 * @version October 27, 2018, 10:33 pm UTC
 */
class Cuento extends Model
{
    use SoftDeletes;

    public $table = 'cuentos';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'alumno_id',
        'titulo',
        'cuento',
        'archivo_url'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'alumno_id' => 'integer',
        'titulo' => 'string',
        'cuento' => 'string',
        'archivo_url' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'titulo' => 'required|min:3',
        'cuento' => 'required|min:10'
    ];

    
}
