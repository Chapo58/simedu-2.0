<?php

namespace App\Models\Frontend\Contabilidad;

use App\Models\Frontend\Empresa;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Asiento extends Model{
    use SoftDeletes;

    public $table = 'asientos';
    

    protected $dates = ['deleted_at','fecha'];


    public $fillable = [
        'empresa_id',
        'periodo_id',
        'fecha',
        'denominacion',
        'numero_lista',
        'nota'
    ];

    protected $casts = [
        'id' => 'integer',
        'empresa_id' => 'integer',
        'periodo_id' => 'integer',
        'fecha' => 'date',
        'denominacion' => 'string',
        'numero_lista' => 'integer',
        'nota' => 'string'
    ];

    public static $rules = [
        'fecha' => 'required',
        'denominacion' => 'required|min:3',
        'numero_lista' => 'required'
    ];

    public function empresa(){
        return $this->belongsTo(Empresa::class);
    }

    public function periodo(){
        return $this->belongsTo(Periodo::class);
    }

    public function imputaciones(){
        return $this->hasMany(Imputacion::class);
    }

    public function __toString(){
        return $this->denominacion;
    }
}
