<?php

namespace App\Models\Frontend\Contabilidad;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Imputacion extends Model{
    use SoftDeletes;

    public $table = 'imputaciones';
    

    protected $dates = ['deleted_at'];

    public $fillable = [
        'asiento_id',
        'cuenta_id',
        'debe',
        'haber'
    ];

    public function asiento(){
        return $this->belongsTo(Asiento::class);
    }

    public function cuenta(){
        return $this->belongsTo(PlanCuenta::class,'cuenta_id');
    }

    public function __toString(){
        return $this->cuenta;
    }
}
