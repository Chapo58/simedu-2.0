<?php

namespace App\Models\Frontend\Contabilidad;

use Illuminate\Database\Eloquent\Model as Model;

class RubrosContables extends Model{

    const ACTIVO   = 0;
    const PASIVO   = 1;
    const PATRIMONIO_NETO   = 2;
    const INGRESOS_Y_GANANCIAS   = 3;
    const GASTOS_Y_PERDIDAS   = 4;
    const COSTO   = 5;

    public static $getArray = [
        self::ACTIVO   => 'Activo',
        self::PASIVO   => 'Pasivo',
        self::PATRIMONIO_NETO   => 'Patrimonio Neto',
        self::INGRESOS_Y_GANANCIAS   => 'Ingresos y Ganancias',
        self::GASTOS_Y_PERDIDAS   => 'Gastos y Perdidas',
        self::COSTO   => 'Costo',
    ];

}
