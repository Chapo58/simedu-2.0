<?php

namespace App\Models\Frontend\Contabilidad;

use App\Models\Frontend\Empresa;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class PlanCuenta extends Model{
    use SoftDeletes;

    public $table = 'planes_cuentas';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'empresa_id',
        'codigo',
        'denominacion',
        'rubro',
        'imputable',
        'cuenta_sumariza_id'
    ];

    protected $casts = [
        'id' => 'integer',
        'empresa_id' => 'integer',
        'codigo' => 'integer',
        'denominacion' => 'string',
        'cuenta_sumariza_id' => 'integer'
    ];

    public static $rules = [
        'codigo' => 'required',
        'denominacion' => 'required|min:3'
    ];

    public static function getCuentas(){
        $cuentas = PlanCuenta::whereNull('deleted_at')->orderBy('codigo','asc')->get();
        return $cuentas;
    }

    public static function getCuentasArray(){
        $cuentas = self::getCuentas()->pluck('denominacion', 'id')->all();
        return $cuentas;
    }

    public function empresa(){
        return $this->belongsTo(Empresa::class);
    }

    public function sumariza(){
        return $this->belongsTo(PlanCuenta::class,'cuenta_sumariza_id');
    }

    public function __toString(){
        return $this->codigo.' - '.$this->denominacion;
    }
    
}
