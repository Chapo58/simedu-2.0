<?php

namespace App\Models\Frontend\Contabilidad;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Periodo extends Model{
    use SoftDeletes;

    public $table = 'periodos';
    

    protected $dates = ['deleted_at','desde','hasta'];


    public $fillable = [
        'user_id',
        'periodo',
        'descripcion',
        'desde',
        'hasta'
    ];

    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'periodo' => 'string',
        'descripcion' => 'string',
        'desde' => 'date',
        'hasta' => 'date'
    ];

    public static $rules = [
        'periodo' => 'required',
        'desde' => 'required',
        'hasta' => 'required'
    ];

    public function __toString(){
        return $this->periodo;
    }

    
}
