<?php

namespace App\Models\Frontend;

use Illuminate\Database\Eloquent\Model as Model;

class CondicionIva extends Model{

    const MONOTRIBUTISTA = 1;
    const RESPONSABLE_INSCRIPTO     = 2;
    const RESPONSABLE_NO_INSCRIPTO   = 3;
    const EXCENTO = 4;
    const CONSUMIDOR_FINAL = 5;

    public static $getArray = [
        self::MONOTRIBUTISTA => 'Monotributista',
        self::RESPONSABLE_INSCRIPTO => 'Responsable Inscripto',
        self::RESPONSABLE_NO_INSCRIPTO     => 'Responsable no Inscripto',
        self::EXCENTO   => 'Excento',
        self::CONSUMIDOR_FINAL   => 'Consumidor Final',
    ];

}
