<?php

namespace App\Models\Auth;

use App\Models\Backend\Alumno;
use App\Models\Backend\Profesor;
use App\Models\Frontend\Contabilidad\Periodo;
use Auth;
use App\Models\Backend\Superadmin\Institucion;
use App\Models\Traits\Uuid;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Notifications\Notifiable;
use App\Models\Auth\Traits\Scope\UserScope;
use App\Models\Auth\Traits\Method\UserMethod;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Auth\Traits\SendUserPasswordReset;
use App\Models\Auth\Traits\Attribute\UserAttribute;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\Auth\Traits\Relationship\UserRelationship;

/**
 * Class User.
 */
class User extends Authenticatable
{
    use HasRoles,
        Notifiable,
        SendUserPasswordReset,
        SoftDeletes,
        UserAttribute,
        UserMethod,
        UserRelationship,
        UserScope,
        Uuid;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'avatar_type',
        'avatar_location',
        'password',
        'password_changed_at',
        'active',
        'confirmation_code',
        'confirmed',
        'timezone',
        'last_login_at',
        'last_login_ip',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * @var array
     */
    protected $dates = ['last_login_at', 'deleted_at'];

    /**
     * The dynamic attributes from mutators that should be returned with the user object.
     * @var array
     */
    protected $appends = ['full_name'];

    public function institucion()
    {
        $institucion= Institucion::where('user_id',Auth::user()->id)->first();
        return $institucion;
    }

    public function profesor()
    {
        $profesor= Profesor::where('user_id',Auth::user()->id)->first();
        return $profesor;
    }

    public function alumno()
    {
        $alumno= Alumno::where('user_id',Auth::user()->id)->first();
        return $alumno;
    }

    public function periodo(){
        return $this->belongsTo(Periodo::class,'periodo_actual_id');
    }

}
