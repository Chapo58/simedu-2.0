<?php

namespace App\Models\Backend\Superadmin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ObraSocial extends Model{

    use SoftDeletes;

    protected $table = 'obras_sociales';

    protected $primaryKey = 'id';

    protected $fillable = ['codigo', 'descripcion'];

    protected $dates = ['deleted_at'];

    public static function getObrasSociales(){
        $categorias = ObraSocial::whereNull('deleted_at')->orderBy('descripcion','asc')->get();
        return $categorias;
    }

    public static function getObrasSocialesArray(){
        $categorias = self::getObrasSociales()->pluck('descripcion', 'id')->all();
        return $categorias;
    }

    public function __toString(){
        return (string) $this->descripcion;
    }
}
