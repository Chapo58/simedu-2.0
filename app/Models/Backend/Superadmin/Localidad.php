<?php

namespace App\Models\Backend\Superadmin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Localidad extends Model{

    use SoftDeletes;

    protected $table = 'localidades';

    protected $primaryKey = 'id';

    protected $fillable = ['nombre', 'provincia_id'];

    protected $dates = ['deleted_at'];

    public function provincia(){
        return $this->belongsTo(Provincia::class);
    }

    public static function getLocalidades(){
        $localidades = Localidad::whereNull('deleted_at')->orderBy('nombre','asc')->get();
        return $localidades;
    }

    public static function getLocalidadesArray(){
        $localidades = self::getLocalidades()->pluck('nombre', 'id')->all();
        return $localidades;
    }

    public function __toString(){
        return (string) $this->nombre;
    }
}
