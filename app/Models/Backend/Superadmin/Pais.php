<?php

namespace App\Models\Backend\Superadmin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pais extends Model{

    use SoftDeletes;

    protected $table = 'paises';
    
    protected $primaryKey = 'id';

    protected $fillable = ['nombre', 'imagen_url'];

    protected $dates = ['deleted_at'];

    public function provincias(){
        return $this->hasMany(Provincia::class);
    }

    public static function getPaises(){
        $paises = Pais::whereNull('deleted_at')->orderBy('nombre','asc')->get();
        return $paises;
    }

    public static function getPaisesArray(){
        $paises = self::getPaises()->pluck('nombre', 'id')->all();
        return $paises;
    }

    public function __toString(){
        return (string) $this->nombre;
    }
}
