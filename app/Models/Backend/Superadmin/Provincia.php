<?php

namespace App\Models\Backend\Superadmin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Provincia extends Model{

    use SoftDeletes;

    protected $table = 'provincias';

    protected $primaryKey = 'id';

    protected $fillable = ['nombre', 'pais_id'];

    protected $dates = ['deleted_at'];

    public function pais(){
        return $this->belongsTo(Pais::class);
    }

    public static function getProvincias(){
        $provincias = Provincia::whereNull('deleted_at')->orderBy('nombre','asc')->get();
        return $provincias;
    }

    public static function getProvinciasArray(){
        $provincias = self::getProvincias()->pluck('nombre', 'id')->all();
        return $provincias;
    }

    public function __toString(){
        return (string) $this->nombre;
    }
}
