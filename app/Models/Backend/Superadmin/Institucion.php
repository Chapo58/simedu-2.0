<?php

namespace App\Models\Backend\Superadmin;

use App\Models\Auth\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Institucion extends Model{

    use SoftDeletes;

    protected $table = 'instituciones';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    protected $fillable = ['numero', 'nombre', 'direccion', 'telefono1', 'telefono2', 'email', 'user_id', 'logo', 'habilitado', 'nombre_director', 'telefono_director', 'email_director', 'direccion_director'];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'numero' => 'required|unique:instituciones',
        'nombre' => 'required',
        'email' => 'required|email|unique:users,email',
    ];

    protected $dates = ['deleted_at'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    

    public function __toString(){
        return (string) $this->id;
    }
}
