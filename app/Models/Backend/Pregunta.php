<?php

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Pregunta extends Model{

    use SoftDeletes;

    protected $table = 'preguntas';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    protected $fillable = ['pregunta', 'archivo', 'linkexterno', 'opcion1', 'opcion2', 'opcion3', 'opcion4', 'opcion5', 'opcion6', 'opcionCorrecta', 'planteo_id'];

    protected $dates = ['deleted_at'];

    public function planteo()
    {
        return $this->belongsTo('App\Planteo');
    }
    

    public function __toString(){
        return (string) $this->id;
    }
}
