<?php

namespace App\Models\Backend;

use App\Models\Auth\User;
use App\Models\Backend\Superadmin\Institucion;
use App\Models\Frontend\Empresa;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Alumno extends Model{

    use SoftDeletes;

    protected $table = 'alumnos';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    protected $fillable = [
        'nombre',
        'apellido',
        'email',
        'genero',
        'fecha_nacimiento',
        'telefono',
        'direccion',
        'user_id',
        'institucion_id',
        'facebook',
        'twitter',
        'instagram',
        'googlemas',
        'empresa_actual_id'];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nombre' => 'required',
        'apellido' => 'required',
        'email' => 'required|email|unique:users,email',
    ];

    protected $dates = ['deleted_at'];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function institucion(){
        return $this->belongsTo(Institucion::class);
    }

    public function empresa(){
        return $this->belongsTo(Empresa::class, 'empresa_actual_id');
    }

    public function __toString(){
        return (string) $this->nombre.' '.$this->apellido;
    }
}
