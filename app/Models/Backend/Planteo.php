<?php

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Planteo extends Model{

    use SoftDeletes;

    protected $table = 'planteos';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    protected $fillable = ['nombre', 'profesor_id', 'habilitado'];

    public static $rules = [
        'nombre' => 'required',
    ];

    protected $dates = ['deleted_at'];

    public function profesor()
    {
        return $this->belongsTo(Profesor::class);
    }

    public function preguntas()
    {
        return $this->hasMany(Pregunta::class, 'planteo_id');
    }

    public function __toString(){
        return (string) $this->id;
    }
}
