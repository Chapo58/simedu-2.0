<?php

namespace App\Models\Backend;

use App\Models\Auth\User;
use App\Models\Backend\Superadmin\Institucion;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Profesor extends Model{

    use SoftDeletes;

    protected $table = 'profesores';

    protected $primaryKey = 'id';

    protected $fillable = ['nombre', 'apellido', 'email', 'genero', 'fecha_nacimiento', 'telefono', 'direccion', 'user_id', 'institucion_id'];

    public static $rules = [
        'nombre' => 'required',
        'apellido' => 'required',
        'email' => 'required|email|unique:users,email',
    ];

    protected $dates = ['deleted_at'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function institucion()
    {
        return $this->belongsTo(Institucion::class);
    }
    

    public function __toString(){
        return (string) $this->id;
    }
}
