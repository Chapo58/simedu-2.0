<?php

namespace App\Models\Backend;

use App\Models\Backend\Superadmin\Institucion;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Curso extends Model{

    use SoftDeletes;

    protected $table = 'cursos';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    protected $fillable = ['nombre', 'institucion_id', 'habilitado'];

    public static $rules = [
        'nombre' => 'required',
    ];

    protected $dates = ['deleted_at'];

    public function institucion()
    {
        return $this->belongsTo(Institucion::class);
    }

    public function profesores()
    {
        return $this->belongsToMany('App\Models\Backend\Profesor', 'profesorxcurso', 'curso_id', 'profesor_id')->orderBy('profesor_id');
    }

    public function alumnos()
    {
        return $this->belongsToMany('App\Models\Backend\Alumno', 'alumnoxcurso', 'curso_id', 'alumno_id')->orderBy('alumno_id');
    }

    public function __toString(){
        return (string) $this->id;
    }
}
