<?php

namespace App\Models\Backend;

use App\Models\Backend\Superadmin\Localidad;
use App\Models\Backend\Superadmin\Pais;
use App\Models\Backend\Superadmin\Provincia;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Direccion extends Model{

    use SoftDeletes;

    protected $table = 'direcciones';

    protected $primaryKey = 'id';

    protected $fillable = ['direccion', 'codigo_postal', 'pais_id', 'provincia_id', 'localidad_id'];

    protected $dates = ['deleted_at'];

    public function pais(){
        return $this->belongsTo(Pais::class);
    }

    public function provincia(){
        return $this->belongsTo(Provincia::class);
    }

    public function localidad(){
        return $this->belongsTo(Localidad::class);
    }

    public function __toString(){
        return $this->direccion .' - '. $this->localidad .' - '. $this->provincia .' - '. $this->pais;
    }

}
