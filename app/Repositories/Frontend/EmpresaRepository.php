<?php

namespace App\Repositories\Frontend;

use App\Models\Frontend\Empresa;
use InfyOm\Generator\Common\BaseRepository;

class EmpresaRepository extends BaseRepository
{
    protected $fieldSearchable = [
        'razon_social',
        'cuit',
        'telefono',
        'web',
        'email',
        'contacto'
    ];

    public function model()
    {
        return Empresa::class;
    }
}
