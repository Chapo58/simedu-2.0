<?php

namespace App\Repositories\Frontend\Contabilidad;

use App\Models\Frontend\Contabilidad\Asiento;
use InfyOm\Generator\Common\BaseRepository;

class AsientoRepository extends BaseRepository{

    protected $fieldSearchable = [
        'fecha',
        'denominacion',
        'numero_lista',
        'nota'
    ];

    public function model(){
        return Asiento::class;
    }

}
