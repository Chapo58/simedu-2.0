<?php

namespace App\Repositories\Frontend\Contabilidad;

use App\Models\Frontend\Contabilidad\Periodo;
use InfyOm\Generator\Common\BaseRepository;

class PeriodoRepository extends BaseRepository{

    protected $fieldSearchable = [
        'periodo',
        'descripcion'
    ];

    public function model(){
        return Periodo::class;
    }
}
