<?php

namespace App\Repositories\Frontend\Contabilidad;

use App\Models\Frontend\Contabilidad\PlanCuenta;
use InfyOm\Generator\Common\BaseRepository;

class PlanCuentaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'codigo',
        'denominacion'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PlanCuenta::class;
    }
}
