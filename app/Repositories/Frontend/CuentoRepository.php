<?php

namespace App\Repositories\Frontend;

use App\Models\Frontend\Cuento;
use InfyOm\Generator\Common\BaseRepository;

class CuentoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'titulo',
        'cuento'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Cuento::class;
    }
}
