<?php

namespace App\Repositories\Frontend\Sueldos;

use App\Models\Frontend\Sueldos\Empleado;
use InfyOm\Generator\Common\BaseRepository;

class EmpleadosRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre',
        'apellido',
        'cuit',
        'telefono',
        'email',
        'telefono',
        'observaciones'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Empleado::class;
    }
}
