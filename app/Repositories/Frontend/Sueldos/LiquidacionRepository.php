<?php

namespace App\Repositories\Frontend\Sueldos;

use App\Models\Frontend\Sueldos\Liquidacion;
use InfyOm\Generator\Common\BaseRepository;

class LiquidacionRepository extends BaseRepository{

    protected $fieldSearchable = [
        'numero',
        'periodo'
    ];

    public function model(){
        return Liquidacion::class;
    }

}
