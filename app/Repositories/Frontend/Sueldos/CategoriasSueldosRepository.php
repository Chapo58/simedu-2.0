<?php

namespace App\Repositories\Frontend\Sueldos;

use App\Models\Frontend\Sueldos\CategoriasSueldos;
use InfyOm\Generator\Common\BaseRepository;

class CategoriasSueldosRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre',
        'descripcion'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CategoriasSueldos::class;
    }
}
