<?php

namespace App\Repositories\Frontend\Sueldos;

use App\Models\Frontend\Sueldos\Codigo;
use InfyOm\Generator\Common\BaseRepository;

class CodigoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'numero',
        'descripcion'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Codigo::class;
    }
}
