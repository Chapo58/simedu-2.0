@if (auth()->user() && session()->has("admin_user_id") && session()->has("temp_user_id"))
    <div class="alert alert-warning logged-in-as mb-0">
        Actualmente estas usando la cuenta de {{ auth()->user()->name }}. <a href="{{ route("frontend.auth.logout-as") }}">Volver al panel de administración</a>.
    </div><!--alert alert-warning logged-in-as-->
@endif