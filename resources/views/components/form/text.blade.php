<div class="form-group m-form__group row {{ $errors->has($name) ? ' has-danger' : '' }}">
    @if (isset($label))
        <label>{{$label}}</label>
    @else
        <label>{{$name}}</label>
    @endif

    <div class="m-input-icon m-input-icon--left">
        <span class="m-input-icon__icon m-input-icon__icon--left">
		    <span>
                <i class="{{$attributes["icon-class"]}}"></i>
			</span>
		</span>
        @if (count($attributes)>0)
            @if (array_key_exists('type',$attributes))
                @if ($attributes['type']=='password')
                    {{ Form::password($name, array_merge(['id' => str_replace('[]','',$name), 'class' => 'form-control m-input'], $attributes)) }}
                @elseif ($attributes['type']=='email')
                    {{ Form::email($name, $value, array_merge(['id' => str_replace('[]','',$name), 'class' => 'form-control m-input'], $attributes)) }}
                @elseif ($attributes['type']=='date')
                    {{ Form::date($name, $value, array_merge(['id' => str_replace('[]','',$name), 'class' => 'form-control m-input'], $attributes)) }}
                @else
                    {!! Form::number($name, $value, ['id' => str_replace('[]','',$name), 'class' => 'form-control m-input','maxlength' => '8']) !!}
                @endif
            @else
                {{ Form::text($name, $value, array_merge(['id' => str_replace('[]','',$name), 'class' => 'form-control m-input'], $attributes)) }}
            @endif
        @else
            {{ Form::text($name, $value, ['id' => str_replace('[]','',$name), 'class' => 'form-control']) }}
        @endif
    </div>

    @if ($errors->has($name))
        <span id="{{$name}}-error" class="form-control-feedback">{{ $errors->first($name) }}</span>
    @endif

</div>