@if(!empty($errors))
    @if($errors->any())
        @foreach($errors->all() as $error)
        <ul class="m-alert m-alert--icon alert alert-danger" role="alert">
            <div class="m-alert__icon">
                <i class="la la-warning"></i>
            </div>
            <div class="m-alert__text">{!! $error !!}</div>
            <div class="m-alert__close">
                <button type="button" class="close" data-close="alert" aria-label="Close">
                </button>
            </div>

        </ul>
        @endforeach
    @endif
@endif
