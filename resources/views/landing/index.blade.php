@extends('landing.layouts.app')

@section('title', app_name())

@section('content')

    <div class="content-wrapper">
        <div class="header-spacer"></div>
        <!-- Main Slider -->

        <div class="container-full-width">
            <div class="swiper-container main-slider" data-effect="fade" data-autoplay="4000">

                <!-- Additional required wrapper -->
                <div class="swiper-wrapper">
                    <!-- Slides -->
                    <div class="swiper-slide bg-border-color">

                        <div class="container">
                            <div class="row table-cell">

                                <div class="col-lg-12">

                                    <div class="slider-content align-center">

                                        <h1 class="slider-content-title" data-swiper-parallax="-100">Simedu</h1>
                                        <h5 class="slider-content-text c-gray" data-swiper-parallax="-200">Una experiencia interactiva y real para los alumnos</h5>

                                        <div class="main-slider-btn-wrap" data-swiper-parallax="-300">

                                            <a href="{{url('landing/nosotros')}}"
                                               class="btn btn-medium btn--dark btn-hover-shadow">
                                                <span class="text">Ver Mas</span>
                                                <span class="semicircle"></span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="slider-thumb" data-swiper-parallax="-400" data-swiper-parallax-duration="600">
                                        <img src="{{url('landing/img/slider1.png')}}" alt="slider">
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="swiper-slide bg-primary-color main-slider-bg-dark thumb-left">

                        <div class="container table full-height">
                            <div class="row table-cell">

                                <div class="col-lg-5 table-cell">
                                    <div class="slider-content">
                                        <h3 class="slider-content-title" data-swiper-parallax="-100"><span class="c-dark">Simedu</span>
                                            Simulador Educativo Empresarial y Contable.</h3>
                                        <h6 class="slider-content-text" data-swiper-parallax="-200">Un simulador educativo que brinda todas las herramientas pedagógicas que los docentes necesitan para llegar a sus alumnos de una forma simple e inolvidable.
                                        </h6>

                                    </div>
                                </div>

                                <div class="col-lg-7 table-cell">
                                    <div class="slider-thumb" data-swiper-parallax="-300" data-swiper-parallax-duration="500">
                                        <img src="{{url('landing/img/slider2.png')}}" alt="slider">
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide bg-secondary-color main-slider-bg-dark">

                        <div class="container table full-height">
                            <div class="row table-cell">
                                <div class="col-lg-6 table-cell">

                                    <div class="slider-content">

                                        <h3 class="h1 slider-content-title c-dark" data-swiper-parallax="-100">Educación Interactiva
                                        </h3>

                                        <h5 class="slider-content-text" data-swiper-parallax="-200">Uno de los métodos educativos más eficaces para facilitar el aprendizaje de los alumnos a través de la tecnología.
                                        </h5>

                                    </div>

                                </div>
                                <div class="col-lg-6 table-cell">
                                    <div class="slider-thumb" data-swiper-parallax="-300" data-swiper-parallax-duration="500">
                                        <img src="{{url('landing/img/slider3.png')}}" alt="slider">
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide bg-orange-color main-slider-bg-dark">
                        <div class="container table full-height">
                            <div class="row table-cell">

                                <div class="col-lg-12">

                                    <div class="slider-content align-center">
                                        <h3 class="h1 slider-content-title c-dark" data-swiper-parallax="-100">Experiencia Real</h3>
                                        <h5 class="slider-content-text" data-swiper-parallax="-200">Los alumnos pueden crear empresas, artículos, clientes, proveedores, empleados, planes de cuentas, llevar cajas, comprar y vender insumos en grupo y un sin fin de experiencias mas con las que adquirirán conocimientos prácticos y reales como futuros profesionales.
                                        </h5>

                                    </div>

                                </div>

                                <div class="col-lg-12">
                                    <div class="slider-thumb" data-swiper-parallax="-400" data-swiper-parallax-duration="600">
                                        <img src="{{url('landing/img/slider4.png')}}" alt="slider">
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide bg-green-color main-slider-bg-dark">

                        <div class="container table full-height">
                            <div class="row table-cell">

                                <div class="col-lg-6 table-cell">
                                    <div class="slider-content">

                                        <h3 class="h1 slider-content-title c-dark" data-swiper-parallax="-100">Proyecto Educativo
                                        </h3>

                                        <h5 class="slider-content-text" data-swiper-parallax="-200">Simedu fue pensado desde un primer momento como un gran proyecto educativo que brinde de forma lúdica y real herramientas para que las instituciones y los docentes puedan dar un enfoque renovador a sus alumnos.
                                        </h5>
                                    </div>
                                </div>

                                <div class="col-lg-6 table-cell">
                                    <div class="slider-thumb" data-swiper-parallax="-300" data-swiper-parallax-duration="500">
                                        <img src="{{url('landing/img/slider5.png')}}" alt="slider">
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <!--Prev next buttons-->

                <svg class="btn-next btn-next-black">
                    <use xlink:href="#arrow-right"></use>
                </svg>

                <svg class="btn-prev btn-prev-black">
                    <use xlink:href="#arrow-left"></use>
                </svg>

                <!--Pagination tabs-->

                <div class="slider-slides">
                    <a href="#" class="slides-item bg-border-color main-slider-bg-light">
                        <div class="content">
                            <div class="text-wrap">
                                <h4 class="slides-title">SIMEDU</h4>
                            </div>
                            <div class="slides-number">01</div>
                        </div>
                    </a>

                    <a href="#" class="slides-item bg-primary-color">
                        <div class="content">
                            <div class="text-wrap">
                                <h4 class="slides-title">Acerca de</h4>
                                <div class="slides-sub-title">Simulador Educativo Empresarial y Contable.</div>
                            </div>
                            <div class="slides-number">02</div>
                        </div>
                    </a>

                    <a href="#" class="slides-item bg-secondary-color">
                        <div class="content">
                            <div class="text-wrap">
                                <h4 class="slides-title">Educación Interactiva</h4>
                            </div>
                            <div class="slides-number">03</div>
                        </div>
                    </a>

                    <a href="#" class="slides-item bg-orange-color">
                        <div class="content">
                            <div class="text-wrap">
                                <h4 class="slides-title">Experiencia Real</h4>
                            </div>
                            <div class="slides-number">04</div>
                        </div>
                    </a>

                    <a href="#" class="slides-item bg-green-color">
                        <div class="content">
                            <div class="text-wrap">
                                <h4 class="slides-title">Proyecto Educativo</h4>
                            </div>
                            <div class="slides-number">05</div>
                        </div>
                    </a>
                </div>
            </div>
        </div>


        <!-- ... End Main Slider -->


        <!-- Info-Box -->

        <div class="container info-boxes pt100 pb100">

            <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <div class="info-box--standard" data-mh="info-boxes">
                        <div class="info-box-image">
                            <img src="{{url('landing/img/info-box1.png')}}" alt="image">
                        </div>
                        <div class="info-box-content">
                            <h5 class="info-box-title">Modulo Empresarial</h5>
                            <p class="text">Articulos, Comprobantes, Ivas, Clientes, Proveedores, Caja y todas las herramientas que una empresa necesita para gestionarse.
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <div class="info-box--standard" data-mh="info-boxes">
                        <div class="info-box-image">
                            <img src="{{url('landing/img/info-box2.png')}}" alt="image">
                        </div>
                        <div class="info-box-content">
                            <h5 class="info-box-title">Modulo Contable</h5>
                            <p class="text">Plan de Cuentas, Asientos Contables, Libros Contables, Flujo de Fondos y todas las herramientas que utilizan los verdaderos contadores.
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <div class="info-box--standard" data-mh="info-boxes">
                        <div class="info-box-image">
                            <img src="{{url('landing/img/info-box3.png')}}" alt="image">
                        </div>
                        <div class="info-box-content">
                            <h5 class="info-box-title">Modulo Emprendedor</h5>
                            <p class="text">Investigationes demonstraverunt lectores
                                legere me lius quod ii legunt saepius notare.
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <div class="info-box--standard" data-mh="info-boxes">
                        <div class="info-box-image">
                            <img src="{{url('landing/img/info-box4.png')}}" alt="image">
                        </div>
                        <div class="info-box-content">
                            <h5 class="info-box-title">Bienes de Uso</h5>
                            <p class="text">Clientes, Actividades, Rubros, Grupos y Listados.
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <div class="info-box--standard" data-mh="info-boxes">
                        <div class="info-box-image">
                            <img src="{{url('landing/img/info-box5.png')}}" alt="image">
                        </div>
                        <div class="info-box-content">
                            <h5 class="info-box-title">Liquidación de Sueldos</h5>
                            <p class="text">Empleados, Codigos, Categorias e Informes, todas las herramientas necesarias para realizar una liquidación de sueldos real.
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <div class="info-box--standard" data-mh="info-boxes">
                        <div class="info-box-image">
                            <img src="{{url('landing/img/info-box6.png')}}" alt="image">
                        </div>
                        <div class="info-box-content">
                            <h5 class="info-box-title">Calculo de Costos</h5>
                            <p class="text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit,
                                sed diam nonummy nibh euismod.
                            </p>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <!-- ... End Info-Box -->


        <!-- SEO-Score -->

        <div class="container-fluid">
            <div class="row">

                <div class="seo-score scrollme">

                    <div class="container">
                        <div class="row">
                            <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-xs-12 col-sm-12">
                                <div class="seo-score-content align-center">

                                    <div class="heading align-center">
                                        <h4 class="h1 heading-title">¿Ya tienes tu usuario?</h4>
                                        <p class="heading-text">Inicia sesión para utilizar el sistema!</p>
                                    </div>

                                    <div class="seo-score-form">

                                        {{ html()->form('POST', route('frontend.auth.login.post'))->open() }}

                                            <div class="row">

                                                <div class="col-lg-6 no-padding col-md-12 col-xs-12 col-sm-12">
                                                    <input name="email" type="email" class="input-dark site" required="required" placeholder="Usuario">
                                                </div>
                                                <div class="col-lg-6 no-padding col-md-12 col-xs-12 col-sm-12">
                                                    <input name="password" type="password" class="input-dark e-mail" required="required" placeholder="Contraseña">
                                                </div>

                                            </div>

                                            <button type="submit" class="btn btn-medium btn--green btn-hover-shadow">
                                                <span class="text">Iniciar Sesión!</span>
                                                <span class="semicircle"></span>
                                            </button>

                                        {{ html()->form()->close() }}

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="images">
                        <img src="{{url('landing/img/seoscore1.png')}}" alt="image">
                        <img src="{{url('landing/img/seoscore2.png')}}" alt="image">
                        <img src="{{url('landing/img/seoscore3.png')}}" alt="image">
                    </div>

                </div>
            </div>
        </div>

        <!-- ... End SEO-Score -->


        <!-- Offers -->

        <div class="container">
            <div class="row medium-padding120">
                <div class="col-lg-12">
                    <div class="offers">
                        <div class="row">
                            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                                <div class="heading">
                                    <h4 class="h1 heading-title">Acerca del Simulador Educativo</h4>
                                    <div class="heading-line">
                                        <span class="short-line"></span>
                                        <span class="long-line"></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                <div class="heading">
                                    <p align="justify">
                                        Lo fundamental del proyecto es que desarrollamos un Recurso Educativo didáctico pedagógico para las carreras con orientación en Economía y Administración a través de un software web de simulación de gestión que permite a los estudiantes un primer contacto con el fascinante mundo de los negocios y del empresario proveyendo al sector un instrumento de aprendizaje vivencial.
                                    </p>
                                    <p align="justify">
                                        El fin último es que los alumnos aprendan divirtiéndose y viviendo una experiencia a pesar de que todo tenga como base una simulación. Guiados por ejercicios planteados por los profesores, los estudiantes podrán formar sus propias empresas virtuales y tener a su cargo sus decisiones.
                                    </p>
                                </div>

                                <a href="{{url('landing/nosotros')}}" class="btn btn-medium btn--primary btn-hover-shadow">
                                    <span class="text">Ver Mas</span>
                                    <span class="semicircle"></span>
                                </a>

                            </div>

                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                <div class="offers-thumb">
                                    <img src="{{url('landing/img/offers1.png')}}" alt="offers">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- ... End Offers -->


        <!-- Our-video -->

        <div class="container-fluid">
            <div class="row">
                <div class="our-video js-equal-child">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 no-padding">
                        <div class="video theme-module">
                            <div class="video-thumb">
                                <div class="overlay"></div>
                                <a href="https://www.youtube.com/watch?v=0O2aH4XLbto" class="video-control js-popup-iframe">
                                    <img src="svg/video-control.svg" alt="go">
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-12 col-xs-12 no-padding">
                        <div class="content theme-module centered-on-mobile medium-padding100">
                            <div class="heading">
                                <h4 class="h1 heading-title">Simedu en video</h4>
                                <div class="heading-line">
                                    <span class="short-line"></span>
                                    <span class="long-line"></span>
                                </div>
                                <p class="heading-text">Video promocional de Simedu.
                                </p>
                            </div>
                            <a href="{{url('landing/Proyecto.pdf')}}" target="_blank" class="btn btn-medium btn--secondary">
                                <span class="text">Mas informacion sobre el proyecto</span>
                                <span class="semicircle"></span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- ... End Offers -->


        <!-- Background-mountains -->

        <div class="container-fluid">
            <div class="row">
                <div class="background-mountains medium-padding120 scrollme">

                    <div class="images">
                        <img src="{{url('landing/img/mountain1.png')}}" alt="mountain">
                        <img src="{{url('landing/img/mountain2.png')}}" alt="mountain">
                    </div>

                    <div class="container">
                        <div class="row">
                            <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                                <div class="heading align-center">
                                    <h4 class="h1 heading-title">Algunas Caracteristicas</h4>
                                    <div class="heading-line">
                                        <span class="short-line"></span>
                                        <span class="long-line"></span>
                                    </div>
                                    <p class="heading-text">Simedu tiene mucho para ofrecerles, pero aqui resumimos algunas cualidades.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                <div class="info-box--standard-centered">
                                    <div class="info-box-image">
                                        <img src="{{url('landing/img/info-box7.png')}}" alt="image">
                                    </div>
                                    <div class="info-box-content">
                                        <h4 class="info-box-title">Multiplataforma</h4>
                                        <p class="text">Puede utilizarse desde cualquier parte y en cualquier dispositivo.
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                <div class="info-box--standard-centered">
                                    <div class="info-box-image">
                                        <img src="{{url('landing/img/info-box8.png')}}" alt="image">
                                    </div>
                                    <div class="info-box-content">
                                        <h4 class="info-box-title">Intuitivo</h4>
                                        <p class="text">Facil de usar e intuitivo para los alumnos y docentes.
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                <div class="info-box--standard-centered">
                                    <div class="info-box-image">
                                        <img src="{{url('landing/img/info-box9.png')}}" alt="image">
                                    </div>
                                    <div class="info-box-content">
                                        <h4 class="info-box-title">Ludico</h4>
                                        <p class="text">El simulador se ideo con fines ludicos, cada paso es un juego.
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                <div class="info-box--standard-centered">
                                    <div class="info-box-image">
                                        <img src="{{url('landing/img/info-box10.png')}}" alt="image">
                                    </div>
                                    <div class="info-box-content">
                                        <h4 class="info-box-title">Informes</h4>
                                        <p class="text">Información agrupada y ordenada para una facil corrección.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>

        <!-- End Background-mountains -->


        <!-- Testimonial-slider -->

        <div class="container-fluid">
            <div class="row">
                <div class="testimonial-slider scrollme">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <div class="heading">
                                    <h4 class="h1 heading-title">Opiniones</h4>
                                    <div class="heading-line">
                                        <span class="short-line bg-yellow-color"></span>
                                        <span class="long-line bg-yellow-color"></span>
                                    </div>
                                    <p class="heading-text c-white">Simedu lleva años brindado herramientas educativas a las instituciones, aqui algunas de sus experiencias.
                                    </p>
                                </div>
                            </div>

                            <div class="col-lg-7 col-lg-offset-1 col-md-8 col-sm-12 col-xs-12">

                                <div class="testimonial-item">
                                    <!-- Slider main container -->
                                    <div class="swiper-container testimonial__thumb overflow-visible" data-effect="fade" data-loop="false">

                                        <div class="swiper-wrapper">
                                            <div class="testimonial-slider-item swiper-slide">
                                                <div class="testimonial-content">
                                                    <p class="text" data-swiper-parallax="-200">Excelente e innovadora forma de aprender administración y contabilidad. El simulador nos ayudo mucho.
                                                    </p>
                                                    <a href="#" class="author" data-swiper-parallax="-150">Belen Mosconi</a>
                                                    <a href="#" class="company" data-swiper-parallax="-150">Alumna</a>

                                                </div>
                                                <div class="avatar" data-swiper-parallax="-50">
                                                    <img src="{{url('landing/img/avatar3.png')}}" alt="avatar">
                                                </div>
                                            </div>
                                            <div class="testimonial-slider-item swiper-slide">
                                                <div class="testimonial-content">
                                                    <p class="text" data-swiper-parallax="-200">El simulador combina la dosis exacta de practica y teoría para que los docentes podamos ejemplificar todos los conceptos básicos y avanzados de administración y contabilidad.
                                                    </p>
                                                    <a href="#" class="author" data-swiper-parallax="-150">Mariano Solis</a>
                                                    <a href="#" class="company" data-swiper-parallax="-150">Docente</a>

                                                </div>
                                                <div class="avatar" data-swiper-parallax="-50">
                                                    <img src="{{url('landing/img/avatar.png')}}" alt="avatar">
                                                </div>
                                            </div>
                                            <div class="testimonial-slider-item swiper-slide">
                                                <div class="testimonial-content">
                                                    <p class="text" data-swiper-parallax="-200">Simedu fue clave para poder hacer nuestro trabajo final en tiempo y forma. Los informes son muy claros y precisos y nos ayudaron mucho para encontrar errores antes de presentar todo.
                                                    </p>
                                                    <a href="#" class="author" data-swiper-parallax="-150">Flavio Martinez</a>
                                                    <a href="#" class="company" data-swiper-parallax="-150">Alumno</a>

                                                </div>
                                                <div class="avatar" data-swiper-parallax="-50">
                                                    <img src="{{url('landing/img/avatar.png')}}" alt="avatar">
                                                </div>
                                            </div>
                                        </div>
                                        <!-- If we need pagination -->
                                        <div class="swiper-pagination"></div>

                                        <div class="quote">
                                            <i class="seoicon-quotes"></i>
                                        </div>
                                    </div>

                                    <div class="testimonial__thumb-img">
                                        <img src="{{url('landing/img/testimonial1.png')}}" alt="image">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="testimonial-img">
                        <img src="{{url('landing/img/testimonial2.png')}}" alt="image">
                    </div>
                </div>
            </div>
        </div>

        <!-- End Testimonial-slider -->

    </div>
@endsection
