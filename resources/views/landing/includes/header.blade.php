<header class="header" id="site-header">

    <div class="container">

        <div class="header-content-wrapper">

            <a href="#" id="top-bar-js" class="top-bar-link">TOPBAR</a>

            <div class="logo">
                <a href="{{url('/')}}" class="full-block-link"></a>
                <img src="{{url('landing/img/logo-eye.png')}}" alt="Simedu">
                <div class="logo-text">
                    <div class="logo-title">Simedu</div>
                    <div class="logo-sub-title">Simulador Educativo</div>
                </div>
            </div>

            <nav id="primary-menu" class="primary-menu">

                <a href='javascript:void(0)' id="menu-icon-trigger" class="menu-icon-trigger showhide">
                    <span class="mob-menu--title">Menu</span>
                    <span id="menu-icon-wrapper" class="menu-icon-wrapper" style="visibility: hidden">
                            <svg width="1000px" height="1000px">
                                <path id="pathD" d="M 300 400 L 700 400 C 900 400 900 750 600 850 A 400 400 0 0 1 200 200 L 800 800"></path>
                                <path id="pathE" d="M 300 500 L 700 500"></path>
                                <path id="pathF" d="M 700 600 L 300 600 C 100 600 100 200 400 150 A 400 380 0 1 1 200 800 L 800 200"></path>
                            </svg>
                        </span>
                </a>

                <!-- menu-icon-wrapper -->

                <ul class="primary-menu-menu">
                    <li class="menu-item-has-children">
                        <a href="{{url('/')}}">Inicio</a>
                    </li>
                    <li class="">
                        <a href="{{url('landing/nosotros')}}">Acerca de</a>
                    </li>
                    <li class="">
                        <a href="{{url('landing/Proyecto.pdf')}}" target="_blank">Proyecto</a>
                    </li>
                    <li class="">
                        <a href="{{url('landing/contacto')}}">Contacto</a>
                    </li>
                </ul>
            </nav>

            <div class="user-menu open-overlay">
                <a href="#" class="user-menu-content  js-open-aside">
                    <span></span>
                    <span></span>
                    <span></span>
                </a>
            </div>
        </div>

    </div>

</header>
<!-- ... End Header -->

<!-- Right-menu -->

<div class="mCustomScrollbar" data-mcs-theme="dark">

    <div class="popup right-menu">

        <div class="right-menu-wrap">

            <div class="user-menu-close js-close-aside">
                <a href="#" class="user-menu-content  js-clode-aside">
                    <span></span>
                    <span></span>
                </a>
            </div>

            <div class="logo">
                <a href="index.html" class="full-block-link"></a>
                <img src="{{url('landing/img/logo-eye.png')}}" alt="Seosight">
                <div class="logo-text">
                    <div class="logo-title">SIMEDU</div>
                </div>
            </div>

            <p class="text">Simulador Educativo Empresarial y Contable.
            </p>

        </div>
        @include('includes.partials.messages')
        <div class="widget login">

            {{ html()->form('POST', route('frontend.auth.login.post'))->open() }}
            <h4 class="login-title">Ingresa con tu cuenta</h4>
            <input class="email input-standard-grey" placeholder="Usuario" name="email" required="required" type="email">
            <input class="password input-standard-grey" placeholder="Contraseña" name="password" required="required" type="password">
            <div class="login-btn-wrap">

                <button type="submit" class="btn btn-medium btn--dark btn-hover-shadow">
                    <span class="text">Iniciar Sesión</span>
                    <span class="semicircle"></span>
                </button>

                <div class="remember-wrap">

                    <div class="checkbox">
                        <input id="remember" type="checkbox" name="remember" value="remember">
                        <label for="remember">Recordarme</label>
                    </div>
                </div>
            </div>
            {{ html()->form()->close() }}

            <a href="{{url('password/reset')}}" class="helped">¿Olvidaste tu contraseña?</a>
            {{--<a class="helped">Registrarse Ahora</a>--}}

        </div>



        <div class="widget contacts">

            <h4 class="contacts-title">Ponte en Contacto</h4>
            <p class="contacts-text">Si desea adquirir Simedu para su institución, pongase en contacto con nosotros!
            </p>

            <div class="contacts-item">
                <img src="{{url('landing/img/contact4.png')}}" alt="phone">
                <div class="content">
                    <a href="#" class="title">0353-1542314721</a>
                    <p class="sub-title">Lun-Vie 9am-8pm</p>
                </div>
            </div>

            <div class="contacts-item">
                <img src="{{url('landing/img/contact5.png')}}" alt="phone">
                <div class="content">
                    <a href="#" class="title">soporte@simedu.com.ar</a>
                    <p class="sub-title">Soporte Online</p>
                </div>
            </div>

            <div class="contacts-item">
                <img src="{{url('landing/img/contact6.png')}}" alt="phone">
                <div class="content">
                    <a href="#" class="title">Villa Maria, Argentina</a>
                    <!--<p class="sub-title">795 South Park Avenue</p>-->
                </div>
            </div>

        </div>

    </div>

</div>

<!-- ... End Right-menu -->