<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title', app_name())</title>
    <meta name="description" content="@yield('meta_description', 'Simulador Educativo')">
    <meta name="author" content="@yield('meta_author', 'Ciatt Software')">

    <?php
            /*
        if($_SERVER['SERVER_PORT'] !== 443 &&
            (empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] === 'off')) {
            header('Location: https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
            exit;
        }
        */
    ?>

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{{ asset('favicon.png') }}}">
@yield('meta')

@stack('before-styles')

    <link href="{{asset('landing/css/fonts.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('landing/css/crumina-fonts.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('landing/css/normalize.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('landing/css/grid.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('landing/css/base.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('landing/css/blocks.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('landing/css/layouts.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('landing/css/modules.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('landing/css/widgets-styles.css')}}" rel="stylesheet" type="text/css" />

    <!--Plugins styles-->
    <link href="{{asset('landing/css/jquery.mCustomScrollbar.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('landing/css/swiper.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('landing/css/primary-menu.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('landing/css/magnific-popup.css')}}" rel="stylesheet" type="text/css" />

    <!--External fonts-->
    <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet" type="text/css" />

@stack('after-styles')

</head>