@extends('landing.layouts.app')

@section('title', app_name().' | Acerca de')

@section('content')

<div class="header-spacer"></div>

<div class="content-wrapper">

<!-- Stunning header -->

<div class="stunning-header stunning-header-bg-lightblue">
    <div class="stunning-header-content">
        <h1 class="stunning-header-title">Acerca de Simedu</h1>
        <ul class="breadcrumbs">
            <li class="breadcrumbs-item">
                <a href="{{url('/')}}">Inicio</a>
                <i class="seoicon-right-arrow"></i>
            </li>
            <li class="breadcrumbs-item active">
                <span>Acerca de Simedu</span>
                <i class="seoicon-right-arrow"></i>
            </li>
        </ul>
    </div>
</div>

<!-- End Stunning header -->


<!--  Overlay Search -->

<div class="overlay_search">
    <div class="container">
        <div class="row">
            <div class="form_search-wrap">
                <form>
                    <input class="overlay_search-input" placeholder="Type and hit Enter..." type="text">
                    <a href="#" class="overlay_search-close">
                        <span></span>
                        <span></span>
                    </a>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- End Overlay Search -->

<div class="container">
    <div class="row pt120">
        <div class="col-lg-12">
            <div class="heading mb30">
                <h4 class="h1 heading-title">ACERCA DEL SIMULADOR EDUCATIVO</h4>
                <div class="heading-line">
                    <span class="short-line"></span>
                    <span class="long-line"></span>
                </div>

            </div>
        </div>

        <div align="justify" class="col-lg-12">
<p>Lo fundamental del proyecto es que desarrollamos un <span style="background: #4cc2c0;color: #fff;">Recurso Educativo</span> didáctico pedagógico para las carreras con orientación en <span style="background: #4cc2c0;color: #fff;">Economía y Administración</span> a través de un software web de simulación de gestión que permite a los estudiantes un primer contacto con el fascinante mundo de los negocios y del empresario proveyendo al sector un instrumento de aprendizaje vivencial. </p>
<p>El fin último es que los alumnos aprendan divirtiéndose y viviendo una experiencia a pesar de que todo tenga como base una simulación. Guiados por ejercicios planteados por los profesores, los estudiantes podrán formar sus propias empresas virtuales y tener a su cargo sus decisiones.</p>
<p>Generamos una interfaz simple de uso, muy intuitiva, que permita a cualquier alumnos o profesor operarla sin inconvenientes a nivel de navegación, concentrándose en el fin del programa y no en su operación .
Generamos una plataforma que pueda ser aplicada fácilmente a distintos entornos de negocios, agro, industrias, comerciales, de servicio, etc.
Generamos un producto multiusuario que permita gestionar fácilmente los accesos a grupos de alumnos.</p>
<p>El resultado tecnológico esperado del proyecto es desarrollar una solución web que permite complementar los procesos educativos teóricos sobre una aplicación práctica (simulada) de la gestión y administración y saber de sus ventajas.
Se desarrolló una solución en un entorno web que permite que sea accesible desde cualquier PC con conexión a internet (utilizando un navegador).
El desarrollo está basado en MySQL como motor de base de datos y diversos lenguajes como PHP, Javascript, Ajax y CSS para dejar como resultado un entorno intuitivo y visualmente agradable y sencillo.</p>
<p><strong><u>Los módulos de la solución que desarrollamos se detallan a continuación</u>:</strong></p>
<ul>
<li><strong>Modulo de Generación de la Empresa Virtual:</strong>
Se realiza una definición conceptual y a nivel informativo (razón social, nombre de fantasía, cuit, dirección, etc) de cada empresa, pudiendo definirse fichas conceptuales informativas de hasta 5 como máximo.</li>
<li><strong>Modulo de Generación de Vista Operativa de la Empresa Virtual:</strong>
Permite completar la información de la empresa, definir misión, visión, mercados y valores de la misma.</li>
<li><strong>Módulo de Contabilidad General:</strong>
Permite administrar un plan de cuentas, periodos fiscales, asientos contables y emitir el libro diario, mayor, balance general, balance de sumas y saldos y flujo de fondos.</li>
<li><strong>Módulo Ludico:</strong>
Permite definir planteos por docente, los cuales los alumnos a través de un juego van resolviendo. Los docentes pueden subir sus archivos (fotos, planillas, documentos, links, etc.). Los alumnos responden a las preguntas contenidas por los planteos y el docente evalúa los resultados de cada alumno. Preparado para todas las asignaturas, interactuando con herramientas como pueden ser un reloj, dados (multiopciones), un semáforo.
Teoría + Practica + Juego + Administración son los módulos educativos que permite a docentes y alumnos interactuar en forma dinámica con sus ideas, proyectos, pruebas, presentaciones, además de los módulos administrativos. Preparado para dispositivos móviles, tablets, y PCs.
</li>
<li><strong>Módulo de Gestión de nóminas de Clientes y Proveedores:</strong>
Permite definir y categorizar empresas según su fin, definiendo los proveedores necesarios para la generación de los productos y servicios y posibles clientes finales para la comercialización.</li>
<!--<li><strong>Módulo de generación de puntos de ventas:</strong> Permite capacitar y definir información como puntos de ventas, tipos de comprobantes que se van a usar para registrar las compras y ventas, modos de cargas y configuración técnica de la actividad de la empresa virtual.</li>-->
<li><strong>Módulos de Stock:</strong>
artículos lista de precios y un shopping virtual por cada empresa virtual.</li>
<li><strong>Módulo de Simulación Circuito de Compras:</strong>
Permite recorrer el circuito de compras y manejo de ingresos de materiales e insumos, conceptos como facturas de compras, remitos de ingresos de mercadería y presupuestos.</li>
<li><strong>Módulo de Simulación Circuito de Ventas:</strong>
Permite recorrer los circuitos de ventas,conceptos como factura de ventas, créditos, débitos presupuestos y notas de ventas.</li>
<li><strong>Módulos de Análisis de Indicadores y Reportes de Gestión:</strong>
Permite a los alumnos emitir reportes de gestión y poder analizarlos con los profesores. A partir de los resultados plantear nuevos ejercicios y estrategias. Permite capacitar en conceptos como Balance General, Sumas y saldos, flujo de Fondos, Etc.</li>
</ul>
        </div>

    </div>
</div>



<!-- Time line -->

<div class="container">

    <div class="row">

                    <div class="col-lg-12">

                        <section class="cd-horizontal-timeline">
                            <div class="timeline">
                                <div class="events-wrapper">
                                    <div class="events">
                                        <ol>
                                            <li><a href="#0" data-date="16/08/2003" class="selected">2001</a></li>
                                            <li><a href="#0" data-date="28/09/2004">2004</a></li>
                                            <li><a href="#0" data-date="28/01/2005">2005</a></li>
                                            <li><a href="#0" data-date="10/03/2006">2011</a></li>
                                            <li><a href="#0" data-date="10/07/2006">2012</a></li>
                                            <li><a href="#0" data-date="10/11/2006">2013</a></li>
                                            <li><a href="#0" data-date="20/11/2007">2018</a></li>
                                            <!-- other dates here -->
                                        </ol>

                                        <span class="filling-line" aria-hidden="true"></span>
                                    </div> <!-- .events -->
                                </div> <!-- .events-wrapper -->

                                <ul class="cd-timeline-navigation">
                                    <li><a href="#0" class="prev inactive seoicon-play">Ant</a></li>
                                    <li><a href="#0" class="next seoicon-play">Sig</a></li>
                                </ul> <!-- .cd-timeline-navigation -->
                            </div> <!-- .timeline -->

                            <div class="events-content">
                                <ol>
                                    <li class="selected" data-date="16/08/2003">
                                        <div class="row">
                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 table-cell">
                                                <div class="time-line-thumb">
                                                    <img src="{{url('landing/img/time-line-thumb.png')}}" alt="time-line">
                                                </div>
                                            </div>

                                            <div class="col-lg-8 col-lg-offset-1 col-md-8 col-md-offset-1 col-sm-9 col-xs-12 table-cell">
                                                <div class="time-line-content">
                                                    <h6 class="time-line-subtitle">Abr, 2001</h6>
                                                    <h5 class="time-line-title">Decision a futuro</h5>
                                                    <p class="time-line-text">Durante este año se tomo la decision de hacer un programa en internet que combinara tecnología, ideas y negocios utilizando herramientas que ya habia programado en años anteriores.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>

                                    </li>

                                    <li data-date="28/09/2004">

                                        <div class="row">
                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 table-cell">
                                                <div class="time-line-thumb">
                                                    <img src="{{url('landing/img/time-line-thumb.png')}}" alt="time-line">
                                                </div>
                                            </div>

                                            <div class="col-lg-8 col-lg-offset-1 col-md-8 col-md-offset-1 col-sm-9 col-xs-12 table-cell">
                                                <div class="time-line-content">
                                                    <h6 class="time-line-subtitle">Sept, 2004</h6>
                                                    <h5 class="time-line-title">Tecnologia, Ideas y Negocios</h5>
                                                    <p class="time-line-text">Se armo un servidor web y se lo llamo TIYN que significa Tecnologia, Ideas y Negocios. Este programa aun es utilizado con un subdominio de Simedu.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>

                                    </li>

                                    <li data-date="28/01/2005">

                                        <div class="row">
                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 table-cell">
                                                <div class="time-line-thumb">
                                                    <img src="{{url('landing/img/time-line-thumb.png')}}" alt="time-line">
                                                </div>
                                            </div>

                                            <div class="col-lg-8 col-lg-offset-1 col-md-8 col-md-offset-1 col-sm-9 col-xs-12 table-cell">
                                                <div class="time-line-content">
                                                    <h6 class="time-line-subtitle">Ene, 2005</h6>
                                                    <h5 class="time-line-title">Desarrollo TIYN</h5>
                                                    <p class="time-line-text">Se programaron todas las funcionalidades de este programa que funcionaba en un servidor propio y a modo de prueba.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>

                                    </li>

                                    <li class="" data-date="10/03/2006">
                                        <div class="row">
                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 table-cell">
                                                <div class="time-line-thumb">
                                                    <img src="{{url('landing/img/time-line-thumb.png')}}" alt="time-line">
                                                </div>
                                            </div>

                                            <div class="col-lg-8 col-lg-offset-1 col-md-8 col-md-offset-1 col-sm-9 col-xs-12 table-cell">
                                                <div class="time-line-content">
                                                    <h6 class="time-line-subtitle">Jul, 2011</h6>
                                                    <h5 class="time-line-title">Presentacion del Proyecto</h5>
                                                    <p class="time-line-text">Nos contactamos con una persona que nos ofrecio presentar un proyecto educativo en el ministerio de ciencia y tecnologia que consistia en realizar un simulador educativo web.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </li>

                                    <li data-date="10/07/2006">

                                        <div class="row">
                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 table-cell">
                                                <div class="time-line-thumb">
                                                    <img src="{{url('landing/img/time-line-thumb.png')}}" alt="time-line">
                                                </div>
                                            </div>

                                            <div class="col-lg-8 col-md-8 col-md-offset-1 col-sm-12 col-xs-12 col-lg-offset-1 table-cell">
                                                <div class="time-line-content">
                                                    <h6 class="time-line-subtitle">May, 2012</h6>
                                                    <h5 class="time-line-title">Proyecto Aprobado</h5>
                                                    <p class="time-line-text">El ministerio de ciencia y tecnologia aprobo el proyecto y con fondos no reembolsables se pudo crear simedu.com.ar
                                                    </p>
                                                </div>
                                            </div>
                                        </div>

                                    </li>

                                    <li data-date="10/11/2006">

                                        <div class="row">
                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 table-cell">
                                                <div class="time-line-thumb">
                                                    <img src="{{url('landing/img/time-line-thumb.png')}}" alt="time-line">
                                                </div>
                                            </div>

                                            <div class="col-lg-8 col-md-8 col-md-offset-1 col-sm-12 col-xs-12 col-lg-offset-1 table-cell">
                                                <div class="time-line-content">
                                                    <h6 class="time-line-subtitle">Jul, 2013</h6>
                                                    <h5 class="time-line-title">Simedu 1.0</h5>
                                                    <p class="time-line-text">Se creo simedu.com.ar version 1.0, la cual sigue funcionando hasta la actualidad.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>

                                    </li>

                                    <li data-date="20/11/2007">

                                        <div class="row">
                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 table-cell">
                                                <div class="time-line-thumb">
                                                    <img src="{{url('landing/img/time-line-thumb.png')}}" alt="time-line">
                                                </div>
                                            </div>

                                            <div class="col-lg-8 col-md-8 col-md-offset-1 col-sm-12 col-xs-12 col-lg-offset-1 table-cell">
                                                <div class="time-line-content">
                                                    <h6 class="time-line-subtitle">Ago, 2018</h6>
                                                    <h5 class="time-line-title">Simedu 2.0</h5>
                                                    <p class="time-line-text">Creamos simedu.info, simulador educativo lúdico, didáctico y vivencial. El cual pensamos terminar en Febrero de 2019. El mismo contiene una parte grafica, esta creado por institucion y sirve para combinar tecnologia, ideas, juegos y administracion bajo un entorno ludico que funciona para celulares, tablets y cualquier dispositivo digital sin necesidad de descargar ningun programa, tanto instituciones, docentes como alumnos pueden utilizarlo a travez de un nombre de usuario y una contraseña. Para mayor informacion visite  ayuda.simedu.com.ar y el juego de la vida.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>

                                    </li>

                                    <!-- other descriptions here -->
                                </ol>
                            </div> <!-- .events-content -->
                        </section>

                    </div>

                </div>

</div>

<!-- End Time line -->

<!-- Our vision -->

<div class="container-fluid">
    <div class="row bg-orangedark-color">

        <div class="our-vision scrollme">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                        <div class="heading align-center">
                            <h4 class="h1 heading-title">Nuestra Vision</h4>
                            <div class="heading-line">
                                <span class="short-line bg-yellow-color"></span>
                                <span class="long-line bg-yellow-color"></span>
                            </div>
                            <p class="heading-text c-white">Nuestra visión es que los estudiantes y docentes se diviertan aprendiendo teoría, practica y administración a través de diferentes juegos.
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <img src="{{url('landing/img/elements.png')}}" class="elements" alt="elements">
            <img src="{{url('landing/img/eye.png')}}" class="eye" alt="eye">
        </div>
    </div>
</div>


<!-- End Our vision -->


<!-- Testimonial slider -->


<div class="container-fluid">
    <div class="row medium-padding120 bg-border-color">
        <div class="container">

            <div class="testimonial-arrow-slider">

                <div class="row">

                    <div class="col-lg-12">
                        <div class="heading">
                            <h4 class="h1 heading-title">Comentarios de los Usuarios</h4>
                            <div class="heading-line">
                                <span class="short-line"></span>
                                <span class="long-line"></span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">

                    <div class="swiper-container pagination-bottom">

                        <div class="swiper-wrapper">

                            <div class="case-slider-item swiper-slide">

                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">

                                    <div class="testimonial-item testimonial-arrow mb30">
                                        <div class="testimonial-text">
                                            Excelente e innovadora forma de aprender administración y contabilidad. El simulador nos ayudo mucho.
                                        </div>

                                        <div class="author-info-wrap table">
                                            <div class="testimonial-img-author table-cell">
                                                <img src="{{url('landing/img/avatar3.png')}}" alt="author">
                                            </div>
                                            <div class="author-info table-cell">
                                                <h6 class="author-name">Belen Mosconi</h6>
                                                <div class="author-company c-primary">Alumna</div>
                                            </div>
                                        </div>

                                        <div class="quote">
                                            <i class="seoicon-quotes"></i>
                                        </div>

                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">

                                    <div class="testimonial-item testimonial-arrow mb30">
                                        <div class="testimonial-text">
                                            El simulador combina la dosis exacta de practica y teoría para que los docentes podamos ejemplificar todos los conceptos básicos y avanzados de administración y contabilidad..
                                        </div>

                                        <div class="author-info-wrap table">
                                            <div class="testimonial-img-author table-cell">
                                                <img src="{{url('landing/img/avatar.png')}}" alt="author">
                                            </div>
                                            <div class="author-info table-cell">
                                                <h6 class="author-name">Mariano Solis</h6>
                                                <div class="author-company c-primary">Docente</div>
                                            </div>
                                        </div>

                                        <div class="quote">
                                            <i class="seoicon-quotes"></i>
                                        </div>

                                    </div>
                                </div>

                            </div>

                            <div class="case-slider-item swiper-slide">

                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">

                                    <div class="testimonial-item testimonial-arrow mb30">
                                        <div class="testimonial-text">
                                            Simedu fue clave para poder hacer nuestro trabajo final en tiempo y forma. Los informes son muy claros y precisos y nos ayudaron mucho para encontrar errores antes de presentar todo.
                                        </div>

                                        <div class="author-info-wrap table">
                                            <div class="testimonial-img-author table-cell">
                                                <img src="{{url('landing/img/avatar.png')}}" alt="author">
                                            </div>
                                            <div class="author-info table-cell">
                                                <h6 class="author-name">Flavio Martinez</h6>
                                                <div class="author-company c-primary">Alumno</div>
                                            </div>
                                        </div>

                                        <div class="quote">
                                            <i class="seoicon-quotes"></i>
                                        </div>

                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">

                                    <div class="testimonial-item testimonial-arrow mb30">
                                        <div class="testimonial-text">
                                            Usamos Simedu para casi todo en clase y también para hacer las tareas en casa. Es muy intuitivo y los profesores nos evalúan al instante aunque no estemos en clase.
                                        </div>

                                        <div class="author-info-wrap table">
                                            <div class="testimonial-img-author table-cell">
                                                <img src="{{url('landing/img/avatar3.png')}}" alt="author">
                                            </div>
                                            <div class="author-info table-cell">
                                                <h6 class="author-name">Maria Perez</h6>
                                                <div class="author-company c-primary">Alumna</div>
                                            </div>
                                        </div>

                                        <div class="quote">
                                            <i class="seoicon-quotes"></i>
                                        </div>

                                    </div>
                                </div>

                            </div>

                        </div>

                        <!-- If we need pagination -->
                        <div class="swiper-pagination"></div>

                    </div>

                </div>

            </div>
        </div>
    </div>
</div>

<!-- End Testimonial slider -->

</div>

@endsection

@section('after-footer')
    <script src="{{asset('landing/js/time-line.js')}}"></script>
@endsection