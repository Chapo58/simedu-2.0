<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

@include('landing.includes.head')

<body class=" ">
    @include('landing.includes.header')

    @yield('content')

    @include('landing.includes.footer')

    @yield('after-footer')
</body>
</html>
