@section('before-styles')
    <style>
        * {
            padding: 0;
            margin: 0;
        }

        body {
            font-family: "Roboto", Arial;
            border: none;
            font-size: 14px;
            color: #222;
            background: #f5f6fa;
            line-height: 1.8
        }

        *,
        *::before,
        *::after {
            box-sizing: border-box;
        }

        h1 {
            text-align: center;
        }

        .container {
            margin: 150px auto;
            max-width: 960px;
        }

    </style>
@endsection

<div class="form-group {{ $errors->has('nombre') ? 'has-error' : ''}}">
    <label for="nombre" class="control-label">{{ 'Nombre' }}</label>
    <input class="form-control" name="nombre" type="text" id="nombre"
           value="{{isset($curso)? $curso->nombre :old('nombre') }}">
    {!! $errors->first('nombre', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('habilitado') ? 'has-error' : ''}}">
    <label for="habilitado" class="control-label">{{ 'Habilitado' }}</label>
    <div class="radio">
        <label>
            <input name="habilitado" type="radio" value="1" {{ (isset($curso) && 1 == $curso->habilitado) ? 'checked' : '' }}> Si</label>
    </div>
    <div class="radio">
        <label>
            <input name="habilitado" type="radio" value="0" @if (isset($curso)) {{ (0 == $curso->habilitado) ? 'checked' : '' }} @else {{ 'checked' }} @endif>
            No
        </label>
    </div>
    {!! $errors->first('habilitado', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group col-sm-6">
    <div>
        Profesores: <span id="selectedItemSpan"></span>
    </div>
    <div class="transfer"></div>
</div>

<div class="form-group col-sm-6">
    <div>
        Alumnos: <span id="selectedItemSpan2"></span>
    </div>
    <div class="transfer2"></div>
</div>

<div class="form-group col-sm-12">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Actualizar' : 'Crear' }}">
    <a href="{{ url('admin/%%routeGroup%%%%viewName%%') }}" title="Volver"
       class="btn btn-warning btn-sm float-right">
        <i class="fa fa-arrow-left" aria-hidden="true"></i> Volver
    </a>
</div>

@section('after-scripts')
    {{--Transfer Profesores--}}
    <script type="text/javascript">
        var profesores = [];
        @foreach($profesores as $profe)
            var nombreProfe = "<?php echo $profe->nombre . ' ' . $profe->apellido ?>";
            var id = "<?php echo $profe->id ?>";
            profesor= new Object();
            profesor['nombre']=nombreProfe;
            profesor['id']=id;
            profesores.push(profesor);
        @endforeach

        var profesorescurso = [];
        @if (isset($profesorescurso))
            @foreach($profesorescurso as $profecurso)
                var nombreProfeCurso = "<?php echo $profecurso->nombre . ' ' . $profecurso->apellido ?>";
                var idProfe = "<?php echo $profecurso->id ?>";
                profesorCurso= new Object();
                profesorCurso['nombre']=nombreProfeCurso;
                profesorCurso['id']=idProfe;
                profesorescurso.push(profesorCurso);
             @endforeach
        @endif


        var settings = {
            "inputId": "languageInput",
            "data": profesores,
            "profesorescurso": profesorescurso,
//            "groupData": groupData,
            "itemName": "nombre",
            "groupItemName": "groupName",
            "groupListName": "groupData",
            "container": "transfer",
            "valueName": "id",
            "callable": function (data, names) {
                console.log("Selected ID：" + data)
                $("#selectedItemSpan").text(names)
            }
        };

        Transfer.transfer(settings);
    </script>

    {{--Transfer Alumnos--}}
    <script type="text/javascript">
        var alumnos = [];
                @foreach($alumnos as $alum)
        var nombreAlumno = "<?php echo $alum->nombre . ' ' . $alum->apellido ?>";
        var id = "<?php echo $alum->id ?>";
        alumno= new Object();
        alumno['nombre']=nombreAlumno;
        alumno['id']=id;
        alumnos.push(alumno);
                @endforeach

        var alumnoscurso = [];
        @if (isset($alumnosscurso))
            @foreach($alumnosscurso as $alumcurso)
                var nombreAlumCurso = "<?php echo $alumcurso->nombre . ' ' . $alumcurso->apellido ?>";
                var idAlum = "<?php echo $alumcurso->id ?>";
                alumnoCurso= new Object();
                alumnoCurso['nombre']=nombreAlumCurso;
                alumnoCurso['id']=idAlum;
                alumnoscurso.push(alumnoCurso);
            @endforeach
        @endif

        var settings = {
                "inputId": "languageInput2",
                "data": alumnos,
                "alumnoscurso": alumnoscurso,
//            "groupData": groupData,
                "itemName": "nombre",
                "groupItemName": "groupName",
                "groupListName": "groupData",
                "container": "transfer2",
                "valueName": "id",
                "callable": function (data, names) {
                    console.log("Selected ID：" + data)
                    $("#selectedItemSpan2").text(names)
                }
            };

        Transfer2.transfer(settings);
    </script>
@endsection