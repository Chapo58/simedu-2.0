<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<img src="{{asset('web/images/logo.png')}}" style="max-width: 500px;"/>

<h2>¡Bienvenido a SIMEDU!</h2>

<div>
    <p>
        Estimado Asociado,
        le confirmamos que hemos creado para usted una cuenta en <a href="{{URL::to('/')}}">SIMEDU</a>.
        Es un placer darle la bienvenida, le agradecemos que cuente y confíe en nuestro servicio!
    </p>

    <p style="font-family: Avenir, Helvetica, sans-serif, serif, EmojiFont; color: rgb(116, 120, 126); font-size: 16px; line-height: 1.5em; margin-top: 0px; text-align: left;">
        Pulse aquí para verificar su cuenta:</p>
    <a href="{{URL::to('account/confirm/'.$usuario->confirmation_code)}}" target="_blank"
       rel="noopener noreferrer" data-auth="NotApplicable" class="x_button x_button-blue"
       style="font-family:Avenir,Helvetica,sans-serif; border-radius:3px; color:#FFF; display:inline-block; text-decoration:none; background-color:#3097D1; border-top:10px solid #3097D1; border-right:18px solid #3097D1; border-bottom:10px solid #3097D1; border-left:18px solid #3097D1">Confirmar
        Cuenta</a>


    <p>Estos son sus datos de acceso:</p>
    <ul>
        <li><strong>Usuario:</strong> {{$usuario->email}}</li>
        <li><strong>Contraseña:</strong> SIMEDU2018</li>
    </ul>

    <p>
        Le recomendamos que en su primer uso deberá modificar su contraseña.
    </p>

    <p>Saludos,<br/>
        El equipo de <strong>SIMEDU</strong>.</p>
</div>
</body>
</html>
