@extends ('backend.layouts.app')

@section ('title', app_name() . ' | Paises')

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                    Paises
                     </h4>
                </div><!--col-->
                <div class="col-sm-7">
                    <div class="btn-toolbar float-right" role="toolbar" aria-label="Toolbar with button groups">
                        <a href="{{ url('admin/superadmin/paises/create') }}" class="btn btn-success btn-lg ml-1" data-toggle="tooltip" title="Crear Nuevo" data-original-title="Agregar Nuevo Pais">
                            <i class="fas fa-plus-circle"></i>
                        </a>
                    </div>
                </div><!--col-->
            </div><!--row-->
            <div class="row mt-4">
                <div class="col">
                    <form method="GET" action="{{ url('admin/superadmin/paises') }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
                        <div class="input-group">
                            <input type="text" class="form-control" name="search" placeholder="Buscar..." value="{{ request('search') }}">
                            <span class="input-group-append">
                                <button class="btn btn-secondary" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                    </form>
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Imagen</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($paises as $item)
                                <tr>
                                    <td>{{ $item->nombre }}</td>
                                    <td>
                                        <img src="{{ isset($item->imagen_url) ? $item->imagen_url : '/img/no-image.png' }}" alt="{{ isset($pais) ? $pais : '' }}" width="50" style="border: 2px solid lightslategrey;">
                                    </td>
                                    <td>
                                        <a href="{{ url('admin/superadmin/paises/' . $item->id) }}" title="Ver Pais" class="btn btn-info btn-sm">
                                            <i class="fa fa-eye" aria-hidden="true"></i> Ver
                                        </a>
                                        <a href="{{ url('admin/superadmin/paises/' . $item->id . '/edit') }}" title="Editar Pais" class="btn btn-primary btn-sm">
                                            <i class="fa fa-edit" aria-hidden="true"></i> Editar
                                        </a>
                                        <form method="POST" action="{{ url('admin/superadmin/paises' . '/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                                            {{ method_field('DELETE') }}
                                            {{ csrf_field() }}
                                            <button type="submit" class="btn btn-danger btn-sm" title="Eliminar Pais" onclick="return confirm('¿Desea eliminar este registro?')">
                                                <i class="fa fa-trash" aria-hidden="true"></i> Eliminar
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="pagination-wrapper">
                            {!! $paises->appends(['search' => Request::get('search')])->render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
