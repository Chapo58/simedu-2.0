@extends ('backend.layouts.app')

@section ('title', app_name() . ' | Paises'))

@section('content')
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-5">
                        <h4 class="card-title mb-0">
                        Pais {{ $pais }}
                        </h4>
                    </div><!--col-->
                    <div class="col-sm-7">
                        <a href="{{ url('admin/superadmin/paises') }}" title="Volver" class="btn btn-warning btn-sm float-right">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Volver
                        </a>
                        <a href="{{ url('admin/superadmin/paises/' . $pais->id . '/edit') }}" title="Editar Paise" class="btn btn-primary btn-sm float-right">
                            <i class="fa fa-edit" aria-hidden="true"></i> Editar
                        </a>
                        <form method="POST" action="{{ url('admin/superadmin/paises' . '/' . $pais->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm float-right" title="Eliminar Paise" onclick="return confirm('¿Desea eliminar este registro?')">
                                <i class="fa fa-trash" aria-hidden="true"></i> Eliminar
                            </button>
                        </form>
                    </div>
                </div><!--row-->
                <br/>

                <div class="table-responsive">
                    <table class="table">
                        <tbody>
                            <tr>
                                <th> Nombre </th>
                                <td> {{ $pais->nombre }} </td>
                            </tr>
                            <tr>
                                <th> Imagen </th>
                                <td>
                                    <img src="{{ isset($pais->imagen_url) ? $pais->imagen_url : '/img/no-image.png' }}" alt="{{ isset($pais) ? $pais : '' }}" width="200" style="border: 2px solid lightslategrey;">
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>

@endsection
