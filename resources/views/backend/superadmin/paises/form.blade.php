<div class="form-group {{ $errors->has('nombre') ? 'has-error' : ''}}">
    <label for="nombre" class="control-label">{{ 'Nombre' }}</label>
    <input class="form-control" name="nombre" type="text" id="nombre" value="{{ $pais->nombre or ''}}" required>
    {!! $errors->first('nombre', '<p class="help-block">:message</p>') !!}
</div>
<div class="row">
    <div class="col-md-3">
        {!! Form::label('imagen_url', 'Imagen Actual') !!}
        <img src="{{ isset($pais->imagen_url) ? $pais->imagen_url : '/img/no-image.png' }}" alt="{{ isset($pais) ? $pais : '' }}" width="200" style="border: 2px solid lightslategrey;">
    </div>
    <div class="form-group col-md-9">
        <div class="form-group">
            <label>Seleccionar Imagen</label>
            <div class="row">
                <div class="col-md-8 col-sm-12">
                    <div class="input-group image-preview">
                        <input type="text" class="form-control image-preview-filename" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
                        <span class="input-group-btn">
                                    <!-- image-preview-clear button -->
                                    <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                                      <span class="glyphicon glyphicon-remove"></span> Limpiar
                                    </button>
                            <!-- image-preview-input -->
                                    <div class="btn btn-default image-preview-input">
                                      <span class="glyphicon glyphicon-folder-open"></span>
                                      <span class="image-preview-input-title">Buscar</span>
                                      <input type="file" name="image" accept=".png, .jpg, .jpeg">
                                    </div>
                                </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
