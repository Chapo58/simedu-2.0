@extends ('backend.layouts.app')

@section ('title', app_name() . ' | Paises')

@section('after-styles')
    <link href="{{asset('css/custom.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    <form method="POST" action="{{ url('admin/superadmin/paises/' . $pais->id) }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        Editar Pais -> {{ $pais }}
                    </h4>
                </div><!--col-->
                <div class="col-sm-7">
                     <a href="{{ url('admin/superadmin/paises') }}" title="Volver" class="btn btn-warning btn-sm float-right">
                         <i class="fa fa-arrow-left" aria-hidden="true"></i> Volver
                     </a>
                </div>
            </div><!--row-->

            <hr />

            @if ($errors->any())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif

                {{ method_field('PATCH') }}
                {{ csrf_field() }}

                @include ('backend.superadmin.paises.form', ['formMode' => 'edit'])

        </div>
        <div class="card-footer clearfix">
            <div class="row">
                <div class="col">
                    {{ form_cancel(route('admin.superadmin.paises.index'), __('buttons.general.cancel')) }}
                </div><!--col-->

                <div class="col text-right">
                    {{ form_submit(__('buttons.general.crud.edit')) }}
                </div><!--col-->
            </div><!--row-->
        </div><!--card-footer-->
    </div>
    </form>

@endsection

@section('after-scripts')
    <script src="{{asset('js/custom.js')}}"></script>
@endsection
