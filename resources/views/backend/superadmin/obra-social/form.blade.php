<div class="form-group {{ $errors->has('codigo') ? 'has-error' : ''}}">
    <label for="codigo" class="control-label">{{ 'Codigo' }}</label>
    <input class="form-control" name="codigo" type="text" id="codigo" value="{{isset($obrasocial)? $obrasocial->codigo :old('codigo') }}">
    {!! $errors->first('codigo', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('descripcion') ? 'has-error' : ''}}">
    <label for="descripcion" class="control-label">{{ 'Descripcion' }}</label>
    <input class="form-control" name="descripcion" type="text" id="descripcion" value="{{isset($obrasocial)? $obrasocial->descripcion :old('descripcion') }}">
    {!! $errors->first('descripcion', '<p class="help-block">:message</p>') !!}
</div>