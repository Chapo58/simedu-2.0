@extends ('backend.layouts.app')

@section ('title', app_name() . ' | Ver Obra Social')

@section('content')
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-5">
                        <h4 class="card-title mb-0">
                        Ver Obra Social {{ $obrasocial }}
                        </h4>
                    </div><!--col-->
                    <div class="col-sm-7">
                        <a href="{{ url('admin/superadmin/obra-social') }}" title="Volver" class="btn btn-warning btn-sm float-right">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Volver
                        </a>
                        <a href="{{ url('admin/superadmin/obra-social/' . $obrasocial->id . '/edit') }}" title="Editar Obra Social" class="btn btn-primary btn-sm float-right">
                            <i class="fa fa-edit" aria-hidden="true"></i> Editar
                        </a>
                        <form method="POST" action="{{ url('admin/superadmin/obra-social' . '/' . $obrasocial->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm float-right" title="Eliminar Obra Social" onclick="return confirm('¿Desea eliminar este registro?')">
                                <i class="fa fa-trash" aria-hidden="true"></i> Eliminar
                            </button>
                        </form>
                    </div>
                </div><!--row-->
                <br/>

                <div class="table-responsive">
                    <table class="table">
                        <tbody>
                            <tr>
                                <th> Codigo </th>
                                <td> {{ $obrasocial->codigo }} </td>
                            </tr>
                            <tr>
                                <th> Descripcion </th>
                                <td> {{ $obrasocial->descripcion }} </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>

@endsection
