@extends ('backend.layouts.app')

@section ('title', app_name() . ' | Localidades')

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        Editar Localidad -> {{ $localidad }}
                    </h4>
                </div><!--col-->
                <div class="col-sm-7">
                     <a href="{{ url('admin/superadmin/localidades') }}" title="Volver" class="btn btn-warning btn-sm float-right">
                         <i class="fa fa-arrow-left" aria-hidden="true"></i> Volver
                     </a>
                </div>
            </div><!--row-->

            <hr />

            @if ($errors->any())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif

            <form method="POST" action="{{ url('admin/superadmin/localidades/' . $localidad->id) }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                {{ method_field('PATCH') }}
                {{ csrf_field() }}

                @include ('backend.superadmin.localidades.form', ['formMode' => 'edit'])
            </form>

        </div>
    </div>

@endsection
