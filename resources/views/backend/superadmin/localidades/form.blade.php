<div class="form-group {{ $errors->has('nombre') ? 'has-error' : ''}}">
    <label for="nombre" class="control-label">Nombre</label>
    <input class="form-control" name="nombre" type="text" id="nombre" value="{{isset($localidad)? $localidad->nombre :old('nombre') }}">
    {!! $errors->first('nombre', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('provincia_id') ? 'has-error' : ''}}">
    <label for="provincia_id" class="control-label">Provincia</label>
    {!! Form::select('provincia_id', $provincias, isset($localidad->provincia) ? $localidad->provincia->id : '', array('class' => 'form-control')) !!}
    {!! $errors->first('provincia_id', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group col-sm-12">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Actualizar' : 'Crear' }}">
    <a href="{{ url('admin/superadmin/localidades') }}" title="Volver" class="btn btn-warning btn-sm float-right">
        <i class="fa fa-arrow-left" aria-hidden="true"></i> Volver
    </a>
</div>