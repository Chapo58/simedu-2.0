<div class="form-group {{ $errors->has('numero') ? 'has-error' : ''}}">
    <label for="numero" class="control-label">{{ 'Numero' }}</label>
    <input class="form-control" name="numero" type="number" id="numero"
           value="{{isset($institucion)? $institucion->numero : old('numero') }}">
    {!! $errors->first('numero', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('nombre') ? 'has-error' : ''}}">
    <label for="nombre" class="control-label">{{ 'Nombre' }}</label>
    <input class="form-control" name="nombre" type="text" id="nombre"
           value="{{ isset($institucion)? $institucion->nombre : old('nombre') }}">
    {!! $errors->first('nombre', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('direccion') ? 'has-error' : ''}}">
    <label for="direccion" class="control-label">{{ 'Dirección' }}</label>
    <input class="form-control" name="direccion" type="text" id="direccion"
           value="{{ isset($institucion)?$institucion->direccion: old('direccion')}}">
    {!! $errors->first('direccion', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('telefono1') ? 'has-error' : ''}}">
    <label for="telefono1" class="control-label">{{ 'Telefono1' }}</label>
    <input class="form-control" name="telefono1" type="text"
              id="telefono1" value="{{ isset($institucion)? $institucion->telefono1:old('telefono1')}}">
    {!! $errors->first('telefono1', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('telefono2') ? 'has-error' : ''}}">
    <label for="telefono2" class="control-label">{{ 'Telefono2' }}</label>
    <input class="form-control" name="telefono2" type="text"
              id="telefono2" value="{{ isset($institucion)? $institucion->telefono2:old('telefono2')}}">
    {!! $errors->first('telefono2', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
    <label for="email" class="control-label">{{ 'Email' }}</label>
    <input class="form-control" name="email" type="email" id="email"
           value="{{ isset($institucion)? $institucion->email :old('email')}}">
    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    @if (isset($institucion))
        {{ Form::image('uploads/instituciones/' . $institucion->logo ,'',['class' => 'thumb','style'=>'border: 2px solid lightsteelblue;', 'width' => 200, 'height' => 200])}}
    @else

    @endif
</div>

<div class="form-group {{ $errors->has('logo') ? 'has-error' : ''}}">
    <label for="logo" class="control-label">{{ 'Logo' }}</label>
    <input type="file" name="logo" id="logo" accept=".jpeg,.jpg,.png,.gif,.mp4,.mov,.ogg">
    {!! $errors->first('logo', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('habilitado') ? 'has-error' : ''}}">
    <label for="habilitado" class="control-label">{{ 'Habilitado' }}</label>
    <div class="radio">
        <label>
            <input name="habilitado" type="radio"
                   value="1" {{ (isset($institucion) && 1 == $institucion->habilitado) ? 'checked' : '' }}>
            Si
        </label>
    </div>
    <div class="radio">
        <label>
            <input name="habilitado" type="radio"
                   value="0" @if (isset($institucion)) {{ (0 == $institucion->habilitado) ? 'checked' : '' }} @else {{ 'checked' }} @endif>
            No
        </label>
    </div>
    {!! $errors->first('habilitado', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('nombre_director') ? 'has-error' : ''}}">
    <label for="nombre_director" class="control-label">{{ 'Nombre del Director' }}</label>
    <input class="form-control" name="nombre_director" type="text" id="nombre_director"
           value="{{ isset($institucion)? $institucion->nombre_director :old('nombre_director')}}">
    {!! $errors->first('nombre_director', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('telefono_director') ? 'has-error' : ''}}">
    <label for="telefono_director" class="control-label">{{ 'Telefono del Director' }}</label>
    <input class="form-control" name="telefono_director" type="text"
              id="telefono_director" value="{{ isset($institucion)? $institucion->telefono_director : old('telefono_director')}}">
    {!! $errors->first('telefono_director', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('email_director') ? 'has-error' : ''}}">
    <label for="email_director" class="control-label">{{ 'Email del Director' }}</label>
    <input class="form-control" name="email_director" type="email" id="email_director"
           value="{{ isset($institucion)? $institucion->email_director :  old('email_director')}}">
    {!! $errors->first('email_director', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('direccion_director') ? 'has-error' : ''}}">
    <label for="direccion_director" class="control-label">{{ 'Dirección del Director' }}</label>
    <input class="form-control" name="direccion_director" type="text" id="direccion_director"
           value="{{ isset($institucion)? $institucion->direccion_director:old('direccion_director')}}">
    {!! $errors->first('direccion_director', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group col-sm-12">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Actualizar' : 'Crear' }}">
    <a href="{{ url('admin/superadmin/instituciones') }}" title="Volver" class="btn btn-warning btn-sm float-right">
        <i class="fa fa-arrow-left" aria-hidden="true"></i> Volver
    </a>
</div>
