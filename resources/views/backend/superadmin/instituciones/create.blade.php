@extends ('backend.layouts.app')

@section ('title', app_name() . ' | Instituciones')

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        Crear Nuevo Institución
                     </h4>
                </div><!--col-->
                <div class="col-sm-7">
                    <a href="{{ url('admin/superadmin/instituciones') }}" title="Volver" class="btn btn-warning btn-sm float-right">
                        <i class="fa fa-arrow-left" aria-hidden="true"></i> Volver
                    </a>
                </div>
            </div><!--row-->

            <hr />
            @include('includes.partials.messages')
            <form method="POST" action="{{ url('admin/superadmin/instituciones') }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                {{ csrf_field() }}

                @include ('backend.superadmin.instituciones.form', ['formMode' => 'create'])
            </form>

        </div>
    </div>

@endsection
