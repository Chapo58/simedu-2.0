@extends ('backend.layouts.app')

@section ('title', app_name() . ' | Instituciones')

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        Institucione {{ $institucion }}
                    </h4>
                </div><!--col-->
                <div class="col-sm-7">
                    <a href="{{ url('admin/superadmin/instituciones') }}" title="Volver"
                       class="btn btn-warning btn-sm float-right">
                        <i class="fa fa-arrow-left" aria-hidden="true"></i> Volver
                    </a>
                    <a href="{{ url('admin/superadmin/instituciones/' . $institucion->id . '/edit') }}"
                       title="Editar Institucione"
                       class="btn btn-primary btn-sm float-right">
                        <i class="fa fa-edit" aria-hidden="true"></i> Editar
                    </a>
                    <form method="POST" action="{{ url('admin/superadmin/instituciones/' . $institucion->id) }}"
                          accept-charset="UTF-8" style="display:inline">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <button type="submit" class="btn btn-danger btn-sm float-right" title="Eliminar Institucione"
                                onclick="return confirm('¿Desea eliminar este registro?')">
                            <i class="fa fa-trash" aria-hidden="true"></i> Eliminar
                        </button>
                    </form>
                </div>
            </div><!--row-->
            <br/>

            <div class="table-responsive">
                <table class="table">
                    <tbody>
                    <tr>
                        <th> Numero</th>
                        <td> {{ $institucion->numero }} </td>
                    </tr>
                    <tr>
                        <th> Nombre</th>
                        <td> {{ $institucion->nombre }} </td>
                    </tr>
                    <tr>
                        <th> Dirección</th>
                        <td> {{ $institucion->direccion }} </td>
                    </tr>
                    <tr>
                        <th> Teléfono 1</th>
                        <td> {{ $institucion->telefono1 }} </td>
                    </tr>
                    <tr>
                        <th> Teléfono 2</th>
                        <td> {{ $institucion->telefono2 }} </td>
                    </tr>
                    <tr>
                        <th> E-mail</th>
                        <td> {{ $institucion->email}} </td>
                    </tr>
                    <tr>
                        <th> Logo</th>
                        @if (isset($institucion))
                            <td>{{ Form::image('uploads/instituciones/' . $institucion->logo ,'',['class' => 'thumb','style'=>'border: 2px solid lightsteelblue;', 'width' => 200, 'height' => 200])}}</td>
                        @else

                        @endif
                    </tr>
                    <tr>
                        <th> Estado</th>
                        <td> {{ $institucion->habilitado==1?'Habilitado':'Deshabilitado'}} </td>
                    </tr>
                    <tr>
                        <th> Nombre del Director</th>
                        <td> {{ $institucion->nombre_director}} </td>
                    </tr>
                    <tr>
                        <th> Teléfono del Director</th>
                        <td> {{ $institucion->telefono_director}} </td>
                    </tr>
                    <tr>
                        <th> E-mail del Director</th>
                        <td> {{ $institucion->email_director}} </td>
                    </tr>
                    <tr>
                        <th> Dirección del Director</th>
                        <td> {{ $institucion->direccion_director}} </td>
                    </tr>
                    </tbody>
                </table>
            </div>

        </div>
    </div>

@endsection
