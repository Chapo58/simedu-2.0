<div class="form-group {{ $errors->has('nombre') ? 'has-error' : ''}}">
    <label for="nombre" class="control-label">{{ 'Nombre' }}</label>
    <input class="form-control" name="nombre" type="text" id="nombre" value="{{isset($profesor)? $profesor->nombre :old('nombre') }}">
    {!! $errors->first('nombre', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('apellido') ? 'has-error' : ''}}">
    <label for="apellido" class="control-label">{{ 'Apellido' }}</label>
    <input class="form-control" name="apellido" type="text" id="apellido" value="{{isset($profesor)? $profesor->apellido :old('apellido') }}">
    {!! $errors->first('apellido', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
    <label for="email" class="control-label">{{ 'Email' }}</label>
    <input class="form-control" name="email" type="email" id="email" value="{{isset($profesor)? $profesor->email :old('email') }}">
    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('genero') ? 'has-error' : ''}}">
    <label for="genero" class="control-label">{{ 'Genero' }}</label>
    <div class="radio">
        <label><input name="genero" type="radio" value="1" {{ (isset($profesor) && 1 == $profesor->genero) ? 'checked' : '' }}> Masculino</label>
    </div>
    <div class="radio">
        <label><input name="genero" type="radio" value="0" @if (isset($profesor)) {{ (0 == $profesor->genero) ? 'checked' : '' }} @else {{ 'checked' }} @endif> Femenino</label>
    </div>
    {!! $errors->first('genero', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('fecha_nacimiento') ? 'has-error' : ''}}">
    <label for="fecha_nacimiento" class="control-label">{{ 'Fecha Nacimiento' }}</label>
    <input class="form-control" name="fecha_nacimiento" type="date" id="fecha_nacimiento" value="{{isset($profesor)? $profesor->fecha_nacimiento :old('fecha_nacimiento') }}">
    {!! $errors->first('fecha_nacimiento', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('telefono') ? 'has-error' : ''}}">
    <label for="telefono" class="control-label">{{ 'Telefono' }}</label>
    <input class="form-control" name="telefono" type="text" id="telefono" value="{{isset($profesor)? $profesor->telefono :old('telefono') }}">
    {!! $errors->first('telefono', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('direccion') ? 'has-error' : ''}}">
    <label for="direccion" class="control-label">{{ 'Direccion' }}</label>
    <input class="form-control" name="direccion" type="text" id="direccion" value="{{isset($profesor)? $profesor->direccion :old('direccion') }}">
    {!! $errors->first('direccion', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group col-sm-12">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Actualizar' : 'Crear' }}">
    <a href="{{ url('admin/%%routeGroup%%%%viewName%%') }}" title="Volver" class="btn btn-warning btn-sm float-right">
        <i class="fa fa-arrow-left" aria-hidden="true"></i> Volver
    </a>
</div>