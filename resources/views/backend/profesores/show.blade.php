@extends ('backend.layouts.app')

@section ('title', app_name() . ' | Profesores')

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        Profesores {{ $profesor }}
                    </h4>
                </div><!--col-->
                <div class="col-sm-7">
                    <a href="{{ url('admin/profesores') }}" title="Volver" class="btn btn-warning btn-sm float-right">
                        <i class="fa fa-arrow-left" aria-hidden="true"></i> Volver
                    </a>
                    <a href="{{ url('admin/profesores/' . $profesor->id . '/edit') }}" title="Editar Profesore"
                       class="btn btn-primary btn-sm float-right">
                        <i class="fa fa-edit" aria-hidden="true"></i> Editar
                    </a>
                    <form method="POST" action="{{ url('admin/profesores' . '/' . $profesor->id) }}"
                          accept-charset="UTF-8" style="display:inline">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <button type="submit" class="btn btn-danger btn-sm float-right" title="Eliminar Profesore"
                                onclick="return confirm('¿Desea eliminar este registro?')">
                            <i class="fa fa-trash" aria-hidden="true"></i> Eliminar
                        </button>
                    </form>
                </div>
            </div><!--row-->
            <br/>

            <div class="table-responsive">
                <table class="table">
                    <tbody>
                    <tr>
                        <th> Nombre</th>
                        <td> {{ $profesor->nombre }} </td>
                    </tr>
                    <tr>
                        <th> Apellido</th>
                        <td> {{ $profesor->apellido }} </td>
                    </tr>
                    <tr>
                        <th> Email</th>
                        <td> {{ $profesor->email }} </td>
                    </tr>
                    <tr>
                        <th> Género</th>
                        <td> {{ $profesor->genero==0?"Masculino":"Femenino"}} </td>
                    </tr>
                    <tr>
                        <th> Fecha de Nacimiento</th>
                        <td> {{ $profesor->fecha_nacimiento}} </td>
                    </tr>
                    <tr>
                        <th> Teléfono</th>
                        <td> {{ $profesor->telefono}} </td>
                    </tr>
                    <tr>
                        <th> Dirección</th>
                        <td> {{ $profesor->direccion}} </td>
                    </tr>
                    </tbody>
                </table>
            </div>

        </div>
    </div>

@endsection
