@extends ('backend.layouts.app')

@section ('title', app_name() . ' | Preguntas')

@section('content')
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-5">
                        <h4 class="card-title mb-0">
                        Pregunta {{ $pregunta }}
                        </h4>
                    </div><!--col-->
                    <div class="col-sm-7">
                        <a href="{{ url('admin/preguntas') }}" title="Volver" class="btn btn-warning btn-sm float-right">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Volver
                        </a>
                        <a href="{{ url('admin/preguntas/' . $pregunta->id . '/edit') }}" title="Editar Pregunta" class="btn btn-primary btn-sm float-right">
                            <i class="fa fa-edit" aria-hidden="true"></i> Editar
                        </a>
                        <form method="POST" action="{{ url('admin/preguntas' . '/' . $pregunta->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm float-right" title="Eliminar Pregunta" onclick="return confirm('¿Desea eliminar este registro?')">
                                <i class="fa fa-trash" aria-hidden="true"></i> Eliminar
                            </button>
                        </form>
                    </div>
                </div><!--row-->
                <br/>

                <div class="table-responsive">
                    <table class="table">
                        <tbody>
                            <tr><th> Pregunta </th><td> {{ $pregunta->pregunta }} </td></tr><tr><th> Archivo </th><td> {{ $pregunta->archivo }} </td></tr><tr><th> Linkexterno </th><td> {{ $pregunta->linkexterno }} </td></tr>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>

@endsection
