<!DOCTYPE html>
@langrtl
    <html lang="{{ app()->getLocale() }}" dir="rtl">
@else
    <html lang="{{ app()->getLocale() }}">
@endlangrtl

@include('backend.includes.head')

<body class="{{ config('backend.body_classes') }}">
    @include('backend.includes.header')

    <div class="app-body">
        @include('backend.includes.sidebar')

        <main class="main">
            @include('includes.partials.logged-in-as')
            <br>

            <div class="container-fluid">
                <div class="animated fadeIn">
                    <div class="content-header">
                        @yield('page-header')
                    </div><!--content-header-->

                    @yield('content')
                </div><!--animated-->
            </div><!--container-fluid-->
        </main><!--main-->

        @include('backend.includes.aside')
    </div><!--app-body-->

    @include('backend.includes.footer')

    <!-- Scripts -->
    @yield('before-scripts')
    <script src="{{asset('backend/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('backend/dual-listbox/js/jquery.transfer.js')}}"></script>
    <script type="text/javascript" src="{{asset('backend/dual-listbox/js/jquery.transfer2.js')}}"></script>

    <script src="{{asset('js/custom.js')}}" type="text/javascript"></script>
    {!! script(mix('js/backend.js')) !!}
    <script type="text/javascript">
        var baseurl = '{{URL::to('/')}}';
    </script>
    @yield('after-scripts')
</body>
</html>
