<div class="form-group {{ $errors->has('nombre') ? 'has-error' : ''}}">
    <label for="nombre" class="control-label">{{ 'Nombre' }}</label>
    <input class="form-control" name="nombre" type="text" id="nombre" value="{{isset($planteo)? $planteo->nombre :old('nombre') }}">
    {!! $errors->first('nombre', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('profesor_id') ? 'has-error' : ''}}">
    <label for="profesor_id" class="control-label">{{ 'Profesor: ' }}</label>
    <span>{{$planteo->profesor->nombre . ' ' . $planteo->profesor->apellido}}</span>
</div>
<div class="form-group {{ $errors->has('habilitado') ? 'has-error' : ''}}">
    <label for="habilitado" class="control-label">{{ 'Habilitado' }}</label>
    <div class="radio">
    <label><input name="habilitado" type="radio" value="1" {{ (isset($planteo) && 1 == $planteo->habilitado) ? 'checked' : '' }}> Yes</label>
</div>
<div class="radio">
    <label><input name="habilitado" type="radio" value="0" @if (isset($planteo)) {{ (0 == $planteo->habilitado) ? 'checked' : '' }} @else {{ 'checked' }} @endif> No</label>
</div>
    {!! $errors->first('habilitado', '<p class="help-block">:message</p>') !!}
</div>

<div class="table-responsive">
    <table class="table">
        <thead>
        <tr style="background-color: #e4e5e6">
            <th>Pregunta</th>
            <th>Acciones
                <a data-toggle="modal" style="float: right"
                   data-target="#modal-dialog-agregarPregunta"
                   href="#" class="btn btn-primary btn-sm">
                    <i class="fa fa-plus" aria-hidden="true"></i> Agregar Pregunta
                </a>
            </th>
        </tr>
        </thead>
        <tbody id="contenidoTablaPreguntas">
        @foreach($preguntas as $pregunta)
            <tr>
                <td>{{ $pregunta->pregunta }}</td>
                <td>
                    <a data-toggle="modal"
                       data-target="#modal-dialog-edit-{{$pregunta->id}}"
                       href="#" class="btn btn-primary btn-sm">
                        <i class="fa fa-edit" aria-hidden="true"></i> Editar
                    </a>

                    <form method="POST" action="{{ url('admin/preguntas/' . $pregunta->id) }}"
                          accept-charset="UTF-8" style="display:inline">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <button type="submit" class="btn btn-danger btn-sm" title="Eliminar Pregunta"
                                onclick="return confirm('¿Desea eliminar este registro?')">
                            <i class="fa fa-trash" aria-hidden="true"></i> Eliminar
                        </button>
                    </form>
                </td>

                {{--Modal para crear Preguntas--}}
                <div class="modal fade inverse" tabindex="-1" role="dialog" id="modal-dialog-edit-{{$pregunta->id}}">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Editar Pregunta</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form method="POST" action="{{ url('admin/preguntas/'.$pregunta->id) }}" accept-charset="UTF-8" class="form-horizontal"
                                  enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="modal-body">
                                    {{method_field('PUT')}}
                                    <input type="hidden" name="planteo" value="{{$planteo->id}}">
                                    @include('backend.preguntas.form')

                                </div>
                                <div class="modal-footer">
                                    <input class="btn btn-primary" type="submit" value="Guardar">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </tr>
        @endforeach
        </tbody>
    </table>

</div>

<div class="form-group col-sm-12">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Actualizar' : 'Crear' }}">
    <a href="{{ url('admin/%%routeGroup%%%%viewName%%') }}" title="Volver" class="btn btn-warning btn-sm float-right">
        <i class="fa fa-arrow-left" aria-hidden="true"></i> Volver
    </a>
</div>

@section('after-scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $("#boton-dado-create").click(function () {
                $("#opciones-multiples-create").toggle(1000);
            });

            $(".boton-dado").click(function () {
                $(".funkyradio").toggle(1000);
            });
        });
    </script>
@endsection