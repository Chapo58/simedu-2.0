{{--Modal para crear Preguntas--}}
<div class="modal fade inverse" tabindex="-1" role="dialog" id="modal-dialog-agregarPregunta">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Agregar Pregunta</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="create-pregunta-form" method="POST" action="{{ url('admin/preguntas') }}" accept-charset="UTF-8" class="form-horizontal"
                  enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="modal-body">
                    <input type="hidden" name="planteo" value="{{$planteo->id}}">

                    <div class="form-group {{ $errors->has('pregunta') ? 'has-error' : ''}}">
                        <label for="pregunta" class="control-label">{{ 'Pregunta' }}</label>
                        <textarea class="form-control" rows="5" name="pregunta" type="textarea"
                                  id="pregunta">{{  old('pregunta')}}</textarea>
                        {!! $errors->first('pregunta', '<p class="help-block">:message</p>') !!}
                    </div>

                    <div class="form-group {{ $errors->has('archivo') ? 'has-error' : ''}}">
                        <label for="logo" class="control-label">{{ 'Archivo' }}</label>
                        <input type="file" name="archivo" id="archivo" accept=".jpeg,.jpg,.png,.gif,.pdf,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,.doc,.docx,.xml,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document">
                        {!! $errors->first('archivo', '<p class="help-block">:message</p>') !!}
                    </div>

                    <div class="form-group {{ $errors->has('linkexterno') ? 'has-error' : ''}}">
                        <div class="input-group">
                            <div class="input-group-addon"><span class="fa fa-link"></span>&nbsp;<strong>Link</strong>
                            </div>
                            <input type="text" class="form-control" name="linkexterno" id="linkexterno" placeholder="ENLACE EXTERNO"
                                   value="{{  old('linkexterno')}}">
                        </div>
                        {!! $errors->first('linkexterno', '<p class="help-block">:message</p>') !!}
                    </div>

                    <div class="row funkyradio" id="opciones-multiples-create" style="display: none;">
                        <div class="col-md-12 col-xs-12 funkyradio-success">
                            <input type="radio" value="1" name="opcionCorrectaCreate" id="radio1">
                            <label for="radio1">
                                <input type="text" class="form-control" style="text-align: center;" name="opcion1"
                                       id="opcion1" placeholder="OPCIÓN 1">
                            </label>
                        </div>
                        <div class="col-md-12 col-xs-12 funkyradio-success">
                            <input type="radio" value="2" name="opcionCorrectaCreate" id="radio2">
                            <label for="radio2">
                                <input type="text" class="form-control" style="text-align: center;" name="opcion2"
                                       id="opcion2" placeholder="OPCIÓN 2">
                            </label>
                        </div>
                        <div class="col-md-12 col-xs-12 funkyradio-success">
                            <input type="radio" value="3" name="opcionCorrectaCreate" id="radio3">
                            <label for="radio3">
                                <input type="text" class="form-control" style="text-align: center;" name="opcion3"
                                       id="opcion3" placeholder="OPCIÓN 3">
                            </label>
                        </div>
                        <div class="col-md-12 col-xs-12 funkyradio-success">
                            <input type="radio" value="4" name="opcionCorrectaCreate" id="radio4">
                            <label for="radio4">
                                <input type="text" class="form-control" style="text-align: center;" name="opcion4"
                                       id="opcion4" placeholder="OPCIÓN 4">
                            </label>
                        </div>
                        <div class="col-md-12 col-xs-12 funkyradio-success">
                            <input type="radio" value="5" name="opcionCorrectaCreate" id="radio5">
                            <label for="radio5">
                                <input type="text" class="form-control" style="text-align: center;" name="opcion5"
                                       id="opcion5" placeholder="OPCIÓN 5">
                            </label>
                        </div>
                        <div class="col-md-12 col-xs-12 funkyradio-success">
                            <input type="radio" value="6" name="opcionCorrectaCreate" id="radio6">
                            <label for="radio6">
                                <input type="text" class="form-control" style="text-align: center;" name="opcion6"
                                       id="opcion6" placeholder="OPCIÓN 6">
                            </label>
                        </div>
                    </div>

                    <button id="boton-dado-create" type="button" class="btn btn-warning">Dado</button>

                </div>
                <div class="modal-footer">
                    <input class="btn btn-primary" type="submit" value="Guardar">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </form>
        </div>
    </div>
</div>

