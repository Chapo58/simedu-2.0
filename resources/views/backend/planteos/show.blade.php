@extends ('backend.layouts.app')

@section ('title', app_name() . ' | Planteos')

@section('content')
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-5">
                        <h4 class="card-title mb-0">
                        Planteo {{ $planteo }}
                        </h4>
                    </div><!--col-->
                    <div class="col-sm-7">
                        <a href="{{ url('admin/planteos') }}" title="Volver" class="btn btn-warning btn-sm float-right">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Volver
                        </a>
                        <a href="{{ url('admin/planteos/' . $planteo->id . '/edit') }}" title="Editar Planteo" class="btn btn-primary btn-sm float-right">
                            <i class="fa fa-edit" aria-hidden="true"></i> Editar
                        </a>
                        <form method="POST" action="{{ url('admin/planteos' . '/' . $planteo->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm float-right" title="Eliminar Planteo" onclick="return confirm('¿Desea eliminar este registro?')">
                                <i class="fa fa-trash" aria-hidden="true"></i> Eliminar
                            </button>
                        </form>
                    </div>
                </div><!--row-->
                <br/>

                <div class="table-responsive">
                    <table class="table">
                        <tbody>
                            <tr><th> Nombre </th><td> {{ $planteo->nombre }} </td></tr><tr><th> Profesor Id </th><td> {{ $planteo->profesor_id }} </td></tr><tr><th> Habilitado </th><td> {{ $planteo->habilitado }} </td></tr>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>

@endsection
