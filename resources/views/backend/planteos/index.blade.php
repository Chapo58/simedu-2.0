@extends ('backend.layouts.app')

@section ('title', app_name() . ' | Planteos')

@section('content')
    @include('includes.partials.messages')
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        Planteos
                    </h4>
                </div><!--col-->
                <div class="col-sm-7">

                    <div class="btn-toolbar float-right" role="toolbar" aria-label="Toolbar with button groups">
                        <a data-toggle="modal"
                           data-target="#modal-dialog-create" title="Crear Nuevo"
                           href="#" class="btn btn-success btn-lg ml-1">
                            <i class="fas fa-plus-circle"></i>
                        </a>
                    </div>
                </div><!--col-->
            </div><!--row-->
            <div class="row mt-4">
                <div class="col">
                    <form method="GET" action="{{ url('admin/planteos') }}" accept-charset="UTF-8"
                          class="form-inline my-2 my-lg-0 float-right" role="search">
                        <div class="input-group">
                            <input type="text" class="form-control" name="search" placeholder="Buscar..."
                                   value="{{ request('search') }}">
                            <span class="input-group-append">
                                <button class="btn btn-secondary" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                    </form>
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Profesor Id</th>
                                <th>Habilitado</th>
                                <th>Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($planteos as $item)
                                <tr>
                                    <td>{{ $item->nombre }}</td>
                                    <td>{{ $item->profesor->nombre . ' ' . $item->profesor->apellido }}</td>
                                    <td>{{ $item->habilitado }}</td>
                                    <td>
                                        <a href="{{ url('admin/planteos/' . $item->id) }}" title="Ver Planteo"
                                           class="btn btn-info btn-sm">
                                            <i class="fa fa-eye" aria-hidden="true"></i> Ver
                                        </a>
                                        <a href="{{ url('admin/planteos/' . $item->id . '/edit') }}"
                                           title="Editar Planteo" class="btn btn-primary btn-sm">
                                            <i class="fa fa-edit" aria-hidden="true"></i> Editar
                                        </a>
                                        <form method="POST" action="{{ url('admin/planteos' . '/' . $item->id) }}"
                                              accept-charset="UTF-8" style="display:inline">
                                            {{ method_field('DELETE') }}
                                            {{ csrf_field() }}
                                            <button type="submit" class="btn btn-danger btn-sm" title="Eliminar Planteo"
                                                    onclick="return confirm('¿Desea eliminar este registro?')">
                                                <i class="fa fa-trash" aria-hidden="true"></i> Eliminar
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="pagination-wrapper">
                            {!! $planteos->appends(['search' => Request::get('search')])->render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

{{--Modal para crear Planteos--}}
<div class="modal fade inverse" tabindex="-1" role="dialog" id="modal-dialog-create">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Agregar Planteo</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="POST" action="{{ url('admin/planteos') }}" accept-charset="UTF-8" class="form-horizontal"
                  enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="modal-body">
                    <div class="form-group {{ $errors->has('nombre') ? 'has-error' : ''}}">
                        <label for="nombre" class="control-label">{{ 'Nombre' }}</label>
                        <input class="form-control" name="nombre" type="text" id="nombre"
                               value="{{old('nombre') }}">
                        {!! $errors->first('nombre', '<p class="help-block">:message</p>') !!}
                    </div>
                    <div class="form-group {{ $errors->has('habilitado') ? 'has-error' : ''}}">
                        <label for="habilitado" class="control-label">{{ 'Habilitado' }}</label>
                        <div class="radio">
                            <label><input name="habilitado" type="radio"
                                          value="1" {{ (isset($planteo) && 1 == $planteo->habilitado) ? 'checked' : '' }}>
                                Si</label>
                        </div>
                        <div class="radio">
                            <label><input name="habilitado" type="radio"
                                          value="0" @if (isset($planteo)) {{ (0 == $planteo->habilitado) ? 'checked' : '' }} @else {{ 'checked' }} @endif>
                                No</label>
                        </div>
                        {!! $errors->first('habilitado', '<p class="help-block">:message</p>') !!}
                    </div>

                </div>
                <div class="modal-footer">
                    <input class="btn btn-primary" type="submit" value="Guardar">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </form>
        </div>
    </div>
</div>