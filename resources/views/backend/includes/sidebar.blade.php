<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav">
            <li class="nav-title">
                {{ __('menus.backend.sidebar.general') }}
            </li>

            <li class="nav-item">
                <a class="nav-link {{ active_class(Active::checkUriPattern('admin/dashboard')) }}"
                   href="{{ route('admin.dashboard') }}"><i
                            class="icon-speedometer"></i> {{ __('menus.backend.sidebar.dashboard') }}</a>
            </li>

            <li class="nav-title">
                {{ __('menus.backend.sidebar.system') }}
            </li>

            @if ($logged_in_user->isAdmin())
                <li class="nav-item nav-dropdown {{ active_class(Active::checkUriPattern('admin/auth*'), 'open') }}">
                    <a class="nav-link nav-dropdown-toggle {{ active_class(Active::checkUriPattern('admin/auth*')) }}"
                       href="#">
                        <i class="icon-user"></i> {{ __('menus.backend.access.title') }}

                        @if ($pending_approval > 0)
                            <span class="badge badge-danger">{{ $pending_approval }}</span>
                        @endif
                    </a>

                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a class="nav-link {{ active_class(Active::checkUriPattern('admin/auth/user*')) }}"
                               href="{{ route('admin.auth.user.index') }}">
                                {{ __('labels.backend.access.users.management') }}

                                @if ($pending_approval > 0)
                                    <span class="badge badge-danger">{{ $pending_approval }}</span>
                                @endif
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ active_class(Active::checkUriPattern('admin/auth/role*')) }}"
                               href="{{ route('admin.auth.role.index') }}">
                                {{ __('labels.backend.access.roles.management') }}
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item nav-dropdown {{ active_class(Active::checkUriPattern('admin/log-viewer*'), 'open') }}">
                    <a class="nav-link nav-dropdown-toggle {{ active_class(Active::checkUriPattern('admin/log-viewer*')) }}"
                       href="#">
                        <i class="icon-list"></i> {{ __('menus.backend.log-viewer.main') }}
                    </a>

                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a class="nav-link {{ active_class(Active::checkUriPattern('admin/log-viewer')) }}"
                               href="{{ route('log-viewer::dashboard') }}">
                                {{ __('menus.backend.log-viewer.dashboard') }}
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ active_class(Active::checkUriPattern('admin/log-viewer/logs*')) }}"
                               href="{{ route('log-viewer::logs.list') }}">
                                {{ __('menus.backend.log-viewer.logs') }}
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item nav-dropdown {{ active_class(Active::checkUriPattern('admin/superadmin/paises*'), 'open') }}">
                    <a class="nav-link nav-dropdown-toggle {{ active_class(Active::checkUriPattern('admin/superadmin/paises*')) }}"
                       href="#">
                        <i class="icon-list"></i> Ubicaciones
                    </a>

                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a class="nav-link {{ active_class(Active::checkUriPattern('admin/paises')) }}"
                               href="{{ url('admin/superadmin/paises') }}">
                                Paises
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ active_class(Active::checkUriPattern('admin/provincias')) }}"
                               href="{{ url('admin/superadmin/provincias') }}">
                                Provincias
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ active_class(Active::checkUriPattern('admin/localidades')) }}"
                               href="{{ url('admin/superadmin/localidades') }}">
                                Localidades
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ active_class(Active::checkUriPattern('admin/instituciones')) }}"
                       href="{{ url('admin/superadmin/instituciones') }}">
                        <i class="icon-user"></i> Instituciones
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ active_class(Active::checkUriPattern('admin/obra-social')) }}"
                       href="{{ url('admin/superadmin/obra-social') }}">
                        <i class="icon-user"></i> Obras Sociales
                    </a>
                </li>
            @endif
            @if ($logged_in_user->isInstitucion())
                <li class="nav-item">
                    <a class="nav-link {{ active_class(Active::checkUriPattern('admin/profesores')) }}"
                       href="{{ url('admin/profesores') }}">
                        <i class="icon-user"></i> Profesores
                    </a>
                </li>
            @endif
            @if ($logged_in_user->isInstitucion() || $logged_in_user->isProfesor())
                <li class="nav-item">
                    <a class="nav-link {{ active_class(Active::checkUriPattern('admin/alumnos')) }}"
                       href="{{ url('admin/alumnos') }}">
                        <i class="icon-user"></i> Alumnos
                    </a>
                </li>
            @endif
            @if ($logged_in_user->isProfesor())
                <li class="nav-item">
                    <a class="nav-link {{ active_class(Active::checkUriPattern('admin/planteos')) }}"
                       href="{{ url('admin/planteos') }}">
                        <i class="icon-user"></i> Planteos
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link {{ active_class(Active::checkUriPattern('admin/preguntas')) }}"
                       href="{{ url('admin/preguntas') }}">
                        <i class="icon-user"></i> Preguntas
                    </a>
                </li>
            @endif
            @if ($logged_in_user->isInstitucion())
                <li class="nav-item">
                    <a class="nav-link {{ active_class(Active::checkUriPattern('admin/cursos')) }}"
                       href="{{ url('admin/cursos') }}">
                        <i class="icon-user"></i> Cursos
                    </a>
                </li>
            @endif
        </ul>
    </nav>
</div><!--sidebar-->