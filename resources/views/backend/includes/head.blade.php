<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title', app_name())</title>
    <meta name="description" content="@yield('meta_description', 'Laravel 5 Boilerplate')">
    <meta name="author" content="@yield('meta_author', 'Anthony Rappa')">
    @yield('meta')

    <link href="{{asset('backend/dual-listbox/icon_font/css/icon_font.css')}}" rel="stylesheet" type="text/css"/>
    {{--<link href="{{asset('backend/dual-listbox/icon_font/css/icon_font2.css')}}" rel="stylesheet" type="text/css"/>--}}
    <link href="{{asset('backend/dual-listbox/css/jquery.transfer.css')}}" rel="stylesheet" type="text/css"/>
    {{--<link href="{{asset('backend/dual-listbox/css/jquery.transfer2.css')}}" rel="stylesheet" type="text/css"/>--}}

    {{-- See https://laravel.com/docs/5.5/blade#stacks for usage --}}
@yield('before-styles')

<!-- Check if the language is set to RTL, so apply the RTL layouts -->
    <!-- Otherwise apply the normal LTR layouts -->
    {{ style(mix('css/backend.css')) }}

    @yield('after-styles')
</head>