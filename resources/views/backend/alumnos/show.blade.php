@extends ('backend.layouts.app')

@section ('title', app_name() . ' | Alumnos')

@section('content')
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-5">
                        <h4 class="card-title mb-0">
                        Alumno {{ $alumno }}
                        </h4>
                    </div><!--col-->
                    <div class="col-sm-7">
                        <a href="{{ url('admin/alumnos') }}" title="Volver" class="btn btn-warning btn-sm float-right">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Volver
                        </a>
                        <a href="{{ url('admin/alumnos/' . $alumno->id . '/edit') }}" title="Editar Alumno" class="btn btn-primary btn-sm float-right">
                            <i class="fa fa-edit" aria-hidden="true"></i> Editar
                        </a>
                        <form method="POST" action="{{ url('admin/alumnos' . '/' . $alumno->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm float-right" title="Eliminar Alumno" onclick="return confirm('¿Desea eliminar este registro?')">
                                <i class="fa fa-trash" aria-hidden="true"></i> Eliminar
                            </button>
                        </form>
                    </div>
                </div><!--row-->
                <br/>

                <div class="table-responsive">
                    <table class="table">
                        <tbody>
                        <tr>
                            <th> Nombre</th>
                            <td> {{ $alumno->nombre }} </td>
                        </tr>
                        <tr>
                            <th> Apellido</th>
                            <td> {{ $alumno->apellido }} </td>
                        </tr>
                        <tr>
                            <th> Email</th>
                            <td> {{ $alumno->email }} </td>
                        </tr>
                        <tr>
                            <th> Género</th>
                            <td> {{ $alumno->genero==0?"Masculino":"Femenino"}} </td>
                        </tr>
                        <tr>
                            <th> Fecha de Nacimiento</th>
                            <td> {{ $alumno->fecha_nacimiento}} </td>
                        </tr>
                        <tr>
                            <th> Teléfono</th>
                            <td> {{ $alumno->telefono}} </td>
                        </tr>
                        <tr>
                            <th> Dirección</th>
                            <td> {{ $alumno->direccion}} </td>
                        </tr>
                        <tr>
                            <th> Facebook</th>
                            <td> {{ $alumno->facebook}} </td>
                        </tr>
                        <tr>
                            <th> Twiter</th>
                            <td> {{ $alumno->twitter}} </td>
                        </tr>
                        <tr>
                            <th> Instagram</th>
                            <td> {{ $alumno->instagram}} </td>
                        </tr>
                        <tr>
                            <th> Google +</th>
                            <td> {{ $alumno->googlemas}} </td>
                        </tr>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>

@endsection
