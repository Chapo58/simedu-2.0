<div class="form-group {{ $errors->has('nombre') ? 'has-error' : ''}}">
    <label for="nombre" class="control-label">{{ 'Nombre' }}</label>
    <input class="form-control" name="nombre" type="text" id="nombre" value="{{isset($alumno)? $alumno->nombre :old('nombre') }}">
    {!! $errors->first('nombre', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('apellido') ? 'has-error' : ''}}">
    <label for="apellido" class="control-label">{{ 'Apellido' }}</label>
    <input class="form-control" name="apellido" type="text" id="apellido" value="{{isset($alumno)? $alumno->apellido :old('apellido') }}">
    {!! $errors->first('apellido', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
    <label for="email" class="control-label">{{ 'Email' }}</label>
    <input class="form-control" name="email" type="email" id="email" value="{{isset($alumno)? $alumno->email :old('email') }}">
    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('genero') ? 'has-error' : ''}}">
    <label for="genero" class="control-label">{{ 'Genero' }}</label>
    <div class="radio">
    <label><input name="genero" type="radio" value="1" {{ (isset($alumno) && 1 == $alumno->genero) ? 'checked' : '' }}> Masculino</label>
</div>
<div class="radio">
    <label><input name="genero" type="radio" value="0" @if (isset($alumno)) {{ (0 == $alumno->genero) ? 'checked' : '' }} @else {{ 'checked' }} @endif> Femenino</label>
</div>
    {!! $errors->first('genero', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('fecha_nacimiento') ? 'has-error' : ''}}">
    <label for="fecha_nacimiento" class="control-label">{{ 'Fecha Nacimiento' }}</label>
    <input class="form-control" name="fecha_nacimiento" type="date" id="fecha_nacimiento" value="{{isset($alumno)? $alumno->fecha_nacimiento :old('fecha_nacimiento') }}">
    {!! $errors->first('fecha_nacimiento', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('telefono') ? 'has-error' : ''}}">
    <label for="telefono" class="control-label">{{ 'Telefono' }}</label>
    <input class="form-control" name="telefono" type="text" id="telefono" value="{{isset($alumno)? $alumno->telefono :old('telefono') }}">
    {!! $errors->first('telefono', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('direccion') ? 'has-error' : ''}}">
    <label for="direccion" class="control-label">{{ 'Direccion' }}</label>
    <input class="form-control" name="direccion" type="text" id="direccion" value="{{isset($alumno)? $alumno->direccion :old('direccion') }}">
    {!! $errors->first('direccion', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('facebook') ? 'has-error' : ''}}">
    <label for="facebook" class="control-label">{{ 'Facebook' }}</label>
    <input class="form-control" name="facebook" type="text" id="facebook" value="{{isset($alumno)? $alumno->facebook :old('facebook') }}">
    {!! $errors->first('facebook', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('twitter') ? 'has-error' : ''}}">
    <label for="twitter" class="control-label">{{ 'Twitter' }}</label>
    <input class="form-control" name="twitter" type="text" id="twitter" value="{{isset($alumno)? $alumno->twitter :old('twitter') }}">
    {!! $errors->first('twitter', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('instagram') ? 'has-error' : ''}}">
    <label for="instagram" class="control-label">{{ 'Instagram' }}</label>
    <input class="form-control" name="instagram" type="text" id="instagram" value="{{isset($alumno)? $alumno->instagram :old('instagram') }}">
    {!! $errors->first('instagram', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('googlemas') ? 'has-error' : ''}}">
    <label for="googlemas" class="control-label">{{ 'Google +' }}</label>
    <input class="form-control" name="googlemas" type="text" id="googlemas" value="{{isset($alumno)? $alumno->googlemas :old('googlemas') }}">
    {!! $errors->first('googlemas', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group col-sm-12">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Actualizar' : 'Crear' }}">
    <a href="{{ url('admin/%%routeGroup%%%%viewName%%') }}" title="Volver" class="btn btn-warning btn-sm float-right">
        <i class="fa fa-arrow-left" aria-hidden="true"></i> Volver
    </a>
</div>