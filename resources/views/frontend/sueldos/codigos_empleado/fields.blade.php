<div class="col-sm-6">
    <div class="form-group m-form__group row">
        {!! Form::label('codigo_id', 'Codigo:') !!}
        {!! Form::select('codigo_id', $codigos, (isset($codigoEmpleado)) ? $codigoEmpleado->id : '', ['class' => 'form-control select2','required' => 'required']) !!}
    </div>
</div>

<!-- Cantidad Field -->
<div class="col-lg-6">
    <div class="form-group m-form__group row ">
        {!! Form::label('cantidad', 'Cantidad:') !!}
        <div class="m-input-icon m-input-icon--left">
            <span class="m-input-icon__icon m-input-icon__icon--left">
                <span>
                    <i class="fa fa-caret-right"></i>
        		</span>
        	</span>
        	{!! Form::number('cantidad', (isset($codigoEmpleado)) ? $codigoEmpleado->pivot->cantidad : '', ['class' => 'form-control m-input','help-block'=>'Cantidad', 'icon-class'=>'fa fa-caret-right', 'placeholder'=>'Ingrese cantidad']) !!}
        </div>
    </div>
</div>

<!-- Multiplica Field -->
<div class="col-lg-6">
    <div class="form-group m-form__group row ">
        {!! Form::label('multiplica', 'Multiplica:') !!}
        <div class="m-input-icon m-input-icon--left">
            <span class="m-input-icon__icon m-input-icon__icon--left">
                <span>
                    <i class="fa fa-caret-right"></i>
        		</span>
        	</span>
        	{!! Form::number('multiplica', (isset($codigoEmpleado)) ? $codigoEmpleado->pivot->multiplica : '', ['class' => 'form-control m-input','help-block'=>'Multiplica', 'icon-class'=>'fa fa-caret-right', 'placeholder'=>'Ingrese multiplica']) !!}
        </div>
    </div>
</div>

<!-- Divide Field -->
<div class="col-lg-6">
    <div class="form-group m-form__group row ">
        {!! Form::label('divide', 'Divide:') !!}
        <div class="m-input-icon m-input-icon--left">
            <span class="m-input-icon__icon m-input-icon__icon--left">
                <span>
                    <i class="fa fa-caret-right"></i>
        		</span>
        	</span>
        	{!! Form::number('divide', (isset($codigoEmpleado)) ? $codigoEmpleado->pivot->divide : '', ['class' => 'form-control m-input','help-block'=>'Divide', 'icon-class'=>'fa fa-caret-right', 'placeholder'=>'Ingrese divide']) !!}
        </div>
    </div>
</div>

{{ Form::hidden('empleado_id', $empleado->id) }}
@if(isset($codigo))
    {{ Form::hidden('codigo_actual_id', $codigo->id) }}
@endif



