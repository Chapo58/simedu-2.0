<table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
    <thead>
        <tr>
            <th>Codigo</th>
            <th>Cantidad</th>
            <th>Multiplica</th>
            <th>Divide</th>
            <th>Acciones</th>
        </tr>
    </thead>
    <tbody>
    @foreach($empleado->codigos as $codigo)
        <tr>
            <td>{!! $codigo !!}</td>
            <td>{!! $codigo->pivot->cantidad !!}</td>
            <td>{!! $codigo->pivot->multiplica !!}</td>
            <td>{!! $codigo->pivot->divide !!}</td>
            <td>
                {!! Form::open(['url' => 'sueldos/codigos_empleado/'.$empleado->id.'/eliminar/'.$codigo->id,'method' => 'GET']) !!}
                <div class='m-btn-group m-btn-group--pill btn-group'>
                    <a href="{{ url('sueldos/codigos_empleado/'.$empleado->id.'/editar/'.$codigo->id) }}" class='m-btn btn btn-info' data-container="body" data-toggle="m-tooltip" data-placement="top" data-skin="dark" title data-original-title="Editar"><i class="far fa-edit"></i></a>
                    {!! Form::button('<i class="fas fa-trash"></i>', ['type' => 'submit', 'class' => 'm-btn btn btn-danger','data-container' => 'body','data-toggle' => 'm-tooltip', 'data-placement' => 'top','data-skin' => 'dark','title data-original-title'=>'Eliminar','onclick' => "return confirm('Estás seguro?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>