<!-- Id Field -->
<div class="col-md-6">
    {{ Form::mdText('id',  $codigoEmpleado->id, ['disabled' => 'disabled', 'help-block' => 'Id', 'icon-class' => 'fa fa-caret-right']) }}
</div>

<!-- Cantidad Field -->
<div class="col-md-6">
    {{ Form::mdText('cantidad',  $codigoEmpleado->cantidad, ['disabled' => 'disabled', 'help-block' => 'Cantidad', 'icon-class' => 'fa fa-caret-right']) }}
</div>

<!-- Multiplica Field -->
<div class="col-md-6">
    {{ Form::mdText('multiplica',  $codigoEmpleado->multiplica, ['disabled' => 'disabled', 'help-block' => 'Multiplica', 'icon-class' => 'fa fa-caret-right']) }}
</div>

<!-- Divide Field -->
<div class="col-md-6">
    {{ Form::mdText('divide',  $codigoEmpleado->divide, ['disabled' => 'disabled', 'help-block' => 'Divide', 'icon-class' => 'fa fa-caret-right']) }}
</div>

<!-- Created At Field -->
<div class="col-md-6">
    {{ Form::mdText('created_at',  $codigoEmpleado->created_at, ['disabled' => 'disabled', 'help-block' => 'Created At', 'icon-class' => 'fa fa-caret-right']) }}
</div>

<!-- Updated At Field -->
<div class="col-md-6">
    {{ Form::mdText('updated_at',  $codigoEmpleado->updated_at, ['disabled' => 'disabled', 'help-block' => 'Updated At', 'icon-class' => 'fa fa-caret-right']) }}
</div>

