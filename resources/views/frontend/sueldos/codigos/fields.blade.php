<!-- Numero Field -->
<div class="col-lg-6">
    <div class="form-group m-form__group row ">
        {!! Form::label('numero', 'Numero:') !!}
        <div class="m-input-icon m-input-icon--left">
            <span class="m-input-icon__icon m-input-icon__icon--left">
                <span>
                    <i class="fa fa-caret-right"></i>
        		</span>
        	</span>
        	{!! Form::number('numero', null, ['class' => 'form-control m-input','help-block'=>'Numero', 'icon-class'=>'fa fa-caret-right', 'placeholder'=>'Ingrese numero']) !!}
        </div>
    </div>
</div>

<!-- Descripcion Field -->
<div class="col-lg-6">
    {{ Form::mdText('descripcion', null, ['help-block' => 'Descripcion', 'icon-class' => 'fa fa-caret-right','placeholder'=>'Ingrese descripcion'],'Descripcion') }}
</div>

<!-- Es Sueldo Field -->
<div class="col-sm-6">
    <div class="form-group m-form__group">
        {!! Form::label('es_sueldo', 'Es Sueldo') !!}
        <div class="m-radio-list">
            <label class="m-radio m-radio--state-success">
                {!! Form::radio('es_sueldo', "1", 1) !!} Si
                <span></span>
            </label>
            <label class="m-radio m-radio--state-success">
                {!! Form::radio('es_sueldo', "0", null) !!} No
                <span></span>
            </label>
        </div>
    </div>
</div>

<!-- Sujeto A Aguinaldo Field -->
<div class="col-sm-6">
    <div class="form-group m-form__group">
        {!! Form::label('sujeto_a_aguinaldo', 'Sujeto a aguinaldo') !!}
        <div class="m-radio-list">
            <label class="m-radio m-radio--state-success">
                {!! Form::radio('sujeto_a_aguinaldo', "1", 1) !!} Si
                <span></span>
            </label>
            <label class="m-radio m-radio--state-success">
                {!! Form::radio('sujeto_a_aguinaldo', "0", null) !!} No
                <span></span>
            </label>
        </div>
    </div>
</div>

<!-- Tipo Haberes Field -->
<div class="col-sm-6">
    <div class="form-group m-form__group row">
    {!! Form::label('tipo_haberes', 'Tipo Haberes:') !!}
    {!! Form::select('tipo_haberes', ['0' => 'Haberes', '1' => 'Haberes sin Retención', '2' => 'Retenciones'], null, ['class' => 'form-control select2']) !!}
    </div>
</div>

<!-- Obtencion Field -->
<div class="col-sm-6">
    <div class="form-group m-form__group row">
    {!! Form::label('obtencion', 'Obtencion:') !!}
    {!! Form::select('obtencion', ['0' => 'Haberes', '1' => 'Haberes sin Retención', '2' => 'Retenciones', '3' => 'Grupo', '4' => 'Ninguno'], null, ['class' => 'form-control select2']) !!}
    </div>
</div>

<!-- Cantidad Field -->
<div class="col-lg-6">
    <div class="form-group m-form__group row ">
        {!! Form::label('cantidad', 'Cantidad:') !!}
        <div class="m-input-icon m-input-icon--left">
            <span class="m-input-icon__icon m-input-icon__icon--left">
                <span>
                    <i class="fa fa-caret-right"></i>
        		</span>
        	</span>
        	{!! Form::number('cantidad', null, ['class' => 'form-control m-input','help-block'=>'Cantidad', 'icon-class'=>'fa fa-caret-right', 'placeholder'=>'Ingrese cantidad']) !!}
        </div>
    </div>
</div>

<!-- Multiplica Field -->
<div class="col-lg-6">
    <div class="form-group m-form__group row ">
        {!! Form::label('multiplica', 'Multiplica:') !!}
        <div class="m-input-icon m-input-icon--left">
            <span class="m-input-icon__icon m-input-icon__icon--left">
                <span>
                    <i class="fa fa-caret-right"></i>
        		</span>
        	</span>
        	{!! Form::number('multiplica', null, ['class' => 'form-control m-input','help-block'=>'Multiplica', 'icon-class'=>'fa fa-caret-right', 'placeholder'=>'Ingrese multiplica']) !!}
        </div>
    </div>
</div>

<!-- Divide Field -->
<div class="col-lg-6">
    <div class="form-group m-form__group row ">
        {!! Form::label('divide', 'Divide:') !!}
        <div class="m-input-icon m-input-icon--left">
            <span class="m-input-icon__icon m-input-icon__icon--left">
                <span>
                    <i class="fa fa-caret-right"></i>
        		</span>
        	</span>
        	{!! Form::number('divide', null, ['class' => 'form-control m-input','help-block'=>'Divide', 'icon-class'=>'fa fa-caret-right', 'placeholder'=>'Ingrese divide']) !!}
        </div>
    </div>
</div>

