<!-- Numero Field -->
<div class="col-md-6">
    {{ Form::mdText('numero',  $codigo->numero, ['disabled' => 'disabled', 'help-block' => 'Numero', 'icon-class' => 'fa fa-caret-right']) }}
</div>

<!-- Descripcion Field -->
<div class="col-md-6">
    {{ Form::mdText('descripcion',  $codigo->descripcion, ['disabled' => 'disabled', 'help-block' => 'Descripcion', 'icon-class' => 'fa fa-caret-right']) }}
</div>

<!-- Es Sueldo Field -->
<div class="col-md-6">
    {{ Form::mdText('es_sueldo',  $codigo->es_sueldo, ['disabled' => 'disabled', 'help-block' => 'Es Sueldo', 'icon-class' => 'fa fa-caret-right']) }}
</div>

<!-- Tipo Haberes Field -->
<div class="col-md-6">
    {{ Form::mdText('tipo_haberes',  $codigo->tipo_haberes, ['disabled' => 'disabled', 'help-block' => 'Tipo Haberes', 'icon-class' => 'fa fa-caret-right']) }}
</div>

<!-- Obtencion Field -->
<div class="col-md-6">
    {{ Form::mdText('obtencion',  $codigo->obtencion, ['disabled' => 'disabled', 'help-block' => 'Obtencion', 'icon-class' => 'fa fa-caret-right']) }}
</div>

<!-- Cantidad Field -->
<div class="col-md-6">
    {{ Form::mdText('cantidad',  $codigo->cantidad, ['disabled' => 'disabled', 'help-block' => 'Cantidad', 'icon-class' => 'fa fa-caret-right']) }}
</div>

<!-- Multiplica Field -->
<div class="col-md-6">
    {{ Form::mdText('multiplica',  $codigo->multiplica, ['disabled' => 'disabled', 'help-block' => 'Multiplica', 'icon-class' => 'fa fa-caret-right']) }}
</div>

<!-- Divide Field -->
<div class="col-md-6">
    {{ Form::mdText('divide',  $codigo->divide, ['disabled' => 'disabled', 'help-block' => 'Divide', 'icon-class' => 'fa fa-caret-right']) }}
</div>

<!-- Sujeto A Aguinaldo Field -->
<div class="col-md-6">
    {{ Form::mdText('sujeto_a_aguinaldo',  $codigo->sujeto_a_aguinaldo, ['disabled' => 'disabled', 'help-block' => 'Sujeto A Aguinaldo', 'icon-class' => 'fa fa-caret-right']) }}
</div>

<!-- Created At Field -->
<div class="col-md-6">
    {{ Form::mdText('created_at',  $codigo->created_at, ['disabled' => 'disabled', 'help-block' => 'Created At', 'icon-class' => 'fa fa-caret-right']) }}
</div>

<!-- Updated At Field -->
<div class="col-md-6">
    {{ Form::mdText('updated_at',  $codigo->updated_at, ['disabled' => 'disabled', 'help-block' => 'Updated At', 'icon-class' => 'fa fa-caret-right']) }}
</div>

