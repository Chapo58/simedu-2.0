<table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
    <thead>
        <tr>
        <th>Numero</th>
        <th>Descripcion</th>
        <th>Cantidad</th>
        <th>Multiplica</th>
        <th>Divide</th>
        <th>Acciones</th>
        </tr>
    </thead>
    <tbody>
    @foreach($codigos as $codigo)
            <td>{!! $codigo->numero !!}</td>
            <td>{!! $codigo->descripcion !!}</td>
            <td>{!! $codigo->cantidad !!}</td>
            <td>{!! $codigo->multiplica !!}</td>
            <td>{!! $codigo->divide !!}</td>
            <td>
                {!! Form::open(['route' => ['frontend.sueldos.codigos.destroy', $codigo->id], 'method' => 'delete']) !!}
                <div class='m-btn-group m-btn-group--pill btn-group'>
                    <a href="{!! route('frontend.sueldos.codigos.show', [$codigo->id]) !!}" class='m-btn btn btn-success' data-container="body" data-toggle="m-tooltip" data-placement="top" data-skin="dark" title data-original-title="Consultar"><i class="fas fa-eye"></i></a>
                    <a href="{!! route('frontend.sueldos.codigos.edit', [$codigo->id]) !!}" class='m-btn btn btn-info' data-container="body" data-toggle="m-tooltip" data-placement="top" data-skin="dark" title data-original-title="Editar"><i class="far fa-edit"></i></a>
                    {!! Form::button('<i class="fas fa-trash"></i>', ['type' => 'submit', 'class' => 'm-btn btn btn-danger','data-container' => 'body','data-toggle' => 'm-tooltip', 'data-placement' => 'top','data-skin' => 'dark','title data-original-title'=>'Eliminar','onclick' => "return confirm('Estás seguro?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>