<!-- Periodo Field -->
<div class="form-group col-sm-12">
    <div class="form-group m-form__group row ">
        {!! Form::label('periodo', 'Periodo:') !!}
        {!! Form::date('periodo', null, ['class' => 'form-control', 'required' => 'required']) !!}
    </div>
</div>

{{ Form::hidden('empleado_id', $empleado->id) }}
