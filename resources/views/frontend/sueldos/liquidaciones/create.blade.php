@extends('frontend.layouts.app')

@section('content')
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="col-lg-12">
                @include('components.views.errors')
            </div>
            @if(Session::has('msg'))
                <div class="alert alert-{{Session::get('status')}} alert-dismissable">{{Session::get('msg')}}
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                </div>
            @endif
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">
                <!--begin::Portlet-->
                <div class="m-portlet m-portlet--brand m-portlet--head-solid-bg m-portlet--bordered">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
    						    <span class="m-portlet__head-icon">
    							    <i class="flaticon-placeholder-2"></i>
    							</span>
                                <h3 class="m-portlet__head-text">
                                    Realizar Liquidación
                                </h3>
                            </div>
                        </div>
                    </div>

                    {!! Form::open(['route' => 'frontend.sueldos.liquidaciones.store', 'class'=>'m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed']) !!}
                    <div class="m-portlet__body">
                        <div class="row">
                            @include('frontend.sueldos.liquidaciones.fields')
                        </div>
                    </div>
                    <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                        <div class="m-form__actions m-form__actions--solid">
                            <div class="row">
                                <div class="col-lg-12 m--align-right">
                                    {!! Form::submit('Guardar', ['class' => 'btn btn-primary btn-oneclick']) !!}
                                    <a href="{{ url('sueldos/liquidaciones/empleado/' . $empleado->id) }}" class="btn btn-secondary">Cancelar</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
                <!--end::Portlet-->
            </div>
        </div>
    </div>
@endsection
