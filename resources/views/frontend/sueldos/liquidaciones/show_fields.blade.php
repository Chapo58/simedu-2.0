<!-- Id Field -->
<div class="col-md-6">
    {{ Form::mdText('id',  $liquidacion->id, ['disabled' => 'disabled', 'help-block' => 'Id', 'icon-class' => 'fa fa-caret-right']) }}
</div>

<!-- Empleado Id Field -->
<div class="col-md-6">
    {{ Form::mdText('empleado_id',  $liquidacion->empleado_id, ['disabled' => 'disabled', 'help-block' => 'Empleado Id', 'icon-class' => 'fa fa-caret-right']) }}
</div>

<!-- Numero Field -->
<div class="col-md-6">
    {{ Form::mdText('numero',  $liquidacion->numero, ['disabled' => 'disabled', 'help-block' => 'Numero', 'icon-class' => 'fa fa-caret-right']) }}
</div>

<!-- Periodo Field -->
<div class="col-md-6">
    {{ Form::mdText('periodo',  $liquidacion->periodo, ['disabled' => 'disabled', 'help-block' => 'Periodo', 'icon-class' => 'fa fa-caret-right']) }}
</div>

<!-- Archivo Url Field -->
<div class="col-md-6">
    {{ Form::mdText('archivo_url',  $liquidacion->archivo_url, ['disabled' => 'disabled', 'help-block' => 'Archivo Url', 'icon-class' => 'fa fa-caret-right']) }}
</div>

<!-- Created At Field -->
<div class="col-md-6">
    {{ Form::mdText('created_at',  $liquidacion->created_at, ['disabled' => 'disabled', 'help-block' => 'Created At', 'icon-class' => 'fa fa-caret-right']) }}
</div>

<!-- Updated At Field -->
<div class="col-md-6">
    {{ Form::mdText('updated_at',  $liquidacion->updated_at, ['disabled' => 'disabled', 'help-block' => 'Updated At', 'icon-class' => 'fa fa-caret-right']) }}
</div>

