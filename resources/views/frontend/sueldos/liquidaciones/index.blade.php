@extends('frontend.layouts.app')

@section('content')
    <div class="m-content">
        @include('flash::message')
    </div>
    <div class="m-content">
            <div class="m-portlet m-portlet--mobile">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Liquidaciones {{$empleado}}
                            </h3>
                        </div>
                    </div>
                    <div class="m-portlet__head-tools">
                        <ul class="m-portlet__nav">
                            <li class="m-portlet__nav-item">
                                <a href="{{ url('sueldos/liquidaciones/empleado/'.$empleado->id.'/create') }}" class="btn btn-primary m-btn m-btn--pill m-btn--custom m-btn--icon m-btn--air">
    							    <span>
                                        <i class="la la-plus"></i>
                                        <span>Nueva Liquidación</span>
    								</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="m-portlet__body">
                    @include('frontend.sueldos.liquidaciones.table')
                </div>
            </div>

        </div>
@endsection




