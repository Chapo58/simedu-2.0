<table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
    <thead>
        <tr>
        <th>Numero</th>
        <th>Periodo</th>
             <th>Acciones</th>
        </tr>
    </thead>
    <tbody>
    @foreach($liquidacions as $liquidacion)
        <tr>
            <td>{!! $liquidacion->numero !!}</td>
            <td>{!! $liquidacion->periodo->format('m/Y') !!}</td>
            <td>
                {!! Form::open(['route' => ['frontend.sueldos.liquidaciones.destroy', $liquidacion->id], 'method' => 'delete']) !!}
                <div class='m-btn-group m-btn-group--pill btn-group'>
                    <a href="{{url($liquidacion->archivo_url)}}" target="_blank" class='m-btn btn btn-success' data-container="body" data-toggle="m-tooltip" data-placement="top" data-skin="dark" title data-original-title="Consultar"><i class="fas fa-eye"></i></a>
                    {!! Form::button('<i class="fas fa-trash"></i>', ['type' => 'submit', 'class' => 'm-btn btn btn-danger','data-container' => 'body','data-toggle' => 'm-tooltip', 'data-placement' => 'top','data-skin' => 'dark','title data-original-title'=>'Eliminar','onclick' => "return confirm('Estás seguro?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>