<!DOCTYPE html>
<html lang="es">
    <head>
        <title>Recibo</title>
        <link rel="shortcut icon" href="favicon.png">
        <link rel="stylesheet" href="css/bootstrap-pdf.min.css" />
    </head>
    <body class="no-skin">
        <div class="main-container ace-save-state" id="main-container">
            <div class="container">

                <table style="width:100%">
                    <tr>
                        <td><h1 style="color:#5692CB;">Recibo</h1></td>
                    </tr>
                </table>

                <hr>

                <table class="table table-condensed table-bordered" style="width:100%">
                    <tr>
                        <td rowspan="2">
                            {{$empleado->empresa}}
                        </td>
                        <td align="center">
                            Recibo N° {{$liquidacion->numero}}
                        </td>
                    </tr>
                    <tr align="center">
                        <td>
                            RECIBO DE HABERES LEY<br>
                            20.744
                        </td>
                    </tr>
                    <tr align="center">
                        <td>
                            Lugar y Fecha de Pago<br>
                            Lugar - {{date("d/m/Y H:i:s")}}
                        </td>
                        <td>
                            Periodo<br>
                            {{$liquidacion->periodo->format("m/Y")}}
                        </td>
                    </tr>
                </table>

                <table class="table table-condensed table-bordered" style="width:100%;margin-top:-20px;">
                    <tr>
                        <td colspan="2">
                            Apellido y Nombres<br>
                            {{$empleado}}
                        </td>
                        <td align="center">
                            Secc. &nbsp;&nbsp;&nbsp; Legajo<br>
                            2 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {{$empleado->legajo}}
                        </td>
                        <td align="center">
                            C.U.I.L.<br>
                            {{$empleado->cuit}}
                        </td>
                    </tr>
                    <tr align="center">
                        <td>
                            Básico<br>
                            $ 10.000
                        </td>
                        <td>
                            Ingreso<br>
                            {{($empleado->fecha_ingreso) ? $empleado->fecha_ingreso->format('d/m/Y') : ''}}
                        </td>
                        <td>
                            Cargo<br>
                            {{$empleado->cargo}}
                        </td>
                        <td>
                            Regimen<br>
                            S.I.P.A
                        </td>
                    </tr>
                </table>

                <table class="table table-condensed table-bordered" style="width:100%;margin-top:-20px;">
                    <tr>
                        <td>
                            {{$empleado->obra_social}}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            A tiempo completo indeterminado /Trabajo Permanente
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{$empleado->obra_social}}
                        </td>
                    </tr>
                </table>

                <table class="table table-condensed table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr class="info" align="center">
                            <th>Concepto</th>
                            <th>V. Unit.</th>
                            <th>Unid/Porc</th>
                            <th>Haberes</th>
                            <th>Descuentos</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($empleado->codigos as $codigo)
                        <tr>
                            <td>{{$codigo}}</td>
                            <td>{{$codigo->pivot->cantidad}}</td>
                            <td>{{$codigo->pivot->divide}}%</td>
                            <td>$ {{$codigo->pivot->multiplica}}</td>
                            <td style="color:green;"></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

            </div><!-- container -->
        </div>
    </body>
</html>
