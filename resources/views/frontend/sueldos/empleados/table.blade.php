<table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
    <thead>
        <tr>
        <th>Nombre</th>
        <th>Apellido</th>
        <th>Cuit</th>
        <th>Categoria</th>
         <th>Acciones</th>
        </tr>
    </thead>
    <tbody>
    @foreach($empleados as $empleados)
        <tr>
            <td>{!! $empleados->nombre !!}</td>
            <td>{!! $empleados->apellido !!}</td>
            <td>{!! $empleados->cuit !!}</td>
            <td>{!! $empleados->categoria !!}</td>
            <td>
                {!! Form::open(['route' => ['frontend.sueldos.empleados.destroy', $empleados->id], 'method' => 'delete']) !!}
                <div class='m-btn-group m-btn-group--pill btn-group'>
                    <a href="{!! route('frontend.sueldos.empleados.show', [$empleados->id]) !!}" class='m-btn btn btn-success' data-container="body" data-toggle="m-tooltip" data-placement="top" data-skin="dark" title data-original-title="Consultar"><i class="fas fa-eye"></i></a>
                    <a href="{!! route('frontend.sueldos.empleados.edit', [$empleados->id]) !!}" class='m-btn btn btn-info' data-container="body" data-toggle="m-tooltip" data-placement="top" data-skin="dark" title data-original-title="Editar"><i class="far fa-edit"></i></a>
                    <a href="{{ url('sueldos/codigos_empleado/' . $empleados->id) }}" class='m-btn btn btn-warning' data-container="body" data-toggle="m-tooltip" data-placement="top" data-skin="dark" title data-original-title="Codigos"><i class="fa fa-barcode"></i></a>
                    <a href="{{ url('sueldos/liquidaciones/empleado/' . $empleados->id) }}" target="_blank" class='m-btn btn btn-primary' data-container="body" data-toggle="m-tooltip" data-placement="top" data-skin="dark" title data-original-title="Imprimir"><i class="fa fa-print"></i></a>
                    {!! Form::button('<i class="fas fa-trash"></i>', ['type' => 'submit', 'class' => 'm-btn btn btn-danger','data-container' => 'body','data-toggle' => 'm-tooltip', 'data-placement' => 'top','data-skin' => 'dark','title data-original-title'=>'Eliminar','onclick' => "return confirm('Estás seguro?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>