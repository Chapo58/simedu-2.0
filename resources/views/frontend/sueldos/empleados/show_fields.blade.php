<!-- Nombre Field -->
<div class="col-md-6">
    {{ Form::mdText('nombre',  $empleado->nombre, ['disabled' => 'disabled', 'help-block' => 'Nombre', 'icon-class' => 'fa fa-caret-right']) }}
</div>

<!-- Apellido Field -->
<div class="col-md-6">
    {{ Form::mdText('apellido',  $empleado->apellido, ['disabled' => 'disabled', 'help-block' => 'Apellido', 'icon-class' => 'fa fa-caret-right']) }}
</div>

<!-- Sexo Field -->
<div class="col-md-6">
    {{ Form::mdText('sexo',  $empleado->sexo, ['disabled' => 'disabled', 'help-block' => 'Sexo', 'icon-class' => 'fa fa-caret-right']) }}
</div>

<!-- Fecha Ingreso Field -->
<div class="col-md-6">
    {{ Form::mdText('fecha_ingreso',  $empleado->fecha_ingreso, ['disabled' => 'disabled', 'help-block' => 'Fecha Ingreso', 'icon-class' => 'fa fa-caret-right']) }}
</div>

<!-- Estado Civil Field -->
<div class="col-md-6">
    {{ Form::mdText('estado_civil',  $empleado->estado_civil, ['disabled' => 'disabled', 'help-block' => 'Estado Civil', 'icon-class' => 'fa fa-caret-right']) }}
</div>

<!-- Cuit Field -->
<div class="col-md-6">
    {{ Form::mdText('cuit',  $empleado->cuit, ['disabled' => 'disabled', 'help-block' => 'Cuit', 'icon-class' => 'fa fa-caret-right']) }}
</div>

<!-- Fecha Nacimiento Field -->
<div class="col-md-6">
    {{ Form::mdText('fecha_nacimiento',  $empleado->fecha_nacimiento, ['disabled' => 'disabled', 'help-block' => 'Fecha Nacimiento', 'icon-class' => 'fa fa-caret-right']) }}
</div>

<!-- Direccion Id Field -->
<div class="col-md-6">
    {{ Form::mdText('direccion_id',  $empleado->direccion_id, ['disabled' => 'disabled', 'help-block' => 'Direccion Id', 'icon-class' => 'fa fa-caret-right']) }}
</div>

<!-- Categoria Id Field -->
<div class="col-md-6">
    {{ Form::mdText('categoria_id',  $empleado->categoria_id, ['disabled' => 'disabled', 'help-block' => 'Categoria Id', 'icon-class' => 'fa fa-caret-right']) }}
</div>

<!-- Obras Sociales Id Field -->
<div class="col-md-6">
    {{ Form::mdText('obras_sociales_id',  $empleado->obras_sociales_id, ['disabled' => 'disabled', 'help-block' => 'Obras Sociales Id', 'icon-class' => 'fa fa-caret-right']) }}
</div>

<!-- Telefono Field -->
<div class="col-md-6">
    {{ Form::mdText('telefono',  $empleado->telefono, ['disabled' => 'disabled', 'help-block' => 'Telefono', 'icon-class' => 'fa fa-caret-right']) }}
</div>

<!-- Email Field -->
<div class="col-md-6">
    {{ Form::mdText('email',  $empleado->email, ['disabled' => 'disabled', 'help-block' => 'Email', 'icon-class' => 'fa fa-caret-right']) }}
</div>

<!-- Telefono Field -->
<div class="col-md-6">
    {{ Form::mdText('telefono',  $empleado->telefono, ['disabled' => 'disabled', 'help-block' => 'Telefono', 'icon-class' => 'fa fa-caret-right']) }}
</div>

<!-- Observaciones Field -->
<div class="col-md-6">
    {{ Form::mdText('observaciones',  $empleado->observaciones, ['disabled' => 'disabled', 'help-block' => 'Observaciones', 'icon-class' => 'fa fa-caret-right']) }}
</div>

<!-- Created At Field -->
<div class="col-md-6">
    {{ Form::mdText('created_at',  $empleado->created_at, ['disabled' => 'disabled', 'help-block' => 'Created At', 'icon-class' => 'fa fa-caret-right']) }}
</div>

<!-- Updated At Field -->
<div class="col-md-6">
    {{ Form::mdText('updated_at',  $empleado->updated_at, ['disabled' => 'disabled', 'help-block' => 'Updated At', 'icon-class' => 'fa fa-caret-right']) }}
</div>

