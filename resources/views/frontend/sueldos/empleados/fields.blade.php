<!-- Nombre Field -->
<div class="col-lg-6">
    {{ Form::mdText('nombre', null, ['help-block' => 'Nombre', 'icon-class' => 'fa fa-caret-right','placeholder'=>'Ingrese Nombre'],'Nombre') }}
</div>

<!-- Apellido Field -->
<div class="col-lg-6">
    {{ Form::mdText('apellido', null, ['help-block' => 'Apellido', 'icon-class' => 'fa fa-caret-right','placeholder'=>'Ingrese Apellido'],'Apellido') }}
</div>

<!-- Sexo Field -->
<div class="col-sm-6">
    <div class="form-group m-form__group row ">
        {!! Form::label('sexo', 'Sexo:') !!}
        {!! Form::select('sexo', $sexos, isset($empleado->sexo) ? $empleado->sexo : '', ['class' => 'form-control select2']) !!}
    </div>
</div>

<!-- Fecha Ingreso Field -->
<div class="col-sm-6">
    <div class="form-group m-form__group row">
    {!! Form::label('fecha_ingreso', 'Fecha Ingreso:') !!}
    {!! Form::date('fecha_ingreso', null, ['class' => 'form-control']) !!}
    </div>
</div>

<!-- Estado Civil Field -->
<div class="col-sm-6">
    <div class="form-group m-form__group row">
    {!! Form::label('estado_civil', 'Estado Civil:') !!}
    {!! Form::select('estado_civil', $estados_civiles, isset($empleado->estado_civil) ? $empleado->estado_civil : '', ['class' => 'form-control select2']) !!}
    </div>
</div>

<!-- Cuit Field -->
<div class="col-lg-6">
    <div class="form-group m-form__group row">
        {!! Form::label('cuit', 'Cuit:') !!}
        <div class="m-input-icon m-input-icon--left">
            <span class="m-input-icon__icon m-input-icon__icon--left">
                <span>
                    <i class="fa fa-caret-right"></i>
        		</span>
        	</span>
        	{!! Form::number('cuit', null, ['class' => 'form-control m-input','help-block'=>'Cuit', 'icon-class'=>'fa fa-caret-right', 'placeholder'=>'Ingrese cuit']) !!}
        </div>
    </div>
</div>

<!-- Fecha Nacimiento Field -->
<div class="col-sm-6">
    <div class="form-group m-form__group row">
    {!! Form::label('fecha_nacimiento', 'Fecha Nacimiento:') !!}
    {!! Form::date('fecha_nacimiento', null, ['class' => 'form-control']) !!}
    </div>
</div>

<!-- Direccion Id Field -->
<div class="col-lg-3">
    {{ Form::mdText('direccion[direccion]', isset($empresa->direccion->direccion) ? $empresa->direccion->direccion : '', ['help-block' => 'Direccion', 'icon-class' => 'fa fa-caret-right','placeholder'=>'Ingrese una direccion'],'Direccion') }}
</div>

<div class="col-lg-3">
    <div class="form-group m-form__group row ">
        {!! Form::label('direccion[localidad_id]', 'Localidad') !!}
        {{ Form::select('direccion[localidad_id]', $localidades, isset($empresa->direccion->localidad) ? $empresa->direccion->localidad->id : '', array('class' => 'form-control select2')) }}
    </div>
</div>

<div class="col-lg-3">
    <div class="form-group m-form__group row ">
        {!! Form::label('direccion[provincia_id]', 'Provincia') !!}
        {{ Form::select('direccion[provincia_id]', $provincias, isset($empresa->direccion->provincia) ? $empresa->direccion->provincia->id : '', array('class' => 'form-control select2')) }}
    </div>
</div>

<div class="col-lg-3">
    <div class="form-group m-form__group row ">
        {!! Form::label('direccion[pais_id]', 'Pais') !!}
        {{ Form::select('direccion[pais_id]', $paises, isset($empresa->direccion->pais) ? $empresa->direccion->pais->id : '', array('class' => 'form-control select2')) }}
    </div>
</div>

<!-- Categoria Id Field -->
<div class="col-sm-6">
    <div class="form-group m-form__group row">
    {!! Form::label('categoria_id', 'Categoria:') !!}
    {!! Form::select('categoria_id', $categorias, isset($empleado->categoria) ? $empleado->categoria : '', ['class' => 'form-control select2']) !!}
    </div>
</div>

<!-- Obras Sociales Id Field -->
<div class="col-sm-6">
    <div class="form-group m-form__group row">
    {!! Form::label('obra_social_id', 'Obra Social:') !!}
    {!! Form::select('obra_social_id', $obras_sociales, isset($empleado->obra_social) ? $empleado->obra_social : '', ['class' => 'form-control select2']) !!}
    </div>
</div>

<!-- Telefono Field -->
<div class="col-lg-6">
    {{ Form::mdText('telefono', null, ['help-block' => 'Telefono', 'icon-class' => 'fa fa-caret-right','placeholder'=>'Ingrese telefono'],'Telefono') }}
</div>

<div class="col-lg-6">
    {{ Form::mdText('legajo', null, ['help-block' => 'Legajo', 'icon-class' => 'fa fa-caret-right','placeholder'=>'Ingrese Legajo'],'Legajo') }}
</div>

<div class="col-lg-6">
    {{ Form::mdText('cargo', null, ['help-block' => 'Cargo', 'icon-class' => 'fa fa-caret-right','placeholder'=>'Ingrese Cargo'],'Cargo') }}
</div>

<!-- Email Field -->
<div class="col-md-6">
    {{ Form::mdText('email', null, ['type' => 'email', 'help-block' => 'Email', 'icon-class' => 'fa fa-envelope'],'Email') }}
</div>

<!-- Observaciones Field -->
<div class="col-lg-6">
    {{ Form::mdTextArea('observaciones', null, ['help-block' => '', 'placeholder'=>'Ingrese observaciones']) }}
</div>

