<!-- Nombre Field -->
<div class="col-md-6">
    {{ Form::mdText('nombre',  $categoriasSueldos->nombre, ['disabled' => 'disabled', 'help-block' => 'Nombre', 'icon-class' => 'fa fa-caret-right']) }}
</div>

<!-- Descripcion Field -->
<div class="col-md-6">
    {{ Form::mdTextArea('descripcion',  $categoriasSueldos->descripcion, ['disabled' => 'disabled', 'help-block' => 'Descripcion', 'icon-class' => 'fa fa-caret-right']) }}
</div>

<!-- Created At Field -->
<div class="col-md-6">
    {{ Form::mdText('created_at',  $categoriasSueldos->created_at, ['disabled' => 'disabled', 'help-block' => 'Created At', 'icon-class' => 'fa fa-caret-right']) }}
</div>

<!-- Updated At Field -->
<div class="col-md-6">
    {{ Form::mdText('updated_at',  $categoriasSueldos->updated_at, ['disabled' => 'disabled', 'help-block' => 'Updated At', 'icon-class' => 'fa fa-caret-right']) }}
</div>

