
<!-- Nombre Field -->
<div class="col-lg-12">
    {{ Form::mdText('nombre', null, ['help-block' => 'Nombre', 'icon-class' => 'fa fa-caret-right','placeholder'=>'Ingrese nombre'],'Nombre') }}
</div>

<!-- Descripcion Field -->
<div class="col-lg-12">
    {{ Form::mdTextArea('descripcion', null, ['help-block' => 'Descripcion', 'icon-class' => 'fa fa-caret-right','placeholder'=>'Ingrese descripcion'],'Descripcion') }}
</div>

