<!--begin::Base Scripts -->
<script src="{{asset('frontend/assets/vendors/base/vendors.bundle.js')}}" type="text/javascript"></script>
<script src="{{asset('frontend/assets/demo/default/base/scripts.bundle.js')}}" type="text/javascript"></script>

<!--end::Base Scripts -->

<!--begin::Page Vendors Scripts -->
<script src="{{asset('frontend/assets/vendors/custom/fullcalendar/fullcalendar.bundle.js')}}" type="text/javascript"></script>

<!--end::Page Vendors Scripts -->
<script src="{{asset('frontend/assets/vendors/custom/datatables/datatables.bundle.js')}}" type="text/javascript"></script>
<script src="{{asset('frontend/assets/demo/default/custom/crud/datatables/advanced/column-rendering.js')}}" type="text/javascript"></script>

<!--begin::Page Snippets -->
<script src="{{asset('frontend/assets/app/js/dashboard.js')}}" type="text/javascript"></script>


<script src="{{asset('frontend/js/custom.js')}}" type="text/javascript"></script>
<!--end::Page Snippets -->
@yield('scripts')
