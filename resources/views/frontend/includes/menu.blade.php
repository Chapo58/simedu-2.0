<li class="m-menu__item  {!! Request::is('dashboard*') ? 'm-menu__item--active' : '' !!}" aria-haspopup="true">
    <a href="{!! url('/dashboard') !!}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-home"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
			    <span class="m-menu__link-text">Inicio</span>
			</span>
		</span>
    </a>
</li>
<li class="m-menu__item  {!! Request::is('empresas*') ? 'm-menu__item--active' : '' !!}" aria-haspopup="true">
    <a href="{!! route('frontend.empresas.index') !!}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-network"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
			    <span class="m-menu__link-text">Empresas</span>
			</span>
		</span>
    </a>
</li>

<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true">
    <a href="javascript:;" class="m-menu__link m-menu__toggle">
        <i class="m-menu__link-icon flaticon-graph"></i>
        <span class="m-menu__link-text">Contabilidad</span>
        <i class="m-menu__ver-arrow la la-angle-right"></i>
    </a>
    <div class="m-menu__submenu ">
        <span class="m-menu__arrow"></span>
        <ul class="m-menu__subnav">
            <li class="m-menu__item  {!! Request::is('plan_cuentas*') ? 'm-menu__item--active' : '' !!}" aria-haspopup="true">
                <a href="{!! route('frontend.contabilidad.plan_cuentas.index') !!}" class="m-menu__link ">
                    <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                        <span></span>
                    </i>
                    <span class="m-menu__link-text">Plan de Cuentas</span>
                </a>
            </li>
            <li class="m-menu__item  {!! Request::is('periodos*') ? 'm-menu__item--active' : '' !!}" aria-haspopup="true">
                <a href="{!! route('frontend.contabilidad.periodos.index') !!}" class="m-menu__link ">
                    <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                        <span></span>
                    </i>
                    <span class="m-menu__link-text">Periodos</span>
                </a>
            </li>
            <li class="m-menu__item  {!! Request::is('asientos*') ? 'm-menu__item--active' : '' !!}" aria-haspopup="true">
                <a href="{!! route('frontend.contabilidad.asientos.index') !!}" class="m-menu__link ">
                    <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                        <span></span>
                    </i>
                    <span class="m-menu__link-text">Asientos</span>
                </a>
            </li>
        </ul>
    </div>
</li>

<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true">
    <a href="javascript:;" class="m-menu__link m-menu__toggle">
        <i class="m-menu__link-icon flaticon-clipboard"></i>
        <span class="m-menu__link-text">Sueldos</span>
        <i class="m-menu__ver-arrow la la-angle-right"></i>
    </a>
    <div class="m-menu__submenu ">
        <span class="m-menu__arrow"></span>
        <ul class="m-menu__subnav">
            <li class="m-menu__item  {!! Request::is('categoriasSueldos*') ? 'm-menu__item--active' : '' !!}" aria-haspopup="true">
                <a href="{!! route('frontend.sueldos.categoriasSueldos.index') !!}" class="m-menu__link ">
                    <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                        <span></span>
                    </i>
                    <span class="m-menu__link-text">Categorias</span>
                </a>
            </li>
            <li class="m-menu__item  {!! Request::is('empleados*') ? 'm-menu__item--active' : '' !!}" aria-haspopup="true">
                <a href="{!! route('frontend.sueldos.empleados.index') !!}" class="m-menu__link ">
                    <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                        <span></span>
                    </i>
                    <span class="m-menu__link-text">Empleados</span>
                </a>
            </li>
            <li class="m-menu__item  {!! Request::is('codigos*') ? 'm-menu__item--active' : '' !!}" aria-haspopup="true">
                <a href="{!! route('frontend.sueldos.codigos.index') !!}" class="m-menu__link ">
                    <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                        <span></span>
                    </i>
                    <span class="m-menu__link-text">Codigos</span>
                </a>
            </li>
        </ul>
    </div>
</li>

<li class="m-menu__item  {!! Request::is('cuentos*') ? 'm-menu__item--active' : '' !!}" aria-haspopup="true">
    <a href="{!! route('frontend.cuentos.index') !!}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-line-graph"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
			    <span class="m-menu__link-text">Cuentos</span>
			</span>
		</span>
    </a>
</li>

