<!-- Titulo Field -->
<div class="col-lg-12">
    {{ Form::mdText('titulo', null, ['help-block' => 'Titulo', 'icon-class' => 'fa fa-caret-right','placeholder'=>'Ingrese titulo'],'Titulo') }}
</div>

<!-- Cuento Field -->
<div class="col-lg-12">
    <div class="form-group m-form__group row ">
    {{ Form::textarea('cuento', null, ['help-block' => 'Cuento', 'icon-class' => 'fa fa-caret-right','placeholder'=>'Escribir cuento...']) }}
    </div>
</div>

<!-- Archivo Url Field -->
<div class="col-lg-9">
    <div class="form-group">
        <label>Seleccionar Logo</label>
        <div class="row">
            <div class="col-md-8 col-sm-12">
                <div class="input-group image-preview">
                    <input type="text" class="form-control image-preview-filename" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
                    <span class="input-group-btn">
                                    <!-- image-preview-clear button -->
                                    <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                                      <span class="glyphicon glyphicon-remove"></span> Limpiar
                                    </button>
                        <!-- image-preview-input -->
                                    <div class="btn btn-default image-preview-input">
                                      <span class="glyphicon glyphicon-folder-open"></span>
                                      <span class="image-preview-input-title">Buscar</span>
                                      <input type="file" name="image" accept=".png, .jpg, .jpeg">
                                    </div>
                                </span>
                </div>
            </div>
        </div>
    </div>
</div>

