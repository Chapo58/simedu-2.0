<!-- Id Field -->
<div class="col-md-6">
    {{ Form::mdText('id',  $cuento->id, ['disabled' => 'disabled', 'help-block' => 'Id', 'icon-class' => 'fa fa-caret-right']) }}
</div>

<!-- Alumno Id Field -->
<div class="col-md-6">
    {{ Form::mdText('alumno_id',  $cuento->alumno_id, ['disabled' => 'disabled', 'help-block' => 'Alumno Id', 'icon-class' => 'fa fa-caret-right']) }}
</div>

<!-- Titulo Field -->
<div class="col-md-6">
    {{ Form::mdText('titulo',  $cuento->titulo, ['disabled' => 'disabled', 'help-block' => 'Titulo', 'icon-class' => 'fa fa-caret-right']) }}
</div>

<!-- Cuento Field -->
<div class="col-md-6">
    {{ Form::mdText('cuento',  $cuento->cuento, ['disabled' => 'disabled', 'help-block' => 'Cuento', 'icon-class' => 'fa fa-caret-right']) }}
</div>

<!-- Archivo Url Field -->
<div class="col-md-6">
    {{ Form::mdText('archivo_url',  $cuento->archivo_url, ['disabled' => 'disabled', 'help-block' => 'Archivo Url', 'icon-class' => 'fa fa-caret-right']) }}
</div>

<!-- Created At Field -->
<div class="col-md-6">
    {{ Form::mdText('created_at',  $cuento->created_at, ['disabled' => 'disabled', 'help-block' => 'Created At', 'icon-class' => 'fa fa-caret-right']) }}
</div>

<!-- Updated At Field -->
<div class="col-md-6">
    {{ Form::mdText('updated_at',  $cuento->updated_at, ['disabled' => 'disabled', 'help-block' => 'Updated At', 'icon-class' => 'fa fa-caret-right']) }}
</div>

