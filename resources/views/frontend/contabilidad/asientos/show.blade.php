@extends('frontend.layouts.app')

@section('content')
    <div class="m-content">
         <div class="row">
             <div class="col-lg-12">
                 <div class="m-portlet m-portlet--brand m-portlet--head-solid-bg m-portlet--bordered">
                     <div class="m-portlet__head">
                         <div class="m-portlet__head-caption">
                             <div class="m-portlet__head-title">
    						    <span class="m-portlet__head-icon">
    							    <i class="flaticon-placeholder-2"></i>
    							</span>
                                 <h3 class="m-portlet__head-text">
                                     Consultar Asiento N° {!!$asiento->id!!}
                                 </h3>
                             </div>
                         </div>
                     </div>
                     <div class="m-portlet__body">
                         <div class="row">
                             @include('frontend.contabilidad.asientos.show_fields')
                         </div>
                     </div>
                     <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                         <div class="m-form__actions m-form__actions--solid">
                             <div class="row">
                                 <div class="col-lg-12 m--align-right">
                                     <a href="{!! route('frontend.contabilidad.asientos.index') !!}" class="btn btn-primary">Volver</a>
                                 </div>
                             </div>
                         </div>
                     </div>
                 </div>
             </div>
         </div>
    </div>
@endsection
