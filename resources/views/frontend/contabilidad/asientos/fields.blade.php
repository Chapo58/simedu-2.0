<!-- Fecha Field -->
<div class="form-group col-sm-4">
    <div class="form-group m-form__group row">
        {!! Form::label('fecha', 'Fecha:') !!}
        {!! Form::date('fecha', (isset($asiento)) ? $asiento->fecha : date('Y-m-d'), ['class' => 'form-control']) !!}
    </div>
</div>

<!-- Denominacion Field -->
<div class="col-lg-4">
    {{ Form::mdText('denominacion', null, ['help-block' => 'Denominación', 'icon-class' => 'fa fa-caret-right','placeholder'=>'Denominación'],'Denominación') }}
</div>

<!-- Numero Lista Field -->
<div class="col-lg-4">
    <div class="form-group m-form__group row ">
        {!! Form::label('numero_lista', 'Numero:') !!}
        <div class="m-input-icon m-input-icon--left">
            <span class="m-input-icon__icon m-input-icon__icon--left">
                <span>
                    <i class="fa fa-caret-right"></i>
        		</span>
        	</span>
        	{!! Form::number('numero_lista', (isset($asiento)) ? $asiento->numero_lista : $nro, ['class' => 'form-control m-input','help-block'=>'Numero', 'icon-class'=>'fa fa-caret-right', 'placeholder'=>'Numero del Asiento']) !!}
        </div>
    </div>
</div>

<!-- Nota Field -->
<div class="form-group col-sm-12 col-lg-12">
    <div class="form-group m-form__group row">
        {!! Form::label('nota', 'Nota:') !!}
        {!! Form::textarea('nota', null, ['class' => 'form-control', 'rows' => '5']) !!}
    </div>
</div>

