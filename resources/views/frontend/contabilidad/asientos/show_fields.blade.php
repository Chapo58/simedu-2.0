<!-- Id Field -->
<div class="col-md-6">
    {{ Form::mdText('id',  $asiento->id, ['disabled' => 'disabled', 'help-block' => 'Id', 'icon-class' => 'fa fa-caret-right']) }}
</div>

<!-- Empresa Id Field -->
<div class="col-md-6">
    {{ Form::mdText('empresa_id',  $asiento->empresa_id, ['disabled' => 'disabled', 'help-block' => 'Empresa Id', 'icon-class' => 'fa fa-caret-right']) }}
</div>

<!-- Periodo Id Field -->
<div class="col-md-6">
    {{ Form::mdText('periodo_id',  $asiento->periodo_id, ['disabled' => 'disabled', 'help-block' => 'Periodo Id', 'icon-class' => 'fa fa-caret-right']) }}
</div>

<!-- Fecha Field -->
<div class="col-md-6">
    {{ Form::mdText('fecha',  $asiento->fecha, ['disabled' => 'disabled', 'help-block' => 'Fecha', 'icon-class' => 'fa fa-caret-right']) }}
</div>

<!-- Denominacion Field -->
<div class="col-md-6">
    {{ Form::mdText('denominacion',  $asiento->denominacion, ['disabled' => 'disabled', 'help-block' => 'Denominacion', 'icon-class' => 'fa fa-caret-right']) }}
</div>

<!-- Numero Lista Field -->
<div class="col-md-6">
    {{ Form::mdText('numero_lista',  $asiento->numero_lista, ['disabled' => 'disabled', 'help-block' => 'Numero Lista', 'icon-class' => 'fa fa-caret-right']) }}
</div>

<!-- Nota Field -->
<div class="col-md-6">
    {{ Form::mdText('nota',  $asiento->nota, ['disabled' => 'disabled', 'help-block' => 'Nota', 'icon-class' => 'fa fa-caret-right']) }}
</div>

<!-- Created At Field -->
<div class="col-md-6">
    {{ Form::mdText('created_at',  $asiento->created_at, ['disabled' => 'disabled', 'help-block' => 'Created At', 'icon-class' => 'fa fa-caret-right']) }}
</div>

<!-- Updated At Field -->
<div class="col-md-6">
    {{ Form::mdText('updated_at',  $asiento->updated_at, ['disabled' => 'disabled', 'help-block' => 'Updated At', 'icon-class' => 'fa fa-caret-right']) }}
</div>

