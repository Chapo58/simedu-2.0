<table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
    <thead>
        <tr>
        <th>Numero</th>
        <th>Fecha</th>
        <th>Denominacion</th>
        <th>Debe</th>
        <th>Haber</th>
             <th>Acciones</th>
        </tr>
    </thead>
    <tbody>
    @foreach($asientos as $asiento)
        <tr>
            <td>{!! $asiento->numero_lista !!}</td>
            <td>{!! $asiento->fecha->format('d/m/Y') !!}</td>
            <td>{!! $asiento->denominacion !!}</td>
            <td></td>
            <td></td>
            <td>
                {!! Form::open(['route' => ['frontend.contabilidad.asientos.destroy', $asiento->id], 'method' => 'delete']) !!}
                <div class='m-btn-group m-btn-group--pill btn-group'>
                    <a href="{!! route('frontend.contabilidad.asientos.show', [$asiento->id]) !!}" class='m-btn btn btn-success' data-container="body" data-toggle="m-tooltip" data-placement="top" data-skin="dark" title data-original-title="Consultar"><i class="fas fa-eye"></i></a>
                    <a href="{!! route('frontend.contabilidad.asientos.edit', [$asiento->id]) !!}" class='m-btn btn btn-info' data-container="body" data-toggle="m-tooltip" data-placement="top" data-skin="dark" title data-original-title="Editar"><i class="far fa-edit"></i></a>
                    {!! Form::button('<i class="fas fa-trash"></i>', ['type' => 'submit', 'class' => 'm-btn btn btn-danger','data-container' => 'body','data-toggle' => 'm-tooltip', 'data-placement' => 'top','data-skin' => 'dark','title data-original-title'=>'Eliminar','onclick' => "return confirm('Estás seguro?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>