<!-- Id Field -->
<div class="col-md-6">
    {{ Form::mdText('id',  $periodo->id, ['disabled' => 'disabled', 'help-block' => 'Id', 'icon-class' => 'fa fa-caret-right']) }}
</div>

<!-- User Id Field -->
<div class="col-md-6">
    {{ Form::mdText('user_id',  $periodo->user_id, ['disabled' => 'disabled', 'help-block' => 'User Id', 'icon-class' => 'fa fa-caret-right']) }}
</div>

<!-- Periodo Field -->
<div class="col-md-6">
    {{ Form::mdText('periodo',  $periodo->periodo, ['disabled' => 'disabled', 'help-block' => 'Periodo', 'icon-class' => 'fa fa-caret-right']) }}
</div>

<!-- Descripcion Field -->
<div class="col-md-6">
    {{ Form::mdText('descripcion',  $periodo->descripcion, ['disabled' => 'disabled', 'help-block' => 'Descripcion', 'icon-class' => 'fa fa-caret-right']) }}
</div>

<!-- Desde Field -->
<div class="col-md-6">
    {{ Form::mdText('desde',  $periodo->desde, ['disabled' => 'disabled', 'help-block' => 'Desde', 'icon-class' => 'fa fa-caret-right']) }}
</div>

<!-- Hasta Field -->
<div class="col-md-6">
    {{ Form::mdText('hasta',  $periodo->hasta, ['disabled' => 'disabled', 'help-block' => 'Hasta', 'icon-class' => 'fa fa-caret-right']) }}
</div>

<!-- Created At Field -->
<div class="col-md-6">
    {{ Form::mdText('created_at',  $periodo->created_at, ['disabled' => 'disabled', 'help-block' => 'Created At', 'icon-class' => 'fa fa-caret-right']) }}
</div>

<!-- Updated At Field -->
<div class="col-md-6">
    {{ Form::mdText('updated_at',  $periodo->updated_at, ['disabled' => 'disabled', 'help-block' => 'Updated At', 'icon-class' => 'fa fa-caret-right']) }}
</div>

