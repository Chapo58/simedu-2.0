<!-- Periodo Field -->
<div class="col-lg-6">
    {{ Form::mdText('periodo', null, ['help-block' => 'Periodo', 'icon-class' => 'fa fa-caret-right','placeholder'=>'Ingrese periodo','required' => 'required'],'Periodo') }}
</div>

<!-- Descripcion Field -->
<div class="col-lg-6">
    {{ Form::mdText('descripcion', null, ['help-block' => 'Descripcion', 'icon-class' => 'fa fa-caret-right','placeholder'=>'Ingrese descripcion'],'Descripcion') }}
</div>

<!-- Desde Field -->
<div class="form-group col-sm-6">
    <div class="form-group m-form__group row">
    {!! Form::label('desde', 'Desde:') !!}
    {!! Form::date('desde', (isset($periodo)) ? $periodo->desde : '', ['class' => 'form-control','required' => 'required']) !!}
    </div>
</div>
<!-- Hasta Field -->
<div class="form-group col-sm-6">
    <div class="form-group m-form__group row">
    {!! Form::label('hasta', 'Hasta:') !!}
    {!! Form::date('hasta', (isset($periodo)) ? $periodo->hasta : '', ['class' => 'form-control','required' => 'required']) !!}
    </div>
</div>

