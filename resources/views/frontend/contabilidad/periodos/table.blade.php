<table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
    <thead>
        <tr>
        <th>Periodo</th>
        <th>Descripcion</th>
        <th>Desde</th>
        <th>Hasta</th>
             <th>Acciones</th>
        </tr>
    </thead>
    <tbody>
    @foreach($periodos as $periodo)
        <tr>
            <td>{!! $periodo->periodo !!}</td>
            <td>{!! $periodo->descripcion !!}</td>
            <td>{!! $periodo->desde->format('d/m/Y') !!}</td>
            <td>{!! $periodo->hasta->format('d/m/Y') !!}</td>
            <td>
                {!! Form::open(['route' => ['frontend.contabilidad.periodos.destroy', $periodo->id], 'method' => 'delete']) !!}
                <div class='m-btn-group m-btn-group--pill btn-group'>
                    <a href="{!! route('frontend.contabilidad.periodos.show', [$periodo->id]) !!}" class='m-btn btn btn-success' data-container="body" data-toggle="m-tooltip" data-placement="top" data-skin="dark" title data-original-title="Consultar"><i class="fas fa-eye"></i></a>
                    <a href="{!! route('frontend.contabilidad.periodos.edit', [$periodo->id]) !!}" class='m-btn btn btn-info' data-container="body" data-toggle="m-tooltip" data-placement="top" data-skin="dark" title data-original-title="Editar"><i class="far fa-edit"></i></a>
                    <a href="{{ url('contabilidad/periodos/seleccionar/' . $periodo->id) }}" class='m-btn btn btn-warning' data-container="body" data-toggle="m-tooltip" data-placement="top" data-skin="dark" title data-original-title="Seleccionar"><i class="far fa-hand-point-right"></i></a>
                    {!! Form::button('<i class="fas fa-trash"></i>', ['type' => 'submit', 'class' => 'm-btn btn btn-danger','data-container' => 'body','data-toggle' => 'm-tooltip', 'data-placement' => 'top','data-skin' => 'dark','title data-original-title'=>'Eliminar','onclick' => "return confirm('Estás seguro?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>