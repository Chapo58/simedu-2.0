@extends('frontend.layouts.app')

@section('after-styles')
    <link href="{{asset('frontend/css/jquery/jquery-ui.min.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="col-lg-12">
                @include('components.views.errors')
            </div>
            @if(Session::has('msg'))
                <div class="alert alert-{{Session::get('status')}} alert-dismissable">{{Session::get('msg')}}
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                </div>
            @endif
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">
                <!--begin::Portlet-->
                <div class="m-portlet m-portlet--brand m-portlet--head-solid-bg m-portlet--bordered">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
    						    <span class="m-portlet__head-icon">
    							    <i class="flaticon-placeholder-2"></i>
    							</span>
                                <h3 class="m-portlet__head-text">
                                    Agregar nueva Cuenta
                                </h3>
                            </div>
                        </div>
                    </div>

                    {!! Form::open(['route' => 'frontend.contabilidad.plan_cuentas.store', 'class'=>'m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed']) !!}
                    <div class="m-portlet__body">
                        <div class="row">
                            @include('frontend.contabilidad.plan_cuentas.fields')
                        </div>
                    </div>
                    <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                        <div class="m-form__actions m-form__actions--solid">
                            <div class="row">
                                <div class="col-lg-12 m--align-right">
                                    {!! Form::submit('Guardar', ['class' => 'btn btn-primary btn-oneclick']) !!}
                                    <a href="{!! route('frontend.contabilidad.plan_cuentas.index') !!}" class="btn btn-secondary">Cancelar</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
                <!--end::Portlet-->
            </div>
        </div>
    </div>
@endsection

@section('scripts')

    <script src="{{asset('frontend/js/jquery/jquery-ui.min.js')}}" type="text/javascript"></script>

    <script>
        $("#buscar_cuentas").autocomplete({
            source: function(request, response) {
                $.getJSON("{{ url('contabilidad/plan_cuentas/obtenerCuentas') }}", { term : request.term, codigo : $("#codigo").val()},
                    response);
            },
            minlength: 1,
            autoFocus: true,
            select: function (e, ui) {
                $('input#buscar_cuentas').val(ui.item.value);
                $('input#cuenta_sumariza_id').val(ui.item.id);
            },
            response: function(e, ui) {
                if (ui.content.length == 1){ // Si solo me devuelve una cuenta, lo autocargo
                    ui.item = ui.content[0];
                    $('input#buscar_cuentas').val(ui.item.value);
                    $('input#cuenta_sumariza_id').val(ui.item.id);
                }
            }
        });
    </script>

@endsection