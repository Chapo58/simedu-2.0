<!-- Codigo Field -->
<div class="col-lg-6">
    <div class="form-group m-form__group row ">
        {!! Form::label('codigo', 'Codigo:') !!}
        <div class="m-input-icon m-input-icon--left">
            <span class="m-input-icon__icon m-input-icon__icon--left">
                <span>
                    <i class="fa fa-caret-right"></i>
        		</span>
        	</span>
        	{!! Form::number('codigo', null, ['class' => 'form-control m-input','help-block'=>'Codigo', 'icon-class'=>'fa fa-caret-right', 'placeholder'=>'Ingrese codigo']) !!}
        </div>
    </div>
</div>

<!-- Denominacion Field -->
<div class="col-lg-6">
    {{ Form::mdText('denominacion', null, ['help-block' => 'Denominacion', 'icon-class' => 'fa fa-caret-right','placeholder'=>'Ingrese denominacion'],'Denominacion') }}
</div>

<!-- Rubro Field -->
<div class="col-sm-6">
    <div class="form-group m-form__group row">
        {!! Form::label('rubro', 'Rubro:') !!}
        {!! Form::select('rubro', $rubros, isset($planCuenta->rubro) ? $planCuenta->rubro : '', ['class' => 'form-control select2']) !!}
    </div>
</div>

<!-- Imputable Field -->
<div class="col-sm-6">
    <div class="form-group m-form__group">
        {!! Form::label('imputable', 'Imputable') !!}
        <div class="m-radio-list">
            <label class="m-radio m-radio--state-success">
                {!! Form::radio('imputable', "1", 1) !!} Si
                <span></span>
            </label>
            <label class="m-radio m-radio--state-success">
                {!! Form::radio('imputable', "0", null) !!} No
                <span></span>
            </label>
        </div>
    </div>
</div>

<!-- Cuenta Sumariza Id Field -->
<div class="col-lg-6">
    <div class="form-group m-form__group">
    {!! Form::label('cuenta_sumariza_id', 'Sumariza:') !!}
    <span class="input-icon input-icon-right">
        <input id="buscar_cuentas" name="buscar_cuentas" class="form-control" placeholder="Buscar..." type="text" required="required" value="{{ isset($planCuenta->sumariza) ? $planCuenta->sumariza : '' }}">
    </span>
    <input id="cuenta_sumariza_id" name="cuenta_sumariza_id" type="hidden" value="{{ isset($planCuenta->sumariza) ? $planCuenta->sumariza->id : '' }}">
    </div>
</div>

