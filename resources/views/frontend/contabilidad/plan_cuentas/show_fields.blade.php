<!-- Id Field -->
<div class="col-md-6">
    {{ Form::mdText('id',  $planCuenta->id, ['disabled' => 'disabled', 'help-block' => 'Id', 'icon-class' => 'fa fa-caret-right']) }}
</div>

<!-- Empresa Id Field -->
<div class="col-md-6">
    {{ Form::mdText('empresa_id',  $planCuenta->empresa_id, ['disabled' => 'disabled', 'help-block' => 'Empresa Id', 'icon-class' => 'fa fa-caret-right']) }}
</div>

<!-- Codigo Field -->
<div class="col-md-6">
    {{ Form::mdText('codigo',  $planCuenta->codigo, ['disabled' => 'disabled', 'help-block' => 'Codigo', 'icon-class' => 'fa fa-caret-right']) }}
</div>

<!-- Denominacion Field -->
<div class="col-md-6">
    {{ Form::mdText('denominacion',  $planCuenta->denominacion, ['disabled' => 'disabled', 'help-block' => 'Denominacion', 'icon-class' => 'fa fa-caret-right']) }}
</div>

<!-- Rubro Field -->
<div class="col-md-6">
    {{ Form::mdText('rubro',  $planCuenta->rubro, ['disabled' => 'disabled', 'help-block' => 'Rubro', 'icon-class' => 'fa fa-caret-right']) }}
</div>

<!-- Imputable Field -->
<div class="col-md-6">
    {{ Form::mdText('imputable',  $planCuenta->imputable, ['disabled' => 'disabled', 'help-block' => 'Imputable', 'icon-class' => 'fa fa-caret-right']) }}
</div>

<!-- Cuenta Sumariza Id Field -->
<div class="col-md-6">
    {{ Form::mdText('cuenta_sumariza_id',  $planCuenta->cuenta_sumariza_id, ['disabled' => 'disabled', 'help-block' => 'Cuenta Sumariza Id', 'icon-class' => 'fa fa-caret-right']) }}
</div>

<!-- Created At Field -->
<div class="col-md-6">
    {{ Form::mdText('created_at',  $planCuenta->created_at, ['disabled' => 'disabled', 'help-block' => 'Created At', 'icon-class' => 'fa fa-caret-right']) }}
</div>

<!-- Updated At Field -->
<div class="col-md-6">
    {{ Form::mdText('updated_at',  $planCuenta->updated_at, ['disabled' => 'disabled', 'help-block' => 'Updated At', 'icon-class' => 'fa fa-caret-right']) }}
</div>

