<table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
    <thead>
        <tr>
            <th>Codigo</th>
            <th>Denominacion</th>
            <th>Rubro</th>
            <th>Imputable</th>
            <th>Sumariza</th>
            <th>Acciones</th>
        </tr>
    </thead>
    <tbody>
    @foreach($planCuentas as $planCuenta)
        <tr>
            <td>{!! $planCuenta->codigo !!}</td>
            <td>{!! $planCuenta->denominacion !!}</td>
            <td>{!! $rubros[$planCuenta->rubro] !!}</td>
            <td>{!! ($planCuenta->imputable) ? 'Si' : 'No' !!}</td>
            <td>{!! $planCuenta->sumariza !!}</td>
            <td>
                {!! Form::open(['route' => ['frontend.contabilidad.plan_cuentas.destroy', $planCuenta->id], 'method' => 'delete']) !!}
                <div class='m-btn-group m-btn-group--pill btn-group'>
                    <a href="{!! route('frontend.contabilidad.plan_cuentas.show', [$planCuenta->id]) !!}" class='m-btn btn btn-success' data-container="body" data-toggle="m-tooltip" data-placement="top" data-skin="dark" title data-original-title="Consultar"><i class="fas fa-eye"></i></a>
                    <a href="{!! route('frontend.contabilidad.plan_cuentas.edit', [$planCuenta->id]) !!}" class='m-btn btn btn-info' data-container="body" data-toggle="m-tooltip" data-placement="top" data-skin="dark" title data-original-title="Editar"><i class="far fa-edit"></i></a>
                    {!! Form::button('<i class="fas fa-trash"></i>', ['type' => 'submit', 'class' => 'm-btn btn btn-danger','data-container' => 'body','data-toggle' => 'm-tooltip', 'data-placement' => 'top','data-skin' => 'dark','title data-original-title'=>'Eliminar','onclick' => "return confirm('Estás seguro?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>