<!-- Razon Social Field -->
<div class="col-md-6">
    {{ Form::mdText('razon_social',  $empresa->razon_social, ['disabled' => 'disabled', 'help-block' => 'Razon Social', 'icon-class' => 'fa fa-caret-right'],'Razon Social') }}
</div>

<!-- Cuit Field -->
<div class="col-md-6">
    {{ Form::mdText('cuit',  $empresa->cuit, ['disabled' => 'disabled', 'help-block' => 'Cuit', 'icon-class' => 'fa fa-caret-right'],'Cuit') }}
</div>

<!-- Inicio Actividad Field -->
<div class="col-md-6">
    {{ Form::mdText('inicio_actividad',  $empresa->inicio_actividad, ['disabled' => 'disabled', 'help-block' => 'Inicio Actividad', 'icon-class' => 'fa fa-caret-right'],'Inicio Actividad') }}
</div>

<!-- Direccion Id Field -->
<div class="col-md-6">
    {{ Form::mdText('direccion_id',  $empresa->direccion, ['disabled' => 'disabled', 'help-block' => 'Direccion', 'icon-class' => 'fa fa-caret-right'],'Direccion') }}
</div>

<!-- Condicion Iva Field -->
<div class="col-md-6">
    {{ Form::mdText('condicion_iva',  $empresa->condicionIva(), ['disabled' => 'disabled', 'help-block' => 'Condicion Iva', 'icon-class' => 'fa fa-caret-right'],'Condicion Iva') }}
</div>

<!-- Ingresos Brutos Field -->
<div class="col-md-6">
    {{ Form::mdText('ingresos_brutos',  $empresa->ingresos_brutos, ['disabled' => 'disabled', 'help-block' => 'Ingresos Brutos', 'icon-class' => 'fa fa-caret-right'], 'Ingresos Brutos') }}
</div>

<!-- Telefono Field -->
<div class="col-md-6">
    {{ Form::mdText('telefono',  $empresa->telefono, ['disabled' => 'disabled', 'help-block' => 'Telefono', 'icon-class' => 'fa fa-caret-right'], 'Telefono') }}
</div>

<!-- Web Field -->
<div class="col-md-6">
    {{ Form::mdText('web',  $empresa->web, ['disabled' => 'disabled', 'help-block' => 'Web', 'icon-class' => 'fa fa-caret-right'], 'Web') }}
</div>

<!-- Email Field -->
<div class="col-md-6">
    {{ Form::mdText('email',  $empresa->email, ['disabled' => 'disabled', 'help-block' => 'Email', 'icon-class' => 'fa fa-caret-right'], 'Email') }}
</div>

<!-- Contacto Field -->
<div class="col-md-6">
    {{ Form::mdText('contacto',  $empresa->contacto, ['disabled' => 'disabled', 'help-block' => 'Contacto', 'icon-class' => 'fa fa-caret-right'], 'Contacto') }}
</div>

<!-- Logo Url Field -->
<div class="col-md-6">
    {{ Form::mdText('logo_url',  $empresa->logo_url, ['disabled' => 'disabled', 'help-block' => 'Logo', 'icon-class' => 'fa fa-caret-right'],'Logo') }}
</div>

<!-- Comienzo Asientos Field -->
<div class="col-md-6">
    {{ Form::mdText('comienzo_asientos',  $empresa->comienzo_asientos, ['disabled' => 'disabled', 'help-block' => 'Comienzo Asientos', 'icon-class' => 'fa fa-caret-right'],'Comienzo Asientos') }}
</div>

<!-- Created At Field -->
<div class="col-md-6">
    {{ Form::mdText('created_at',  $empresa->created_at, ['disabled' => 'disabled', 'help-block' => 'Creada el', 'icon-class' => 'fa fa-caret-right'],'Creada el') }}
</div>

<!-- Updated At Field -->
<div class="col-md-6">
    {{ Form::mdText('updated_at',  $empresa->updated_at, ['disabled' => 'disabled', 'help-block' => 'Actualizada el', 'icon-class' => 'fa fa-caret-right'],'Actualizada el') }}
</div>

