
<!-- Razon Social Field -->
<div class="col-lg-6">
    {{ Form::mdText('razon_social', null, ['help-block' => 'Razon Social', 'icon-class' => 'fa fa-caret-right','placeholder'=>'Ingrese razon social o nombre de fantasia'],'Razon Social') }}
</div>
<!-- Cuit Field -->
<div class="col-lg-6">
    <div class="form-group m-form__group row ">
        {!! Form::label('cuit', 'Cuit:') !!}
        <div class="m-input-icon m-input-icon--left">
            <span class="m-input-icon__icon m-input-icon__icon--left">
                <span>
                    <i class="fa fa-caret-right"></i>
        		</span>
        	</span>
        	{!! Form::number('cuit', null, ['class' => 'form-control m-input','help-block'=>'Cuit', 'icon-class'=>'fa fa-caret-right', 'placeholder'=>'Ingrese cuit']) !!}
        </div>
    </div>
</div>

<!-- Direccion Id Field -->
<div class="col-lg-3">
    {{ Form::mdText('direccion[direccion]', isset($empresa->direccion->direccion) ? $empresa->direccion->direccion : '', ['help-block' => 'Direccion', 'icon-class' => 'fa fa-caret-right','placeholder'=>'Ingrese una direccion'],'Direccion') }}
</div>

<div class="col-lg-3">
    <div class="form-group m-form__group row ">
        {!! Form::label('direccion[localidad_id]', 'Localidad') !!}
        {{ Form::select('direccion[localidad_id]', $localidades, isset($empresa->direccion->localidad) ? $empresa->direccion->localidad->id : '', array('class' => 'form-control select2')) }}
    </div>
</div>

<div class="col-lg-3">
    <div class="form-group m-form__group row ">
        {!! Form::label('direccion[provincia_id]', 'Provincia') !!}
        {{ Form::select('direccion[provincia_id]', $provincias, isset($empresa->direccion->provincia) ? $empresa->direccion->provincia->id : '', array('class' => 'form-control select2')) }}
    </div>
</div>

<div class="col-lg-3">
    <div class="form-group m-form__group row ">
        {!! Form::label('direccion[pais_id]', 'Pais') !!}
        {{ Form::select('direccion[pais_id]', $paises, isset($empresa->direccion->pais) ? $empresa->direccion->pais->id : '', array('class' => 'form-control select2')) }}
    </div>
</div>

<!-- Ingresos Brutos Field -->
<div class="col-lg-6">
    <div class="form-group m-form__group row ">
        {!! Form::label('ingresos_brutos', 'Ingresos Brutos:') !!}
        <div class="m-input-icon m-input-icon--left">
            <span class="m-input-icon__icon m-input-icon__icon--left">
                <span>
                    <i class="fa fa-caret-right"></i>
        		</span>
        	</span>
        	{!! Form::number('ingresos_brutos', null, ['class' => 'form-control m-input','help-block'=>'Ingresos Brutos', 'icon-class'=>'fa fa-caret-right', 'placeholder'=>'Ingrese N° de ingresos brutos']) !!}
        </div>
    </div>
</div>

<!-- Telefono Field -->
<div class="col-lg-6">
    {{ Form::mdText('telefono', null, ['help-block' => 'Telefono', 'icon-class' => 'fa fa-caret-right','placeholder'=>'Ingrese telefono'],'Telefono') }}
</div>

<!-- Web Field -->
<div class="col-lg-6">
    {{ Form::mdText('web', null, ['help-block' => 'Web', 'icon-class' => 'fa fa-caret-right','placeholder'=>'Ingrese pagina web'],'Web') }}
</div>

<!-- Email Field -->
<div class="col-md-6">
    {{ Form::mdText('email', null, ['type' => 'email', 'help-block' => 'Email', 'icon-class' => 'fa fa-envelope','placeholder'=>'Ingrese direccion de email'],'Email') }}
</div>

<!-- Contacto Field -->
<div class="col-lg-6">
    {{ Form::mdText('contacto', null, ['help-block' => 'Contacto', 'icon-class' => 'fa fa-caret-right','placeholder'=>'Ingrese contacto'],'Contacto') }}
</div>

<!-- Comienzo Asientos Field -->
<div class="col-lg-6">
    <div class="form-group m-form__group row ">
        {!! Form::label('comienzo_asientos', 'Comienzo Asientos:') !!}
        <div class="m-input-icon m-input-icon--left">
            <span class="m-input-icon__icon m-input-icon__icon--left">
                <span>
                    <i class="fa fa-caret-right"></i>
        		</span>
        	</span>
        	{!! Form::number('comienzo_asientos', isset($empresa->comienzo_asientos) ? $empresa->comienzo_asientos : 1, ['class' => 'form-control m-input','help-block'=>'Comienzo Asientos', 'icon-class'=>'fa fa-caret-right', 'placeholder'=>'Ingrese N° de comienzo de asientos']) !!}
        </div>
    </div>
</div>

<!-- Inicio Actividad Field -->
<div class="col-lg-6">
    <div class="form-group m-form__group row ">
        {!! Form::label('inicio_actividad', 'Inicio Actividad:') !!}
        {!! Form::date('inicio_actividad', null, ['class' => 'form-control']) !!}
    </div>
</div>

<!-- Condicion Iva Field -->
<div class="col-lg-6">
    <div class="form-group m-form__group row ">
        {!! Form::label('condicion_iva', 'Condicion Iva:') !!}
        {!! Form::select('condicion_iva', $condicionesIva, isset($empresa->condicion_iva) ? $empresa->condicion_iva : '', array('class' => 'form-control select2')) !!}
    </div>
</div>

<div class="col-lg-9">
    <div class="form-group">
        <label>Seleccionar Logo</label>
        <div class="row">
            <div class="col-md-8 col-sm-12">
                <div class="input-group image-preview">
                    <input type="text" class="form-control image-preview-filename" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
                    <span class="input-group-btn">
                                    <!-- image-preview-clear button -->
                                    <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                                      <span class="glyphicon glyphicon-remove"></span> Limpiar
                                    </button>
                        <!-- image-preview-input -->
                                    <div class="btn btn-default image-preview-input">
                                      <span class="glyphicon glyphicon-folder-open"></span>
                                      <span class="image-preview-input-title">Buscar</span>
                                      <input type="file" name="image" accept=".png, .jpg, .jpeg">
                                    </div>
                                </span>
                </div>
            </div>
        </div>
    </div>
</div>

