<table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
    <thead>
        <tr>
            <th>Logo</th>
            <th>Razon Social</th>
            <th>Cuit</th>
            <th>Condicion Iva</th>
            <th>Email</th>
            <th>Acciones</th>
        </tr>
    </thead>
    <tbody>
    @foreach($empresas as $empresa)
        <tr>
            <td>{!! $empresa->logo_url !!}</td>
            <td>{!! $empresa->razon_social !!}</td>
            <td>{!! $empresa->cuit !!}</td>
            <td>{!! $empresa->condicionIva() !!}</td>
            <td>{!! $empresa->email !!}</td>
            <td>
                {!! Form::open(['route' => ['frontend.empresas.destroy', $empresa->id], 'method' => 'delete']) !!}
                <div class='m-btn-group m-btn-group--pill btn-group'>
                    <a href="{!! route('frontend.empresas.show', [$empresa->id]) !!}" class='m-btn btn btn-success' data-container="body" data-toggle="m-tooltip" data-placement="top" data-skin="dark" title data-original-title="Consultar"><i class="fas fa-eye"></i></a>
                    <a href="{!! route('frontend.empresas.edit', [$empresa->id]) !!}" class='m-btn btn btn-info' data-container="body" data-toggle="m-tooltip" data-placement="top" data-skin="dark" title data-original-title="Editar"><i class="far fa-edit"></i></a>
                    <a href="{{ url('empresas/seleccionar/' . $empresa->id) }}" class='m-btn btn btn-warning' data-container="body" data-toggle="m-tooltip" data-placement="top" data-skin="dark" title data-original-title="Seleccionar"><i class="far fa-hand-point-right"></i></a>
                    {!! Form::button('<i class="fas fa-trash"></i>', ['type' => 'submit', 'class' => 'm-btn btn btn-danger','data-container' => 'body','data-toggle' => 'm-tooltip', 'data-placement' => 'top','data-skin' => 'dark','title data-original-title'=>'Eliminar','onclick' => "return confirm('Estás seguro?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>