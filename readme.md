## Laravel 5.6 Boilerplate
http://laravel-boilerplate.com/

Primeros Pasos despues de descargar:

Ejecutar en consola:

$ composer install
$ npm install (Habiendo instalado Node.js)

Crear base de datos y configurar .env

$ php artisan key:generate
$ php artisan migrate
$ php artisan db:seed
$ npm run dev
$ php artisan serve

Login:
Usuario: admin@admin.com
Pass: secret

## CRUD Generator
https://github.com/appzcoder/crud-generator/blob/master/doc/README.md

Ejemplo:
php artisan crud:generate Ejemplo --fields="nombre#string; apellido#string;"
--model-namespace=Models\Backend\Ejemplo
--view-path=backend\ejemplo
--controller-namespace=Backend\Ejemplo
--validations="nombre#required; apellido#required;"

Ejemplo from json:
Ruta de json: database/json/ 
php artisan crud:generate Profesores --fields_from_file="C:\xampp\htdocs\simedu\database\json\profesor.json" --model-namespace=Models\Backend --view-path=backend --controller-namespace=Backend

## InfyOm Generator
http://labs.infyom.com/laravelgenerator/docs/5.6/introduction

Ejemplo
Php artisan infyom:scaffold Periodo --fieldsFile=database/json/periodos.json --tableName=periodos --prefix=Contabilidad

#### Instalacion DomPDF -> https://github.com/dompdf/dompdf
composer require dompdf/dompdf
composer require barryvdh/laravel-dompdf

composer update