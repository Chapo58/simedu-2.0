<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePlanesCuentasTable extends Migration{


    public function up(){
        Schema::create('planes_cuentas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('empresa_id')->unsigned();
            $table->integer('codigo');
            $table->string('denominacion', 191);
            $table->tinyInteger('rubro');
            $table->tinyInteger('imputable');
            $table->integer('cuenta_sumariza_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('empresa_id')->references('id')->on('empresas');
            $table->foreign('cuenta_sumariza_id')->references('id')->on('planes_cuentas')->nullable();
        });
    }

    public function down(){
        Schema::drop('planes_cuentas');
    }
}
