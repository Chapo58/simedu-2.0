<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEmpresasTable extends Migration
{

    public function up()
    {
        Schema::create('empresas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('alumno_id')->unsigned();
            $table->string('razon_social', 191);
            $table->string('cuit', 191);
            $table->date('inicio_actividad')->nullable();
            $table->integer('direccion_id')->unsigned()->nullable();
            $table->integer('condicion_iva')->unsigned();
            $table->string('ingresos_brutos', 191)->nullable();
            $table->string('telefono', 191)->nullable();
            $table->string('web', 191)->nullable();
            $table->string('email', 191)->nullable();
            $table->string('contacto', 191)->nullable();
            $table->string('logo_url', 191)->nullable();
            $table->integer('comienzo_asientos');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('alumno_id')->references('id')->on('alumnos');
            $table->foreign('direccion_id')->references('id')->on('direcciones');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('empresas');
    }
}
