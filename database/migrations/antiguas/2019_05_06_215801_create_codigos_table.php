<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCodigosTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('codigos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('empresa_id')->unsigned();
            $table->integer('numero');
            $table->string('descripcion', 191);
            $table->tinyInteger('es_sueldo')->nullable();
            $table->integer('tipo_haberes')->unsigned()->nullable();
            $table->integer('obtencion')->unsigned()->nullable();
            $table->integer('cantidad')->nullable();
            $table->float('multiplica', 12, 2)->nullable();
            $table->float('divide', 12, 2)->nullable();
            $table->tinyInteger('sujeto_a_aguinaldo')->nullable();
            $table->integer('codigo_superior_id')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('empresa_id')->references('id')->on('empresas');
            $table->foreign('codigo_superior_id')->references('id')->on('codigos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('codigos');
    }
}
