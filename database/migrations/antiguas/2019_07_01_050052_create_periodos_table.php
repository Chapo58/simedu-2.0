<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePeriodosTable extends Migration{

    public function up(){
        Schema::create('periodos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('periodo', 191);
            $table->string('descripcion', 191);
            $table->date('desde');
            $table->date('hasta');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    public function down(){
        Schema::drop('periodos');
    }
}
