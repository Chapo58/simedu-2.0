<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProvinciasTable extends Migration{
    public function up(){
        Schema::create('provincias', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->integer('pais_id')->unsigned();
            $table->foreign('pais_id')->references('id')->on('paises');
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });

    }

    public function down(){
        Schema::drop('provincias');
    }
}
