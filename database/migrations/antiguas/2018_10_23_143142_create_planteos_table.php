<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePlanteosTable extends Migration{
    public function up(){
        Schema::create('planteos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->integer('profesor_id')->unsigned();
            $table->boolean('habilitado')->nullable();
            $table->foreign('profesor_id')->references('id')->on('profesores')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
            });
    }

    public function down(){
        Schema::drop('planteos');
    }
}
