<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateObrasSocialesTable extends Migration{
    public function up(){
        Schema::create('obras_sociales', function (Blueprint $table) {
            $table->increments('id');
            $table->string('codigo');
            $table->string('descripcion');
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
            });
    }

    public function down(){
        Schema::drop('obras_sociales');
    }
}
