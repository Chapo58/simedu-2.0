<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Luego de modificar los roles se debe revisar el archivo config/access y actualizar los valores de admin_role y default_role
        DB::table('roles')->where('name', 'administrator')->update(['name' => 'superadministrador']);
        DB::table('roles')->where('name', 'executive')->update(['name' => 'institucion']);
        DB::table('roles')->where('name', 'user')->update(['name' => 'alumno']);
        DB::table('roles')->insert(['name' => 'profesor', 'guard_name' => 'web']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('roles')->where('name', 'superadministrador')->update(['name' => 'administrator']);
        DB::table('roles')->where('name', 'institucion')->update(['name' => 'executive']);
        DB::table('roles')->where('name', 'alumno')->update(['name' => 'user']);
        DB::table('roles')->where('name', 'profesor')->delete();
    }
}
