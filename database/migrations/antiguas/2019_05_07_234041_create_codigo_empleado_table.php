<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCodigoEmpleadoTable extends Migration{

    public function up(){
        Schema::create('codigo_empleado', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('codigo_id')->unsigned();
            $table->integer('empleado_id')->unsigned();
            $table->float('multiplica', 12, 2)->nullable();
            $table->float('divide', 12, 2)->nullable();
            $table->integer('cantidad')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('codigo_id')->references('id')->on('codigos');
            $table->foreign('empleado_id')->references('id')->on('empleados');
        });
    }

    public function down(){
        Schema::dropIfExists('codigo_empleado');
    }
}
