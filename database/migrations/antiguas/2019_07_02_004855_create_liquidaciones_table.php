<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLiquidacionesTable extends Migration{


    public function up(){
        Schema::create('liquidaciones', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('empleado_id')->unsigned();
            $table->integer('numero');
            $table->date('periodo');
            $table->string('archivo_url', 191)->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('empleado_id')->references('id')->on('empleados');
        });
    }

    public function down(){
        Schema::drop('liquidaciones');
    }
}
