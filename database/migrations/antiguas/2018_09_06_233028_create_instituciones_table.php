<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInstitucionesTable extends Migration{
    public function up(){
        Schema::create('instituciones', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('numero')->nullable();
            $table->string('nombre')->nullable();
            $table->string('direccion')->nullable();
            $table->text('telefono1')->nullable();
            $table->text('telefono2')->nullable();
            $table->string('email')->nullable();
            $table->integer('user_id')->unsigned();
            $table->string('logo')->nullable();
            $table->boolean('habilitado')->nullable();
            $table->string('nombre_director')->nullable();
            $table->text('telefono_director')->nullable();
            $table->string('email_director')->nullable();
            $table->string('direccion_director')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
            });
    }

    public function down(){
        Schema::drop('instituciones');
    }
}
