<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCursosTable extends Migration{

    public function up(){
        Schema::create('cursos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre')->nullable();
            $table->integer('institucion_id')->unsigned();
            $table->boolean('habilitado')->nullable();
            $table->foreign('institucion_id')->references('id')->on('instituciones')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
            });

        Schema::create('profesorxcurso', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('profesor_id');
            $table->integer('curso_id');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('alumnoxcurso', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('alumno_id');
            $table->integer('curso_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down(){
        Schema::drop('alumnoxcurso');
        Schema::drop('profesorxcurso');
        Schema::drop('cursos');
    }
}
