<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCuentosTable extends Migration{

    public function up(){
        Schema::create('cuentos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('alumno_id')->unsigned();
            $table->string('titulo', 191);
            $table->text('cuento');
            $table->string('archivo_url', 191)->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('alumno_id')->references('id')->on('alumnos');
        });
    }

    public function down(){
        Schema::drop('cuentos');
    }
}
