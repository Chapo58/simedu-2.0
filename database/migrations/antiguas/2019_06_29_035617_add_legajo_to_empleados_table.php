<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLegajoToEmpleadosTable extends Migration{

    public function up(){
        Schema::table('empleados', function (Blueprint $table) {
            $table->string('legajo', 191);
            $table->string('cargo', 191);
        });
    }

    public function down(){
        Schema::table('empleados', function (Blueprint $table) {
            $table->dropColumn('legajo');
            $table->dropColumn('cargo');
        });
    }
}
