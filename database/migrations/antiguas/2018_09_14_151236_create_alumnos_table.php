<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAlumnosTable extends Migration{
    public function up(){
        Schema::create('alumnos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre')->nullable();
            $table->string('apellido')->nullable();
            $table->string('email')->nullable();
            $table->boolean('genero')->nullable();
            $table->date('fecha_nacimiento')->nullable();
            $table->text('telefono')->nullable();
            $table->string('direccion')->nullable();
            $table->integer('user_id')->unsigned();
            $table->integer('institucion_id')->unsigned();
            $table->string('facebook')->nullable();
            $table->string('twitter')->nullable();
            $table->string('instagram')->nullable();
            $table->string('googlemas')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('institucion_id')->references('id')->on('instituciones')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
            });
    }

    public function down(){
        Schema::drop('alumnos');
    }
}
