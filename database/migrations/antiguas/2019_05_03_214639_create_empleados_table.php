<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEmpleadosTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empleados', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('empresa_id')->unsigned();
            $table->string('nombre', 191);
            $table->string('apellido', 191);
            $table->integer('sexo')->unsigned()->nullable();
            $table->date('fecha_ingreso')->nullable()->nullable();
            $table->integer('estado_civil')->unsigned()->nullable();
            $table->string('cuit', 191);
            $table->date('fecha_nacimiento')->nullable()->nullable();
            $table->integer('direccion_id')->unsigned()->nullable();
            $table->integer('categoria_id')->unsigned()->nullable();
            $table->integer('obra_social_id')->unsigned()->nullable();
            $table->text('telefono')->nullable();
            $table->string('email', 191)->nullable();
            $table->text('observaciones')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('empresa_id')->references('id')->on('empresas');
            $table->foreign('direccion_id')->references('id')->on('direcciones');
            $table->foreign('categoria_id')->references('id')->on('categorias_sueldos');
            $table->foreign('obra_social_id')->references('id')->on('obras_sociales');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('empleados');
    }
}
