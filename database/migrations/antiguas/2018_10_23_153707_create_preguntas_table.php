<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePreguntasTable extends Migration{
    public function up(){
        Schema::create('preguntas', function (Blueprint $table) {
            $table->increments('id');
            $table->text('pregunta');
            $table->string('archivo')->nullable();
            $table->string('linkexterno')->nullable();
            $table->string('opcion1')->nullable();
            $table->string('opcion2')->nullable();
            $table->string('opcion3')->nullable();
            $table->string('opcion4')->nullable();
            $table->string('opcion5')->nullable();
            $table->string('opcion6')->nullable();
            $table->integer('opcionCorrecta')->nullable();
            $table->integer('planteo_id')->unsigned();
            $table->foreign('planteo_id')->references('id')->on('planteos')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
            });
    }

    public function down(){
        Schema::drop('preguntas');
    }
}
