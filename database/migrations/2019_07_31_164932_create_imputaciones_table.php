<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImputacionesTable extends Migration{

    public function up(){
        Schema::create('imputaciones', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('asiento_id')->unsigned();
            $table->integer('cuenta_id')->unsigned();
            $table->decimal('debe', 10, 2);
            $table->decimal('haber', 10, 2);
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('asiento_id')->references('id')->on('asientos');
            $table->foreign('cuenta_id')->references('id')->on('planes_cuentas');
        });
    }

    public function down(){
        Schema::dropIfExists('imputaciones');
    }
}
