<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAsientosTable extends Migration{

    public function up(){
        Schema::create('asientos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('empresa_id')->unsigned();
            $table->integer('periodo_id')->unsigned();
            $table->date('fecha');
            $table->string('denominacion', 191);
            $table->integer('numero_lista')->unsigned();
            $table->string('nota', 191)->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('empresa_id')->references('id')->on('empresas');
            $table->foreign('periodo_id')->references('id')->on('periodos');
        });
    }

    public function down(){
        Schema::drop('asientos');
    }
}
